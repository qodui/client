# Quality Of Devices (QOD)

This is the client side of the QODUI project.

## Pre-Requisites

-   VSCode IDE
-   NodeJS v10.16 and above
-   npm v6.9.0 and above
-   Git

## Setup

Clone the repository to your local machine using the following command from command prompt:

    1. git clone https://<userID>@bitbucket.org/qodui/client.git
    2. npm install
    3. npm start

This will just start the application locally. To run injunction with the server. Run the following command:

    1. npm run build
    2. Copy the build folder generated and paste it in the root folder of the server application.
    3. Start the server application with comand: `npm start`
    4. Open `http://<url>:8080`. Example: `http://localhost:8080`

## Project Structure and information on files

### `src`

-   `actions` | Contains the action creators used to update Redux store
    -   types.js | Contains constants used by actions and reducers to identify actions.
-   `components` | Contains all the components.
    -   `App` | Main component called by the _index.js_ file which depending on whether the user has logged in will show `Landing` or `Login`
    -   `Landing` | Component shown to user after user logs in to the application. Shown at `http://<url>:8080/`

        _The Landing Page uses `AppBar`, `Sidebar` from layout and `PageContent` Component. Based on the user's selection in the `Sidebar` menu, different content is picked from `Pagecontent`_
        
    -   `Login` | Component shown to user if the user is not logged in or has logged out or the session is timed out. Shown at `http://<url>:8080/login`
        -   `Info` | Component that shows the information for the user about what the application is about.
    -   `PageContent`
        -   `Account` | Component for Account information shown to user. Used in `PageContent` index file.
        -   `CapMgmt` | Component for Capability Management page. Used in `PageContent` index file.
        -   `Configurations` | Components for Configurations page. Used in `PageContent` index file.
        ```
            `ConfigDetails` | Show the configuration details while creating new configuration.
            `CreateNew` | CreateNew Component controls data related to creating new configuration. Has the `ConfigDetails` nested inside.
        ```
        -   `Dashboard` | Component for Dashboard page. Used in `PageContent` index file.
        -   `Environments` | Components for Environments page. Used in `PageContent` index file.
        ```
            `CloudEnvironment` | Component related to new cloud environment. All Components in this folder starting from "Cloud" are nested inside this component. Nested in `Environments` index file.
                `CloudOptionalParameters` | Component used for Step 4 of new Cloud environment creation. Nested in `CloudEnvironment`
                `CloudProperties` | Component used for Step 2 and 3 of new cloud environment creation. Nested in `CloudEnvironment`
                `CloudSelection` | Component used for Step 1 of new cloud environment creation. Nested in `CloudEnvironment`
                `ReviewDetails` | Component used for Step 5 of new cloud environment creation. Nested in `CloudEnvironment`
            `OnPremiseEnvironment` | Component related to existing environment. Nested in `Environments` index file.
                `ExistingFile` | Component used to upload existing environment file. Nested in `OnPremiseEnvironment`
                `NewFile` | Component used for new file creation (currently disabled). Nested in `OnPremiseEnvironment`
                `Preview` | Component used to preview the file uploaded/created by user. Nested in `OnPremiseEnvironment`
            -   `ViewFile` | Component used to view the file of created environments from table. Opens up as dialog. Nested in `Environments` index file.
        ```
        -   `Executions` | Components for Executions page. Used in `PageContent` index file. _TODO_
        -   `Reports` | Components for Reports page. Used in `PageContent` index file. _TODO_
        -   `RoleMgmt` | Component for Roles Management page. Used in `PageContent` index file.
        -   `UserMgmt` | Component for Users Management page. Used in `PageContent` index file.
        
-   `content` | Contains the data required to be shown on different pages.
    -   `configurations`
        -   _options.js_ | Dropdown options for `Configurations` Component.
        -   _table.js_ | `Configurations` table properties. Any change in table options needs to be changed in this file.
    -   `dashboard`
        -   _content.js_ | Cards on `Dashboard` component get data from this file. If something needs to change in dashboard view, the data can be modified here.
    -   `drawer`
        -   _content.js_ | Contains the menu items shown on the `Sidebar` component. Any changes in list can be done here.
    -   `environments`
        -   _example.js_ | Contains the example file whose format needs to be followed for the existing env file that needs to be uploaded by the user.
        -   _parameters.js_ | Parameters shown for different cloud platforms in `CloudProperties` comes from here. Any change here will reflect in the UI.
        -   _steps.js_ | Contains the steps in the new cloud environment creation.
        -   _table.js_ | `Environments` table properties. Any change in table options needs to be changed in this file.
    -   `info`
        -   _dom.js_ | Contains the Delivery and Operating Model info on the `Info` page.
        -   _fat.js_ | Contains the Framework and Automation Tools info on the `Info` page.
        -   _kvp.js_ | Contains the Key Value Proposition info on the `Infor` page.
        -   _pov.js_ | Contains the Point of View info on the `Info` page.
-   `layout` | Contains common components required to load the application layout.
    -   `AppBar` | Component for the AppBar containing the link to open Accounts page and Logout of the application.
    -   `Item` | Component for the Sidebar items.
    -   `MyModal` | Component for a Dialog to popup with some information.
    -   `Sidebar` | Component for the Sidebar which contains the link to different pages.
    -   `TabPanel` | Component for the Tab expansion panel on the `Info` Page.
-   `reducers` | Contains the reducers used to update Redux store or get state details.
-   `resources`
    -   `images` | Images shown on screen are stored here.
-   `history.js` | Holds the browser history for navigating between pages.
-   `index.js` | The **Starting Point of the QODUI Application**. Sets up the toaster properties, sets the redux state provider component

### `jsconfig.json`

_Configuration which tells the compiler to take the proper files without having to use relative path for import statements.Other options can be configured here in the future._

### `build`

_Contains the production build files after running the command `npm run build`._

**THE WHOLE FOLDER NEEDS TO BE COPIED TO THE SERVER FOLDER**

## Available Scripts

In the project directory, you can run:

#### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

#### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!
