import axios from "axios";
import {BACKEND_URL} from "constants.js";



export default axios.create({
	baseURL: BACKEND_URL
});
