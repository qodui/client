import axios from "axios";
import {DJANGO_URL} from "constants.js";



export default axios.create({
	baseURL: DJANGO_URL
});
