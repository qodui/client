import React from "react";
import {
	ListItemIcon,
	CssBaseline,
	Typography,
	MenuItem,
	Menu
} from "@material-ui/core";
import { AccountCircle } from "@material-ui/icons";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import store from "store";
import { connect } from "react-redux";
import _ from "lodash";

import { logout, getRoles, getCapabilities } from "actions";
import AppBar from "layout/AppBar";
import Sidebar from "layout/Sidebar";
import PageContent from "components/PageContent";

const styles = theme => ({
	root: {
		display: "flex"
	},
	drawer: {
		width: 320,
		flexShrink: 0
	},
	drawerPaper: {
		width: 320
	},
	content: {
		flexGrow: 1,
		padding: theme.spacing(3)
	},
	toolbar: theme.mixins.toolbar,
	nested: {
		paddingLeft: theme.spacing(4)
	}
});

class Landing extends React.Component {
	state = {
		content: "dashboard",
		anchorEl: null,
		expand: false,
		menu: "",
		currentUserCapabilities: [],
		childrenLoading: false
	};

	componentDidMount() {
		if (store.get("isLoggedIn") && store.get("user")) {
			this.setState({ childrenLoading: true }, async () => {
				await this.props.getRoles();
				await this.props.getCapabilities();
				let currentUserRole = _.find(this.props.roles, {
					name: store.get("user").role
				});
				let currentUserCapabilities = this.props.capabilities
					.filter(capability => {
						return currentUserRole.capabilities.includes(
							capability._id
						);
					})
					.map(cap => {
						return cap.name.toLowerCase();
					});
				this.setState({ currentUserCapabilities, childrenLoading: false });
			});
		}
	}

	handleLogout = () => {
		this.props.logout();
		this.setState({ anchorEl: null });
	};

	handleItemClick = item => {
		if (
			item === "admin" ||
			item === "users" ||
			item === "capabilities" ||
			item === "roles"
		)
			this.setState({ content: item });
		else {
			this.setState({ content: item, expand: false, menu: "" });
		}
	};

	handleMenuClose = () => {
		this.setState({ anchorEl: null });
	};

	handleMenuOpen = event => {
		this.setState({ anchorEl: event.currentTarget });
	};

	handleExpand = menu => {
		if (this.state.expand && this.state.menu === menu) {
			this.setState({ expand: false, menu: "" });
		} else {
			this.setState({ expand: true, menu });
		}
	};

	renderMenu() {
		return (
			<Menu
				anchorEl={this.state.anchorEl}
				anchorOrigin={{ vertical: "top", horizontal: "right" }}
				id="primary-account-menu"
				keepMounted
				transformOrigin={{ vertical: "top", horizontal: "right" }}
				open={Boolean(this.state.anchorEl)}
				onClose={this.handleMenuClose}
			>
				<MenuItem onClick={() => this.handleItemClick("account")}>
					<ListItemIcon>
						<AccountCircle />
					</ListItemIcon>
					<Typography variant="h6">Account</Typography>
				</MenuItem>
				<MenuItem onClick={this.handleLogout}>
					<ListItemIcon>
						<i className="material-icons" fontSize="large">
							power_settings_new
						</i>
					</ListItemIcon>
					<Typography variant="h6">Logout</Typography>
				</MenuItem>
			</Menu>
		);
	}

	render() {
		const { classes } = this.props;
		return (
			<div className={classes.root}>
				{this.renderMenu()}
				<CssBaseline />
				<AppBar
					handleItemClick={this.handleItemClick}
					handleLogout={this.handleLogout}
				/>
				<Sidebar
					expanded={this.state.expand}
					menu={this.state.menu}
					selected={this.state.content}
					currentUserCapabilities={this.state.currentUserCapabilities}
					handleItemClick={this.handleItemClick}
					handleExpand={this.handleExpand}
					childrenLoading={this.state.childrenLoading}
				/>
				<PageContent page={this.state.content} />
			</div>
		);
	}
}

Landing.propTypes = {
	classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
	return {
		roles: _.values(state.roles),
		capabilities: _.values(state.capabilities)
	};
};

export default connect(
	mapStateToProps,
	{ logout, getRoles, getCapabilities }
)(withStyles(styles)(Landing));
