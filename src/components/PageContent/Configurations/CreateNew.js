import React from "react";
import { connect } from "react-redux";
import {
  withStyles,
  Paper,
  Typography,
  Grid,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Box,
  LinearProgress
} from "@material-ui/core";
import _ from "lodash";

import options from "content/configurations/options";
import { createConfiguration } from "actions";
import ConfigDetails from "./ConfigDetails";

const styles = theme => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  heading: {
    paddingTop: "10px",
    color: "#3f51b5",
    fontVariant: "small-caps"
  },
  field: {
    width: "90%",
    margin: theme.spacing(2)
  },
  progress: {
    flexGrow: 1
  }
});

const INITIAL_STATE = {
  name: "",
  testArea: "",
  testType: "",
  type: "",
  suiteType: "",
  createProgress: false,
  configFile: "",
  dataFile: "",
  tcFile: "",
  configFiles: [],
  dataFiles: [],
  tcFiles: []
};

class CreateNew extends React.Component {
  state = INITIAL_STATE;

  handleCreate = () => {
    let { name, testArea, testType, suiteType } = this.state;
    name = `${testArea}_${testType.replace(" ", "_")}_${suiteType.replace(
      " ",
      "_"
    )}_${name}`.toUpperCase();
    this.setState({ createProgress: true, name }, async () => {
      await this.props.createConfiguration(
        _.omit(
          this.state,
          "loading",
          "createProgress",
          "configFile",
          "dataFile",
          "tcFile"
        )
      );
      this.setState({
        ...INITIAL_STATE,
        configFiles: [],
        dataFiles: [],
        tcFiles: []
      });
    });
  };

  handleFileSelection = (fileNames, files, type) => {
    let fls = [];
    Object.keys(files).forEach(f => {
      let fileReader = new FileReader();
      fileReader.onload = e => {
        fls.push({
          name: files[f].name,
          data: e.target.result
        });
        if (fls.length === Object.keys(files).length) {
          this.setState({ [type]: fileNames, [`${type}s`]: fls });
        }
      };
      fileReader.readAsDataURL(files[f]);
    });
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    if (e.target.name === "testType") {
      switch (e.target.value) {
        case "Web UI":
        case "Mobile UI":
        case "Api":
          this.setState({
            type: "functional",
            suiteType: "Integration"
          });
          break;
        case "Non Functional":
          this.setState({
            type: "non-functional",
            suiteType: "Regression"
          });
          break;
        default:
          break;
      }
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <Paper elevation={3} className={classes.paper}>
        <Typography variant="h4" gutterBottom className={classes.heading}>
          <Box fontWeight="fontWeightBold">CREATE CONFIGURATION</Box>
        </Typography>
        <div>
          <Grid container spacing={1}>
            <Grid item xs={3}>
              <TextField
                required
                name="name"
                label="Configuration Name"
                margin="dense"
                id="name"
                value={this.state.name}
                className={classes.field}
                onChange={this.handleChange}
                fullWidth
              />
            </Grid>
            <Grid item xs={3}>
              <FormControl className={classes.field} fullWidth>
                <InputLabel htmlFor="testArea" required>
                  Test Area
                </InputLabel>
                <Select
                  onChange={this.handleChange}
                  value={this.state.testArea}
                  inputProps={{
                    name: "testArea",
                    id: "testArea"
                  }}
                >
                  <MenuItem value="" disabled>
                    Test Area
                  </MenuItem>
                  {options.testArea.map(option => {
                    return (
                      <MenuItem value={option.value} key={option.id}>
                        {option.name}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={3}>
              <FormControl className={classes.field} fullWidth>
                <InputLabel htmlFor="testType" required>
                  Test Type
                </InputLabel>
                <Select
                  onChange={this.handleChange}
                  value={this.state.testType}
                  inputProps={{
                    name: "testType",
                    id: "testType"
                  }}
                >
                  <MenuItem value="" disabled>
                    Test Type
                  </MenuItem>
                  {options.testType.map(option => {
                    return (
                      <MenuItem value={option.value} key={option.id}>
                        {option.name}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={3}>
              <FormControl className={classes.field} fullWidth>
                <InputLabel htmlFor="suiteType" required>
                  Test Suite Type
                </InputLabel>
                <Select
                  onChange={this.handleChange}
                  value={this.state.suiteType}
                  inputProps={{
                    name: "suiteType",
                    id: "suiteType"
                  }}
                >
                  <MenuItem value="" disabled>
                    Test Suite Type
                  </MenuItem>
                  {options.suiteType.map(option => {
                    const disabled =
                      this.state.type !== option.type && option.type !== "both";
                    return (
                      <MenuItem
                        value={option.value}
                        key={option.id}
                        disabled={disabled}
                      >
                        {option.name}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <ConfigDetails
                handleFiles={this.handleFileSelection}
                values={this.state}
                create={this.handleCreate}
              />
            </Grid>
          </Grid>
          {this.state.createProgress && <LinearProgress />}
        </div>
      </Paper>
    );
  }
}

export default connect(
  null,
  { createConfiguration }
)(withStyles(styles)(CreateNew));
