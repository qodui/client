import React from "react";
import { withStyles, Grid, TextField, Button } from "@material-ui/core";

const styles = theme => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: theme.spacing(2),
    marginBottom: theme.spacing(1)
  },
  field: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingBottom: theme.spacing(2)
  },
  file_field: {
    width: "75%",
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(0.5),
    paddingBottom: theme.spacing(0.5)
  },
  button: {
    margin: theme.spacing(1)
  },
  input: {
    display: "none"
  }
});

class ConfigDetails extends React.Component {
  getFileNames(files) {
    return Object.keys(files)
      .map(file => {
        return files[file].name;
      })
      .join("; ");
  }

  getSuiteName() {
    let { name, testArea, testType, suiteType } = this.props.values;
    if (name && testArea && testType && suiteType) {
      if (
        name === "" &&
        testArea === "" &&
        testType === "" &&
        suiteType === ""
      ) {
        return "";
      } else {
        return `${testArea}_${testType.replace(" ", "_")}_${suiteType.replace(
          " ",
          "_"
        )}_${name}`.toUpperCase();
      }
    } else {
      return "";
    }
  }

  getHelpertext(type){
    if(type==="dataFiles"){
      const {values} = this.props;
      console.log(`test type: ${values.testType}`);
      return values.testType === "Non Functional" ?"Choose test data file(s)": "Choose object repository file, capabilities file etc";
    }
  }

  isEmpty(value) {
    return value === "";
  }

  shouldDisableButton() {
    let {
      name,
      testArea,
      testType,
      suiteType,
      configFile,
      dataFile,
      tcFile
    } = this.props.values;
    return (
      this.isEmpty(name) ||
      this.isEmpty(testArea) ||
      this.isEmpty(testType) ||
      this.isEmpty(suiteType) ||
      this.isEmpty(configFile) ||
      //this.isEmpty(dataFile) ||
      this.isEmpty(tcFile)
    );
  }

  handleFileSelection = (files, type) => {
    this.props.handleFiles(this.getFileNames(files), files, type);
  };

  render() {
    const { classes, values, create } = this.props;
    return (
      <>
        <Grid
          container
          alignItems="flex-start"
          justify="flex-end"
          direction="row"
        >
          <Grid item xs={6}>
            <TextField
              className={classes.field}
              id="suiteName"
              helperText="Test Suite Name - Read Only"
              fullWidth
              margin="normal"
              readOnly
              value={this.getSuiteName()}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              className={classes.file_field}
              id="configFile"
              placeholder="Config File *"
              helperText="Choose config file"
              margin="normal"
              readOnly
              value={values.configFile}
            />
            <input
              accept="*"
              className={classes.input}
              id="uploadConfig"
              type="file"
              onChange={e =>
                this.handleFileSelection(e.target.files, "configFile")
              }
            />
            <label htmlFor="uploadConfig">
              <Button
                color="primary"
                variant="contained"
                component="span"
                className={classes.button}
              >
                CHOOSE FILE
              </Button>
            </label>
          </Grid>
          <Grid item xs={6}>
            <TextField
              className={classes.file_field}
              id="dataFile"
              placeholder="Data File(s)"
              helperText={this.getHelpertext("dataFiles")}
              margin="normal"
              readOnly
              value={values.dataFile}
            />
            <input
              accept="*"
              className={classes.input}
              id="uploadData"
              type="file"
              multiple
              onChange={e =>
                this.handleFileSelection(e.target.files, "dataFile")
              }
              required = {false}
            />
            <label htmlFor="uploadData">
              <Button
                color="primary"
                variant="contained"
                component="span"
                className={classes.button}
              >
                CHOOSE FILE(S)
              </Button>
            </label>
          </Grid>
          <Grid item xs={6}>
            <TextField
              className={classes.file_field}
              id="tcFile"
              placeholder="Test Case File(s) *"
              helperText="Choose test case file(s)"
              margin="normal"
              readOnly
              value={values.tcFile}
            />
            <input
              accept="*"
              className={classes.input}
              id="uploadTc"
              type="file"
              multiple
              onChange={e => this.handleFileSelection(e.target.files, "tcFile")}
            />
            <label htmlFor="uploadTc">
              <Button
                color="primary"
                variant="contained"
                component="span"
                className={classes.button}
              >
                CHOOSE FILE(S)
              </Button>
            </label>
          </Grid>
        </Grid>
        <Grid
          container
          alignItems="flex-start"
          justify="flex-end"
          direction="row"
        >
          <Button
            className={classes.button}
            onClick={create}
            color="primary"
            variant="contained"
            fullWidth
            disabled={this.shouldDisableButton()}
          >
            CREATE
          </Button>
        </Grid>
      </>
    );
  }
}

export default withStyles(styles)(ConfigDetails);
