import React from "react";
import { connect } from "react-redux";
import {
	withStyles,
	Typography,
	Box,
	Grid,
	LinearProgress
} from "@material-ui/core";
import Table from "mui-datatables";
import _ from "lodash";
import store from "store";
import { toast } from "react-toastify";

import table from "content/configurations/table";
import {
	getConfigurations,
	deleteConfiguration,
	clearConfigurations
} from "actions";
import CreateNew from "./CreateNew";

const styles = theme => ({
	paper: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center"
	},
	heading: {
		paddingTop: "10px",
		color: "#3f51b5",
		fontVariant: "small-caps"
	},
	tableHeading: {
		paddingTop: "10px",
		color: "#3f51b5",
		fontVariant: "small-caps"
	},
	field: {
		width: "90%",
		margin: theme.spacing(2)
	},
	temp: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		paddingTop: "10px",
		color: "#3f51b5",
		fontVariant: "small-caps"
	}
});

const INITIAL_STATE = {
	loading: false
};

class Configurations extends React.Component {
	state = INITIAL_STATE;

	componentDidMount() {
		window.scrollTo(0, 0);
		this.setState({ loading: true }, async () => {
			await this.props.getConfigurations();
			this.setState({ loading: false });
		});
	}

	componentWillUnmount() {
		this.props.clearConfigurations();
	}

	handleDelete = rowsDeleted => {
		this.setState({ loading: true }, async () => {
			const confsToBeDeleted = rowsDeleted.data.map(
				d => this.props.configurations[d.dataIndex].name
			);
			const idsTobeDeleted = rowsDeleted.data.map(
				d => this.props.configurations[d.dataIndex]._id
			);
			await this.props.deleteConfiguration(
				confsToBeDeleted,
				idsTobeDeleted
			);
			this.setState({ loading: false });
		});
	};

	handleTableChange = (action, tableState) => {
		if (action === "rowDelete") {
			tableState.page = 0;
		}
	};

	render() {
		const { classes } = this.props;
		const { capabilities } = store.get("user");
		let create =
			_.findIndex(capabilities, cap => {
				return cap.name === "Create_Configuration";
			}) !== -1;
		let del =
			_.findIndex(capabilities, cap => {
				return cap.name === "Delete_Configuration";
			}) !== -1;
		return (
			<Grid container spacing={2}>
				{create && (
					<Grid item xs={12}>
						<CreateNew />
					</Grid>
				)}
				<Grid item xs={12}>
					<div className={classes.wrapper}>
						{this.state.loading && <LinearProgress />}
						<Table
							title={
								<Typography
									variant="h4"
									gutterBottom
									className={classes.tableHeading}
								>
									<Box fontWeight="fontWeightBold">
										CONFIGURATIONS
									</Box>
								</Typography>
							}
							columns={table.columns}
							data={this.props.configurations}
							options={{
								...table.options,
								onRowsDelete: rowsDeleted => {
									if (del) this.handleDelete(rowsDeleted);
									else {
										toast.error(
											"Unauthorized to perform this operation"
										);
										return false;
									}
								},
								onTableChange: (action, tableState) => {
									this.handleTableChange(action, tableState);
								}
							}}
						/>
					</div>
				</Grid>
			</Grid>
		);
	}
}

const mapStateToProps = state => {
	return {
		configurations: _.values(state.configurations)
	};
};

export default connect(
	mapStateToProps,
	{
		getConfigurations,
		deleteConfiguration,
		clearConfigurations
	}
)(withStyles(styles)(Configurations));
