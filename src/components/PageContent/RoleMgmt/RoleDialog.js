import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
	List,
	ListSubheader,
	ListItem,
	ListItemText,
	ListItemSecondaryAction,
	Switch
} from "@material-ui/core";
import _ from "lodash";
import { toast } from "react-toastify";

let INITIAL_STATE = {
	name: "",
	description: "",
	capabilities: []
};

class RoleDialog extends React.Component {
	state = INITIAL_STATE;

	componentDidMount() {
		this.setState(this.props.data);
	}

	handleSave = () => {
		let { name, description } = this.state;
		if (name === "" || description === "") {
			toast.warn("Please fill all highlighted fields");
		} else {
			this.props.handleSave(this.state);
			INITIAL_STATE = {
				name: "",
				description: "",
				capabilities: []
			};
		}
	};

	handleTextChange = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	handleCapChange = e => {
		let { capabilities } = this.state;
		if (e.target.checked) {
			if (!capabilities.includes(e.target.value)) {
				capabilities.push(e.target.value);
			}
		} else {
			capabilities = _.pull(capabilities, e.target.value);
		}
		this.setState({ capabilities });
	};

	renderCapabilitiesList() {
		let { capabilities } = this.props;
		if (capabilities) {
			return capabilities.map(cap => {
				return (
					<ListItem key={cap._id}>
						<ListItemText primary={cap.name} />
						<ListItemSecondaryAction>
							<Switch
								edge="end"
								value={cap._id}
								onChange={this.handleCapChange}
								defaultChecked={
									this.state.capabilities.indexOf(cap._id) !==
									-1
										? true
										: false
								}
							/>
						</ListItemSecondaryAction>
					</ListItem>
				);
			});
		} else {
			return <div>Loading...</div>;
		}
	}

	render() {
		return (
			<Dialog
				open={this.props.open}
				onClose={this.props.handleClose}
				aria-labelledby="form-dialog-title"
			>
				<DialogTitle id="form-dialog-title">
					{this.props.title}
				</DialogTitle>
				<DialogContent>
					<DialogContentText>
						Enter Role Details below:
					</DialogContentText>
					<TextField
						name="name"
						autoFocus
						required
						error={this.state.name === ""}
						margin="dense"
						id="name"
						label="Role Name"
						fullWidth
						value={this.state.name}
						onChange={this.handleTextChange}
					/>
					<TextField
						multiline
						name="description"
						required
						error={this.state.description === ""}
						margin="dense"
						id="description"
						label="Role Description"
						fullWidth
						value={this.state.description}
						onChange={this.handleTextChange}
					/>
					<List
						subheader={<ListSubheader>Capabilities</ListSubheader>}
					>
						{this.renderCapabilitiesList()}
					</List>
				</DialogContent>
				<DialogActions>
					<Button onClick={this.props.handleClose} color="secondary">
						Cancel
					</Button>
					<Button onClick={this.handleSave} color="primary">
						Save
					</Button>
				</DialogActions>
			</Dialog>
		);
	}
}

export default RoleDialog;
