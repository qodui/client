import React from "react";
import { connect } from "react-redux";
import _ from "lodash";
import MaterialTable from "material-table";
import { AddBox } from "@material-ui/icons";

import {
	editRole,
	addRole,
	deleteRole,
	getRoles,
	clearRoles,
	getCapabilities,
	clearCapabilities
} from "actions";
import RoleDialog from "./RoleDialog";

class RoleMgmt extends React.Component {
	state = { open: false, operation: "", title: "", id: "", isLoading: false };
	initialData = {};

	componentDidMount() {
		window.scrollTo(0, 0);
		this.setState({ isLoading: true }, async () => {
			await this.props.getRoles();
			await this.props.getCapabilities();
			this.setState({ isLoading: false });
		});
	}

	componentWillUnmount() {
		this.props.clearRoles();
		this.props.clearCapabilities();
	}

	handleDialogClose = () => {
		this.setState({ open: false, operation: "" });
	};

	addRoleDialog() {
		this.setState({ open: true, operation: "add", title: "ADD ROLE" });
	}

	editRoleDialog(id) {
		this.setState({
			open: true,
			operation: "edit",
			title: "EDIT ROLE",
			id: id
		});
	}

	handleDialogSave = async newData => {
		this.setState({ open: false, isLoading: true });
		if (this.state.operation === "add") {
			await this.props.addRole(newData);
		} else if (this.state.operation === "edit") {
			await this.props.editRole(this.state.id, newData);
		}
		this.setState({ open: false, operation: "", isLoading: false });
	};

	renderDialog() {
		if (this.state.open) {
			return (
				<RoleDialog
					open={this.state.open}
					handleClose={this.handleDialogClose}
					handleSave={this.handleDialogSave}
					title={this.state.title}
					data={this.initialData}
					capabilities={this.props.capabilities}
				/>
			);
		}
	}

	render() {
		return (
			<div style={{ maxWidth: "100%" }}>
				<MaterialTable
					isLoading={this.state.isLoading}
					editable={{
						onRowDelete: oldData =>
							this.props.deleteRole(oldData._id)
					}}
					actions={[
						{
							icon: "edit",
							tooltip: "Edit Role",
							onClick: (event, rowData) => {
								let {
									name,
									description,
									capabilities
								} = rowData;
								let _capabilities = this.props.capabilities
									.filter(c => {
										if (capabilities.includes(c._id))
											return c;
										else {
											return undefined;
										}
									})
									.map(c => {
										return c._id;
									});
								capabilities = _capabilities;
								this.initialData = {
									name,
									description,
									capabilities
								};
								this.editRoleDialog(rowData._id);
							}
						},
						{
							icon: () => <AddBox />,
							tooltip: "Add Role",
							isFreeAction: true,
							onClick: event => {
								this.initialData = {};
								this.addRoleDialog();
							}
						}
					]}
					options={{
						search: true,
						actionsColumnIndex: -1,
						headerStyle: {
							fontSize: "14pt",
							color: "white",
							fontWeight: "bolder",
							background: "#3f51b5"
						},
						pageSizeOptions: [5, 10]
					}}
					title="ROLE MANAGEMENT"
					data={this.props.roles}
					columns={[
						{
							title: "Role Name",
							field: "name",
							cellStyle: { fontSize: 16 }
						},
						{
							title: "Role Description",
							field: "description",
							cellStyle: { fontSize: 16 }
						},
						{
							title: "# Capabilities",
							field: `capabilities.length`,
							searchable: false,
							cellStyle: { fontSize: 16 }
						}
					]}
				/>
				{this.renderDialog()}
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		roles: _.values(state.roles),
		capabilities: _.values(state.capabilities)
	};
};

export default connect(
	mapStateToProps,
	{
		editRole,
		addRole,
		deleteRole,
		getRoles,
		clearRoles,
		getCapabilities,
		clearCapabilities
	}
)(RoleMgmt);
