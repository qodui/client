import React from "react";
import { connect } from "react-redux";
import MaterialTable from "material-table";
import _ from "lodash";
import { toast } from "react-toastify";
import WarningIcon from "@material-ui/icons/Warning";
import { Snackbar, withStyles, SnackbarContent } from "@material-ui/core";

import {
	addCapability,
	editCapability,
	deleteCapability,
	getCapabilities,
	clearCapabilities
} from "actions";

const styles = theme => ({
	icon: {
		fontSize: 20,
		opacity: 0.9,
		marginRight: theme.spacing(1)
	},
	message: {
		display: "flex",
		alignItems: "center"
	},
	content: {
		fontSize: 18,
		backgroundColor: theme.palette.error.dark
	}
});

class CapMgmt extends React.Component {
	state = { isLoading: false };

	componentDidMount() {
		window.scrollTo(0, 0);
		this.setState({ isLoading: true }, async () => {
			await this.props.getCapabilities();
			this.setState({ isLoading: false });
		});
	}

	componentWillUnmount() {
		this.props.clearCapabilities();
	}

	render() {
		const { classes } = this.props;
		return (
			<div style={{ maxWidth: "100%" }}>
				<MaterialTable
					isLoading={this.state.isLoading}
					editable={{
						onRowAdd: newData =>
							new Promise(async (resolve, reject) => {
								if (
									newData.name !== undefined &&
									newData.description !== undefined
								)
									await this.props.addCapability(newData);
								else
									toast.warn(
										"Name/Description cannot be empty"
									);
								resolve();
							}),
						onRowUpdate: (newData, oldData) =>
							new Promise(async (resolve, reject) => {
								if (
									newData.name !== "" &&
									newData.description !== ""
								)
									await this.props.editCapability(
										oldData._id,
										_.omit(newData, "_id")
									);
								else
									toast.warn(
										"Name/Description cannot be empty"
									);
								resolve();
							}),
						onRowDelete: oldData =>
							this.props.deleteCapability(oldData._id)
					}}
					options={{
						search: true,
						actionsColumnIndex: -1,
						headerStyle: {
							fontSize: "14pt",
							color: "white",
							fontWeight: "bolder",
							background: "#3f51b5"
						},
						pageSizeOptions: [5, 10]
					}}
					title="CAPABILITY MANAGEMENT"
					data={this.props.capabilities}
					columns={[
						{
							title: "Name",
							field: "name",
							cellStyle: { fontSize: 16 }
						},
						{
							title: "Description",
							field: "description",
							cellStyle: { fontSize: 16 }
						}
					]}
				/>
				<Snackbar
					anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
					open={true}
				>
					<SnackbarContent
						className={classes.content}
						message={
							<span className={classes.message}>
								<WarningIcon className={classes.icon} />
								Changing capabilities may need code change and
								re-deploy. Change only if you know what you are
								doing!
							</span>
						}
					/>
				</Snackbar>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		capabilities: _.values(state.capabilities)
	};
};

export default connect(
	mapStateToProps,
	{
		editCapability,
		deleteCapability,
		addCapability,
		getCapabilities,
		clearCapabilities
	}
)(withStyles(styles)(CapMgmt));
