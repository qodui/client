import React from "react";
import {
	Paper,
	TextField,
	Grid,
	CssBaseline,
	Typography,
	Button,
	Box
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import store from "store";
import { connect } from "react-redux";
import _ from "lodash";
import SaveIcon from "@material-ui/icons/Save";
import { toast } from "react-toastify";
import Slide from "@material-ui/core/Slide";

import { editUser } from "actions";

const styles = theme => ({
	paper: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center"
	},
	form: {
		width: "100%", // Fix IE 11 issue.
		marginTop: theme.spacing(1)
	},
	field: {
		marginLeft: theme.spacing(3),
		maxWidth: "90%"
	},
	chip: {
		margin: theme.spacing(0.5),
		verticalAlign: "middle"
	},
	chipsField: {
		display: "flex",
		justifyContent: "left",
		flexWrap: "wrap",
		padding: theme.spacing(3)
	},
	label: {
		verticalAlign: "middle",
		padding: theme.spacing(2)
	},
	submit: {
		margin: theme.spacing(1),
		padding: theme.spacing(2)
	},
	leftIcon: {
		marginRight: theme.spacing(1)
	},
	note: {
		maxWidth: "97%"
	},
	heading: {
		paddingTop: "10px",
		color: "#3f51b5",
		fontVariant: "small-caps"
	},
	subheading: { color: "#b71c1c" },
	button: {
		margin: theme.spacing(3),
		display: "flex",
		alignItems: "center",
		justifyContent: "center"
	},
	resize: {
		fontSize: 20
	}
});

let INITIAL_STATE = {
	name: "",
	password: "",
	userId: "",
	open: false,
	role: ""
};
class Account extends React.Component {
	state = INITIAL_STATE;

	componentDidMount() {
		window.scrollTo(0, 0);
		let { name, userId, role } = store.get("user");
		this.setState({
			name,
			userId,
			role
		});
	}

	handleChange = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	handleSubmit = () => {
		if (this.state.name === "" || this.state.password === "") {
			toast.error("User ID and/or Password cannot be empty.");
		} else {
			INITIAL_STATE.name = this.state.name;
			this.props.editUser(store.get("user")._id, this.state);
			this.setState(INITIAL_STATE);
		}
	};

	handleToolTipClose = () => {
		this.setState({ open: false });
	};

	render() {
		const { classes } = this.props;
		return (
			<Slide
				direction="up"
				timeout={750}
				in={true}
				mountOnEnter
				unmountOnExit
			>
				<Paper elevation={3} className={classes.paper}>
					<Typography
						variant="h4"
						gutterBottom
						className={classes.heading}
					>
						<Box
							letterSpacing={10}
							m={1}
							fontWeight="fontWeightBold"
						>
							Account Information
						</Box>
					</Typography>
					<Typography
						variant="body1"
						gutterBottom
						className={classes.subheading}
					>
						<Box
							letterSpacing={2}
							fontSize={16}
						>
							You can modify the <strong>User Name</strong> and{" "}
							<strong>Password</strong> only. If you need a change
							in <strong>Role</strong> or <strong>User Id</strong>
							, please contact the Administrator.
						</Box>
					</Typography>
					<form className={classes.form} autoComplete="off">
						<Grid container component="main">
							<CssBaseline />
							<Grid item xs={6} md={6} lg={6}>
								<TextField
									InputLabelProps={{
										style: { fontSize: 16 }
									}}
									InputProps={{
										style: { fontSize: 16 }
									}}
									className={classes.field}
									autoComplete="off"
									variant="standard"
									margin="normal"
									required
									fullWidth
									id="name"
									label="User Name"
									name="name"
									value={this.state.name}
									onChange={this.handleChange}
								/>
							</Grid>
							<Grid item xs={6} md={6} lg={6}>
								<TextField
									InputLabelProps={{
										style: { fontSize: 16 }
									}}
									InputProps={{
										style: { fontSize: 16, color: "black" }
									}}
									className={classes.field}
									autoComplete="off"
									variant="standard"
									margin="normal"
									fullWidth
									disabled
									id="userId"
									label="User ID"
									name="userId"
									value={this.state.userId}
								/>
							</Grid>
							<Grid item xs={6} md={6} lg={6}>
								<TextField
									InputLabelProps={{
										style: { fontSize: 16 }
									}}
									InputProps={{
										style: { fontSize: 16 }
									}}
									className={classes.field}
									autoComplete="off"
									variant="standard"
									margin="normal"
									required
									fullWidth
									id="password"
									label="Password"
									name="password"
									value={this.state.password}
									type="password"
									onChange={this.handleChange}
									helperText="Current password or New password"
								/>
							</Grid>
							<Grid item xs={6} md={6} lg={6}>
								<TextField
									InputLabelProps={{
										style: { fontSize: 16 }
									}}
									InputProps={{
										style: { fontSize: 16, color: "black" }
									}}
									className={classes.field}
									autoComplete="off"
									variant="standard"
									margin="normal"
									fullWidth
									disabled
									id="role"
									label="Role"
									name="role"
									value={this.state.role}
								/>
							</Grid>
						</Grid>
						<Button
							variant="contained"
							color="primary"
							size="large"
							className={classes.button}
							onClick={this.handleSubmit}
						>
							<SaveIcon className={classes.leftIcon} />
							SAVE
						</Button>
					</form>
				</Paper>
			</Slide>
		);
	}
}

Account.propTypes = {
	classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
	return {
		capabilities: _.values(state.capabilities),
		user: state.users[store.get("user")._id]
	};
};

export default connect(
	mapStateToProps,
	{ editUser }
)(withStyles(styles)(Account));
