import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";

import UserMgmt from "./UserMgmt";
import CapMgmt from "./CapMgmt";
import RoleMgmt from "./RoleMgmt";
import Dashboard from "./Dashboard";
import Account from "./Account";
import Environments from "./Environments";
import Configurations from "./Configurations";
import Executions from "./Executions";
import Reports from "./Reports";

const styles = theme => ({
	content: {
		flexGrow: 1,
		padding: theme.spacing(3)
	},
	toolbar: theme.mixins.toolbar
});

class PageContent extends React.Component {
	renderContent(page) {
		switch (page) {
			case "users":
				return <UserMgmt />;
			case "capabilities":
				return <CapMgmt />;
			case "roles":
				return <RoleMgmt />;
			case "dashboard":
				return <Dashboard />;
			case "account":
				return <Account />;
			case "environment":
				return <Environments />;
			case "configurations":
				return <Configurations />;
			case "executions":
				return <Executions />;
			case "reports":
				return <Reports />;
			default:
				return null;
		}
	}

	render() {
		const { classes, page } = this.props;
		return (
			<main className={classes.content}>
				<div className={classes.toolbar} />
				{this.renderContent(page)}
			</main>
		);
	}
}

PageContent.propTypes = {
	classes: PropTypes.object.isRequired,
	page: PropTypes.string.isRequired
};

export default withStyles(styles)(PageContent);
