import React from "react";
import { connect } from "react-redux";
import MaterialTable from "material-table";
import { AddBox } from "@material-ui/icons";
import _ from "lodash";

import UserDialog from "./UserDialog";
import {
	deleteUser,
	editUser,
	addUser,
	getUsers,
	getRoles,
	clearUsers,
	clearRoles
} from "actions";

class UserMgmt extends React.Component {
	state = { open: false, operation: "", title: "", id: "", isLoading: false };
	initialData = {};
	componentDidMount() {
		window.scrollTo(0, 0);
		this.setState({ isLoading: true }, async () => {
			await this.props.getUsers();
			await this.props.getRoles();
			this.setState({ isLoading: false });
		});
	}

	componentWillUnmount() {
		this.props.clearUsers();
		this.props.clearRoles();
	}

	handleDialogClose = () => {
		this.setState({ open: false, operation: "" });
	};

	addUserDialog() {
		this.setState({ open: true, operation: "add", title: "ADD USER" });
	}

	editUserDialog(id) {
		this.setState({
			open: true,
			operation: "edit",
			title: "EDIT USER",
			id: id
		});
	}

	handleDialogSave = async newData => {
		this.setState({ isLoading: true, open: false });
		if (this.state.operation === "add") {
			await this.props.addUser(newData);
		} else if (this.state.operation === "edit") {
			if (newData.password === "") newData = _.omit(newData, "password");
			newData = _.omit(newData, "_id");
			await this.props.editUser(this.state.id, newData);
		}
		this.setState({ operation: "", isLoading: false });
	};

	renderDialog() {
		if (this.state.open) {
			return (
				<UserDialog
					open={this.state.open}
					handleClose={this.handleDialogClose}
					handleSave={this.handleDialogSave}
					title={this.state.title}
					data={this.initialData}
					roles={this.props.roles}
				/>
			);
		}
	}

	render() {
		return (
			<div style={{ maxWidth: "100%" }}>
				<MaterialTable
					isLoading={this.state.isLoading}
					editable={{
						onRowDelete: oldData =>
							this.props.deleteUser(oldData._id)
					}}
					actions={[
						{
							icon: "edit",
							tooltip: "Edit User",
							onClick: (event, rowData) => {
								this.initialData = _.omit(
									rowData,
									"_id",
									"password",
									"tableData"
								);
								this.editUserDialog(rowData._id);
							}
						},
						{
							icon: () => <AddBox />,
							tooltip: "Add User",
							isFreeAction: true,
							onClick: event => {
								this.initialData = {};
								this.addUserDialog();
							}
						}
					]}
					options={{
						search: true,
						actionsColumnIndex: -1,
						headerStyle: {
							fontSize: "14pt",
							color: "white",
							fontWeight: "bolder",
							background: "#3f51b5"
						},
						pageSizeOptions: [5, 10]
					}}
					title="USER MANAGEMENT"
					data={this.props.users}
					columns={[
						{
							title: "User ID",
							field: "userId",
							cellStyle: { fontSize: 16 }
						},
						{
							title: "Name",
							field: "name",
							cellStyle: { fontSize: 16 }
						},
						{
							title: "Role",
							field: "role",
							cellStyle: { fontSize: 16 }
						}
					]}
				/>
				{this.renderDialog()}
			</div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		users: _.values(state.users),
		roles: _.values(state.roles)
	};
};

export default connect(
	mapStateToProps,
	{
		deleteUser,
		editUser,
		addUser,
		getUsers,
		getRoles,
		clearUsers,
		clearRoles
	}
)(UserMgmt);
