import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { FormControl, InputLabel, Select, MenuItem } from "@material-ui/core";
import { toast } from "react-toastify";

const INITIAL_STATE = {
	userId: "",
	name: "",
	password: "",
	role: ""
};

class UserDialog extends React.Component {
	state = INITIAL_STATE;

	componentDidMount() {
		this.setState(this.props.data);
	}

	handleSave = () => {
		let { name, userId, password, role } = this.state;
		if (
			name === "" ||
			userId === "" ||
			role === "" ||
			(this.props.title === "ADD USER" && password === "")
		) {
			toast.warn("Please fill all the highlighted fields.");
		} else {
			this.props.handleSave(this.state);
		}
	};

	handleTextChange = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	render() {
		return (
			<Dialog
				open={this.props.open}
				onClose={this.props.handleClose}
				aria-labelledby="form-dialog-title"
			>
				<DialogTitle id="form-dialog-title">
					{this.props.title}
				</DialogTitle>
				<DialogContent>
					<DialogContentText>
						Enter User Details below:
					</DialogContentText>
					<TextField
						name="userId"
						required
						error={this.state.userId === ""}
						autoFocus
						margin="dense"
						id="userId"
						label="User ID"
						fullWidth
						value={this.state.userId}
						onChange={this.handleTextChange}
					/>
					<TextField
						name="name"
						required
						error={this.state.name === ""}
						margin="dense"
						id="name"
						label="User Name"
						fullWidth
						value={this.state.name}
						onChange={this.handleTextChange}
					/>
					<TextField
						name="password"
						error={
							this.state.password === "" &&
							this.props.title === "ADD USER"
						}
						margin="dense"
						id="password"
						label="Password"
						type="password"
						fullWidth
						onChange={this.handleTextChange}
					/>
					<FormControl
						fullWidth
						required
						error={this.state.role === ""}
					>
						<InputLabel htmlFor="role">Role</InputLabel>
						<Select
							value={this.state.role}
							onChange={this.handleTextChange}
							error={this.state.role === ""}
							inputProps={{
								name: "role",
								id: "role"
							}}
						>
							{this.props.roles &&
								this.props.roles.map(role => {
									return (
										<MenuItem
											value={role.name}
											key={role._id}
										>
											{role.name}
										</MenuItem>
									);
								})}
						</Select>
					</FormControl>
				</DialogContent>
				<DialogActions>
					<Button onClick={this.props.handleClose} color="secondary">
						Cancel
					</Button>
					<Button onClick={this.handleSave} color="primary">
						Save
					</Button>
				</DialogActions>
			</Dialog>
		);
	}
}

export default UserDialog;
