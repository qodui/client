import React from "react";
import PropTypes from "prop-types";
import { Button, withStyles, Typography } from "@material-ui/core";
import { connect } from "react-redux";
import { getLogs, clearLogs } from "actions";
import _ from "lodash";
import { CircularProgress } from "@material-ui/core";
import { LazyLog } from "react-lazylog";

import MyModal from "layout/MyModal";

const styles = theme => ({
  content: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    padding: theme.spacing(2),
    whiteSpace: "pre-line",
    fontFamily: "Monospace",
    fontSize: "1.5rem"
  },
  logDiv: {
	  minHeight: "400px",
	  minWidth: "1000px"
  }
});

class ViewLog extends React.Component {
  state = { open: false };

  componentDidMount() {}

  componentWillUnmount() {}

  openModal = async () => {
    this.props.getLogs(this.props.job_id);
    this.setState({
      open: true
    });
  };

  getContent() {
    const { classes, logs } = this.props;
    let log = logs.join("\n");

    if (this.state.open){console.log(`logs: ${log}`);}
    return _.isEmpty(logs) ? (
      <CircularProgress />
    ) : (
      // <Typography
      // 	variant="body1"
      // 	display="block"
      // 	gutterBottom
      // 	className={classes.content}
      // >
      // 	{logs}
      // </Typography>
	  
      <div className={classes.logDiv}>  
		<LazyLog
          extraLines={1}
          enableSearch
          text={log}
          caseInsensitive
          rowHeight={15}
          overscanRowCount={2}
        />
      </div>
    );
  }

  render() {
    return (
      <>
        <Button
          variant="outlined"
          color="secondary"
          size="small"
          onClick={this.openModal}
        >
          View Logs
        </Button>
        <MyModal
          open={this.state.open}
          onClose={() => this.setState({ open: false })}
          title={this.props.title}
          content={this.getContent()}
          actionButtons={
            <Button
              onClick={() => {
                this.props.clearLogs();
                this.setState({ open: false });
              }}
              color="primary"
              size="large"
              variant="outlined"
            >
              CLOSE
            </Button>
          }
        />
      </>
    );
  }
}

ViewLog.propTypes = {
  classes: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  job_id: PropTypes.string.isRequired
};

const mapStateToProps = state => {
  return {
    logs: state.logs.logviewer_content?state.logs.logviewer_content:[]
  };
};

export default connect(mapStateToProps, {
  getLogs,
  clearLogs
})(withStyles(styles)(ViewLog));
