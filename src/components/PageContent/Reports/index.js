import React from "react";
import { connect } from "react-redux";
import {
  withStyles,
  Typography,
  Box,
  Grid,
  LinearProgress,
  Tabs,
  Tab
} from "@material-ui/core";
import Table from "mui-datatables";
import _ from "lodash";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import store from "store";
import { toast } from "react-toastify";
import { getExecutions, clearExecutions } from "actions";
import table from "content/reports/table";

const styles = theme => ({
  div_root: {
    minHeight: "100%"
  },
  heading: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    paddingTop: "10px",
    color: "#3f51b5",
    fontVariant: "small-caps",
    fontSize: "16px"
  },
  tableHeading: {
    paddingTop: "10px",
    color: "#3f51b5",
    fontVariant: "small-caps",
    align: "center"
  },
  wrapper: {
    maxWidth: "97%",
    marginLeft: "20px",
    minHeight: "500px"
  }
});

const INITIAL_STATE = {
  loading: false,
  executions: null,
  open: false
};

class Reports extends React.Component {
  state = INITIAL_STATE;
  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MUIDataTableBodyCell: {
          root: {
            fontSize: "1.0rem"
          }
        },
        MUIDataTableHeadCell: {
          root: {
            fontSize: "1.2rem",
            fontWeight: "bold",
            fontVariant: "small-caps",
            color: "#3f51b5"
          }
        }
      }
    });

  componentDidMount() {
    window.scrollTo(0, 0);
    this.setState({ loading: true, executions: null }, async () => {
      await this.props.getExecutions(0, "reports");
      this.setState({ loading: false });
    });
  }

  componentWillUnmount() {
    this.props.clearExecutions();
  }

  handleTableChange = (action, tableState) => {
    if (action === "rowDelete") {
      tableState.page = 0;
    }
  };
  render() {
    const { classes, executions } = this.props;
    const { loading } = this.state;
    const { capabilities } = store.get("user");
    let del =
      _.findIndex(capabilities, cap => {
        return cap.name === "Delete_Environment";
      }) !== -1;

    return (
      /*<Paper elevation={3} className={classes.paper}>
				<Typography
					variant="h4"
					gutterBottom
					className={classes.heading}
				>
					<Box fontWeight="fontWeightBold">REPORTS</Box>
				</Typography>
			</Paper>*/
      <div className={classes.div_root}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <div className={classes.wrapper}>
              {loading && <LinearProgress />}
              <MuiThemeProvider theme={this.getMuiTheme()}>
                <Table
                  title={
                    <Typography
                      variant="h4"
                      gutterBottom
                      className={classes.tableHeading}
                    >
                      <Box fontWeight="fontWeightBold">REPORTS</Box>
                    </Typography>
                  }
                  columns={table.columns}
                  data={this.props.executions}
                  options={{
                    ...table.options,
                    onRowsDelete: rowsDeleted => {
                      if (del) this.handleDelete(rowsDeleted);
                      else {
                        toast.error("Unauthorized to perform this operation");
                        return false;
                      }
                    },
                    onTableChange: (action, tableState) => {
                      this.handleTableChange(action, tableState);
                    }
                  }}
                />
              </MuiThemeProvider>
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    executions: _.values(state.executions)
  };
};

export default connect(mapStateToProps, {
  getExecutions,
  clearExecutions
})(withStyles(styles)(Reports));
