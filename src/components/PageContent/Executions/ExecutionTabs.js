import React from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { withStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core/";
import TabPanel from "./TabPanel";
import { connect } from "react-redux";
import { getExecutions, clearExecutions } from "actions";
import _ from "lodash";

const styles = theme => ({
  root: {
    flexGrow: 1,
    display: "flex",
    minWidth: "100%",
    maxheight: "1000px",
    minHeight: "500px"
  },
  tabs: {
    borderRight: `1px solid rgba(0, 0, 0, 0.12)`,
    minWidth: "20%",
    paddingBottom: "10px",
    maxHeight: "500px"
  }
});

const INITIAL_STATE = {
  value: 0,
  index: "",
  executions: "",
  execution: ""
};

class ExecutionTabs extends React.Component {
  state = INITIAL_STATE;

  componentDidMount() {
    this.props.getExecutions(0, "executions");
  }

  componentWillUnmount() {
    this.props.clearExecutions();
  }

  handleChange = (event, newValue) => {
    //let {index} = this.state;
    //console.log(`new value: ${newValue} index:${index}`)
    this.setState({
      value: newValue
    });
  };

  a11yProps(index) {
    return {
      id: `vertical-tab-${index}`,
      "aria-controls": `vertical-tabpanel-${index}`
    };
  }

  render() {
    let { classes, executions } = this.props;
    let { value } = this.state;
    let count = 0;
    // executions = [{
    //   executionName: "exec076",
    //   jobId: "582",
    //   selectConfiguration: {
    //     name: "a"
    //   },
    //   selectEnvironment: {
    //     name: "a"
    //   },
    //   monitorLink: "a",
    //   owner: "a",
    //   created: "a",
    //   status: "running",
    //   logData1: [ 
    //     "2020-03-23 09:57:36,246 p=25129 u=root |  PLAY [localhost] ***************************************************************", 
    //     "2020-03-23 09:57:36,257 p=25129 u=root |  TASK [Gathering Facts] *********************************************************", 
    //     "2020-03-23 09:57:37,287 p=25129 u=root |  ok: [localhost]", 
    //     "2020-03-23 09:57:37,319 p=25129 u=root |  TASK [running log updater script in background] ********************************", 
    //     "2020-03-23 09:57:37,974 p=25129 u=root |  changed: [localhost]", 
    //     "2020-03-23 09:57:37,992 p=25129 u=root |  PLAY [all] *********************************************************************", 
    //     "2020-03-23 09:57:38,028 p=25129 u=root |  TASK [Gathering Facts] *********************************************************", 
    //     "2020-03-23 09:57:39,460 p=25129 u=root |  ok: [13.127.11.68]", 
    //     "2020-03-23 09:57:39,491 p=25129 u=root |  TASK [copy_hosts_files] ********************************************************", 
    //     "2020-03-23 09:57:40,416 p=25129 u=root |  ok: [13.127.11.68] => (item=inventory.ini)", 
    //     "2020-03-23 09:57:40,460 p=25129 u=root |  TASK [copy_config_files] *******************************************************", 
    //     "2020-03-23 09:57:40,542 p=25129 u=root |  TASK [copy_data_files] *********************************************************", 
    //     "2020-03-23 09:57:40,618 p=25129 u=root |  TASK [copy_testcases_files] ****************************************************", 
    //     "2020-03-23 09:57:40,685 p=25129 u=root |  TASK [check_files] *************************************************************", 
    //     "2020-03-23 09:57:40,939 p=25129 u=root |  changed: [13.127.11.68]", 
    //     "2020-03-23 09:57:40,969 p=25129 u=root |  TASK [run_test_script] *********************************************************", 
    //     "2020-03-23 09:57:41,199 p=25129 u=root |  changed: [13.127.11.68]", 
    //     "2020-03-23 09:57:41,230 p=25129 u=root |  TASK [debug] *******************************************************************", 
    //     "2020-03-23 09:57:41,270 p=25129 u=root |  ok: [13.127.11.68] => {"]
    // }];
    return (
      <div className={classes.root}>
        <Tabs
          orientation="vertical"
          variant="scrollable"
          value={value}
          onChange={this.handleChange}
          aria-label="Vertical tabs example"
          className={classes.tabs}
        >
          {executions.map((execution, index) => {
            /*this.setState({
        executionObj: execution
      })*/
            //index = objIndex;
            //executionObj = execution;
            //console.log(`index from map: ${index}`)
            if (
              [
                "started",
                "running",
                "pending",
                "waiting",
                "yet to start"
              ].includes(execution.status)
            ) {
              count += 1;
              return (
                <Tab
                  label={execution.executionName}
                  key={index}
                  {...this.a11yProps(index)}
                />
              );
            }
          })}
        </Tabs>
        {executions.map((execution, index) => {
          /*this.setState({
        executionObj: execution
      })*/
          //index = objIndex;
          //executionObj = execution;
          //console.log(`index from map: ${index}`)
          // console.log(
          //   `execution from map: ${JSON.stringify(execution, null, 2)}`
          // );
          if (
            [
              "started",
              "running",
              "pending",
              "waiting",
              "yet to start"
            ].includes(execution.status)
          ) {
            return (
              <TabPanel
                value={value}
                index={index}
                key={index}
                executionObj={execution}
              />
            );
          }
        })}
        {count == 0 ? "No ongoing executions to display." : null}

        
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    executions: _.values(state.executions)
  };
};
export default connect(mapStateToProps, {
  getExecutions,
  clearExecutions
})(withStyles(styles)(ExecutionTabs));
