import React from "react";
import { connect } from "react-redux";
import { withStyles, Paper, Typography, Box } from "@material-ui/core";
import ExecutionTabs from "./ExecutionTabs";

 

import _ from "lodash";
const styles = theme => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    marginTop: theme.spacing(2)
  },
  heading: {
    paddingTop: "10px",
    color: "#3f51b5",
    fontVariant: "small-caps",
    marginBottom: "50px"
  },
  grid: {
    minWidth: "100%",
    margin: theme.spacing(1),
    height: "100%"
  }
});

const INITIAL_STATE = {};

class MonitorExections extends React.Component {
  state = INITIAL_STATE;

  render() {
    const { classes, configurations} = this.props;
    return (
      <Paper elevation={3} className={classes.paper}>
        <Typography variant="h4" gutterBottom className={classes.heading}>
          <Box fontWeight="fontWeightBold">MONITOR EXECUTIONS</Box>
        </Typography>
        <ExecutionTabs configurations={configurations}/>
      </Paper>
    );
  }
}


export default withStyles(styles)(MonitorExections);
