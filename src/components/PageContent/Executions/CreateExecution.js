import React from "react";
import { connect } from "react-redux";
import {
  withStyles,
  Paper,
  Typography,
  Box,
  InputLabel,
  MenuItem,
  Select,
  FormControl,
  Grid,
  FormControlLabel,
  Checkbox,
  Button,
  LinearProgress,
  TextField,
  Divider,
  CircularProgress
} from "@material-ui/core";
import {
  getConfigurations,
  getEnvironments,
  clearConfigurations,
  clearEnvironments,
  createExecution,
  getExecutions,
  startCloudJob,
  clearCloudJob,
  initializeCloudJob
} from "actions";
import _ from "lodash";
import { toast } from "react-toastify";
import InteractiveButton from "./interactiveButton";

const styles = theme => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    padding: theme.spacing(2)
  },
  buttonContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
    //justifyContent: "center"
  },
  heading: {
    paddingTop: "10px",
    color: "#3f51b5",
    fontVariant: "small-caps"
  },
  field: {
    width: "95%",
    margin: theme.spacing(1)
  },
  names: {
    width: "95%",
    margin: theme.spacing(1)
  },
  button: {
    width: "90%",
    marginTop: "2rem",
    marginBottom: "2rem"
  },
  grid: {
    minWidth: "90%"
  },
  progress: {
    width: "100%"
  },
  input: {
    display: "none"
  },
  input_button: {
    width: "15%",
    margin: theme.spacing(1)
  },
  file_field: {
    width: "80%",
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(0.5)
  }
});

const INITIAL_STATE = {
  selectConfiguration: "",
  selectEnvironment: "",
  deleteEnvironment: true,
  configLoading: false,
  envLoading: false,
  createProgress: false,
  executeClicked: false,
  executionName: "",
  hostName: "",
  filenames: {
    hostfile: "",
    licensefile: ""
  },
  hostFile: {},
  licenseFile: false,
  errorFlag: false,
  errorHelperText: "",
  cloudCreated: false,
  cloudJob: false,
  executionNameError: false,
  selectConfigurationError: false,
  cloudClicked: false,
  licenseError: false
};

class Executions extends React.Component {
  state = INITIAL_STATE;
  timer = null;

  componentDidMount() {
    window.scrollTo(0, 0);
    this.setState({ configLoading: true }, async () => {
      await this.props.getConfigurations();
      this.setState({ configLoading: false });
    });
    this.setState({ envLoading: true }, async () => {
      await this.props.getEnvironments("free");
      this.setState({ envLoading: false });
    });
  }

  componentWillUnmount() {
    this.props.clearConfigurations();
    this.props.clearEnvironments();
    this.setState({
      filenames: {
        hostfile: "",
        licensefile: ""
      },
      hostFile: {},
      licenseFile: false
    });
    this.props.clearCloudJob();
  }

  isEmpty(value) {
    return value === "" || value === {};
  }

  shouldDisableButton(type) {
    let {
      selectConfiguration,
      selectEnvironment,
      executionName,
      executeClicked,
      cloudCreated,
      cloudJob
    } = this.state;
    const { cloudjob } = this.props;
    if (type === "createButton") {
      let flag =
        this.isEmpty(selectConfiguration) ||
        this.isEmpty(selectEnvironment) ||
        this.isEmpty(executionName) ||
        executeClicked ||
        (cloudJob && !(cloudjob.cloudjob ? cloudjob.cloudjob.success : false));
      return flag;
    } else if (type === "licenseButton") {
      return false;
    }
  }

  handleChange = e => {
    const { emptyErrors } = this.state;
    if (e.target.name === "deleteEnvironment") {
      this.setState({ [e.target.name]: !this.state.deleteEnvironment });
    } else if (e.target.name === "executionName" && e.target.value) {
      this.setState({ [e.target.name]: e.target.value }, () => {
        this.setState(
          {
            executionNameError: false
          },
          () => {}
        );
      });
    } else if (e.target.name === "selectConfiguration" && e.target.value) {
      this.setState({ [e.target.name]: e.target.value }, () => {
        this.setState(
          {
            selectConfigurationError: false
          },
          () => {}
        );
      });
    } else {
      //console.log(`change value: ${e.target.value}`);
      this.setState({ [e.target.name]: e.target.value }, () => {
        if (this.getEnvType(this.state.selectEnvironment) === "CLOUD") {
          this.setState(
            {
              cloudJob: true
            },
            () => {
              console.log(`cloud job!`);
            }
          );
        } else {
          this.setState(
            {
              cloudJob: false
            },
            () => {
              console.log(`not cloud job!`);
            }
          );
        }
      });
    }
  };

  handleCreate = () => {
    if (!this.state.executeClicked) {
      this.setState(
        {
          executeClicked: true
        },
        () => {}
      );

      const { configurations, environments } = this.props;
      let execObj = _.assign(_.pick(this.state, "executionName"), {
        hostName: this.state.hostName,
        hostFile: this.state.hostFile,
        licenseFile: this.state.licenseFile,
        selectConfiguration: configurations.find(
          configuration => configuration._id === this.state.selectConfiguration
        ),
        selectEnvironment: environments.find(
          environment => environment._id === this.state.selectEnvironment
        ),
        deleteEnvironment: this.state.deleteEnvironment
      });
      console.log(`execution obj licfile: ${execObj.licenseFile?true:false}`);
      if(execObj.selectConfiguration.type === "functional" && !execObj.licenseFile){
        this.setState({
          licenseError: true,
          executeClicked: false
        },()=>{
          toast.error(`Please upload license.`);
        });
        return;
      }else{
        this.setState({
          licenseError: false
        },()=>{
          
        });
      }
      this.setState(
        {
          createProgress: true,
          executeClicked: true
        },
        async () => {
          await this.props.createExecution(execObj);
          this.setState({
            ...INITIAL_STATE,
            selectEnvironment: "",
            selectConfiguration: "",
            createProgress: false,
            executeClicked: false,
            filenames: {
              hostfile: "",
              licensefile: ""
            },
            hostFile: {},
            licenseFile: {}
          }, ()=>{
            console.log(`licence set to default`);
          });
        }
      );
    } else {
      toast.warn("Execution starting. Please wait...");
    }
  };
  getFileNames(files) {
    return Object.keys(files)
      .map(file => {
        console.log(`${files[file].name}`);
        return files[file].name;
      })
      .join("; ");
  }
  handleFileSelection = (fileNames, files, type) => {
    if (Object.keys(files).length === 1) {
      let fileReader = new FileReader();
      fileReader.onload = e => {
        if (type === "hostFile") {
          let { hostFile, filenames } = this.state;
          filenames["hostfile"] = fileNames;
          hostFile["name"] = files[0].name;
          hostFile["data"] = e.target.result;
          console.log(`file: ${JSON.stringify(hostFile)}`);
          this.setState({ filenames: filenames, hostFile: hostFile });
        } else if (type === "licenseFile") {
          let { licenseFile, filenames } = this.state;
          filenames["licensefile"] = fileNames;
          licenseFile = {}
          licenseFile["name"] = files[0].name;
          licenseFile["data"] = e.target.result;
          console.log(`file: ${JSON.stringify(licenseFile)}`);
          this.setState({ filenames: filenames, licenseFile: licenseFile });
        }
      };
      fileReader.readAsDataURL(files[0]);
    } else {
      this.setState({
        errorFlag: true,
        errorHelperText: "Select a single file"
      });
    }
  };

  createCloudEnv = async () => {
    if (!this.state.cloudClicked) {
      let {
        selectConfiguration,
        selectEnvironment,
        executionName
      } = this.state;
      const { configurations, environments } = this.props;
      let { cloudjob } = this.props;

      if (selectConfiguration && selectEnvironment && executionName) {
        let configuration = configurations.find(configuration =>
          configuration._id === selectConfiguration ? configuration : null
        );
        if (configuration.type === "functional") {
          toast.error(
            "Functional tests not supported on new cloud environment!"
          );
          return;
        }
        console.log(`creating cloud`);
        this.setState({
          cloudClicked: true
        }, ()=>{
          console.log(`cloudClicked set to true`);
        });
        await this.props.initializeCloudJob();
        let execObj = {
          executionName: `cloudjob-${executionName}`,
          hostName: this.state.hostName,
          hostFile: this.state.hostFile,
          licenseFile: this.state.licenseFile,
          selectConfiguration: configurations.find(
            configuration =>
              configuration._id === this.state.selectConfiguration
          ),
          selectEnvironment: environments.find(
            environment => environment._id === this.state.selectEnvironment
          ),
          deleteEnvironment: this.state.deleteEnvironment
        };
        console.log(`cloud execution obj:\n${JSON.stringify(execObj, null, 2)}`);
        await this.props.startCloudJob(execObj);
      } else if (!executionName) {
        this.setState(
          {
            executionNameError: true
          },
          () => {
            console.log("execution name is empty");
          }
        );
      } else if (!selectConfiguration) {
        this.setState(
          {
            selectConfigurationError: true
          },
          () => {
            console.log("configuration name is empty");
          }
        );
      }
      //console.log(`cloud job details: ${JSON.stringify(cloudjob)}`);
    } else {
      toast.error("New cloud environment already created.");
    }
  };

  renderCloudButton() {
    //console.log(`renderCloudButton called`);
    const { classes, cloudjob } = this.props;
    return (
      <InteractiveButton
        loading={cloudjob.cloudjob ? cloudjob.cloudjob.loading : false}
        success={cloudjob.cloudjob ? cloudjob.cloudjob.success : false}
        createCloudEnv={this.createCloudEnv}
        preButtonText="CREATE CLOUD ENVIRONMENT"
        postButtonText="CLOUD ENVIRONMENT CREATED"
      />
    );
  }

  getEnvType(selectEnvironment) {
    //console.log(`selectenv: ${selectEnvironment}`);
    const { environments } = this.props;
    //console.log(JSON.stringify(environments, 2, null));
    let environment = environments.find(environment =>
      selectEnvironment
        ? environment._id === selectEnvironment
          ? environment
          : null
        : null
    );

    return environment ? environment.type : null;
  }

  render() {
    const { classes, configurations, environments } = this.props;
    const {
      selectEnvironment,
      selectConfiguration,
      deleteEnvironment,
      shouldDisableButton,
      filenames,
      errorFlag,
      errorHelperText,
      cloudJob,
      executionNameError,
      selectConfigurationError,
      cloudClicked,
      licenseError
    } = this.state;

    return (
      <Paper elevation={3} className={classes.paper}>
        <Typography variant="h4" gutterBottom className={classes.heading}>
          <Box fontWeight="fontWeightBold">START EXECUTIONS</Box>
        </Typography>
        <div className={classes.grid}>
          <Grid container>
            <Grid item xs={4}>
              <TextField
                required
                name="executionName"
                label="Execution Name"
                margin="dense"
                id="executionName"
                value={this.state.executionName}
                className={classes.names}
                fullWidth
                onChange={this.handleChange}
                error={executionNameError}
                disabled={cloudClicked}
              />
            </Grid>
            <Grid item xs={4}>
              <FormControl
                className={classes.field}
                error={selectConfigurationError}
                disabled={cloudClicked}
              >
                <InputLabel htmlFor="selectConfiguration" required>
                  Configuration Name
                </InputLabel>
                <Select
                  onChange={this.handleChange}
                  value={selectConfiguration}
                  inputProps={{
                    name: "selectConfiguration",
                    id: "selectConfiguration"
                  }}
                >
                  <MenuItem value="" disabled>
                    Configurations
                  </MenuItem>
                  {configurations.map(configuration => {
                    return (
                      <MenuItem
                        value={configuration._id}
                        key={configuration._id}
                      >
                        {configuration.name}
                      </MenuItem>
                    );
                  })}
                </Select>
                {this.state.configLoading && <LinearProgress />}
              </FormControl>
            </Grid>
            <Grid item xs={4}>
              <FormControl className={classes.field} disabled={cloudClicked}>
                <InputLabel htmlFor="selectEnvironment" required>
                  Environment Name
                </InputLabel>
                <Select
                  onChange={this.handleChange}
                  value={selectEnvironment}
                  inputProps={{
                    name: "selectEnvironment",
                    id: "selectEnvironment"
                  }}
                >
                  <MenuItem value="" disabled>
                    Environments
                  </MenuItem>
                  {environments.map(environment => {
                    return (
                      <MenuItem value={environment._id} key={environment._id}>
                        {environment.name}
                      </MenuItem>
                    );
                  })}
                </Select>
                {this.state.envLoading && <LinearProgress />}
              </FormControl>
            </Grid>
            <Grid item xs={8}>
              <input
                accept="*"
                className={classes.input}
                id="uploadHost"
                type="file"
                onChange={e =>
                  this.handleFileSelection(
                    this.getFileNames(e.target.files),
                    e.target.files,
                    "hostFile"
                  )
                }
              />
              <label htmlFor="uploadHost">
                <Button
                  color="primary"
                  variant="contained"
                  component="span"
                  className={classes.input_button}
                >
                  UPLOAD
                </Button>
              </label>
              <TextField
                error={errorFlag}
                className={classes.file_field}
                id="hostFile"
                placeholder="Host File"
                helperText={errorHelperText}
                margin="normal"
                readOnly
                value={filenames["hostfile"]}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                name="hostName"
                label="Host Name"
                margin="normal"
                id="hostName"
                value={this.state.hostName}
                className={classes.names}
                fullWidth
                onChange={this.handleChange}
              />
            </Grid>
            <Grid item xs={12} className={classes.buttonContainer}>
              {cloudJob ? this.renderCloudButton() : null}
            </Grid>
            <Grid item xs={8}>
              <input
                accept="*"
                className={classes.input}
                id="uploadLicense"
                type="file"
                onChange={e =>
                  this.handleFileSelection(
                    this.getFileNames(e.target.files),
                    e.target.files,
                    "licenseFile"
                  )
                }
                required
              />
              <label htmlFor="uploadLicense">
                <Button
                  color="primary"
                  variant="contained"
                  component="span"
                  className={classes.input_button}
                >
                  UPLOAD
                </Button>
              </label>
              <TextField
                error={errorFlag}
                className={classes.file_field}
                id="licenseFile"
                placeholder="License File*"
                helperText={errorHelperText}
                margin="normal"
                readOnly
                value={filenames["licensefile"]}
                fullWidth
                required
                error={licenseError}
              />
            </Grid>
            <Grid item xs={4}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={deleteEnvironment}
                    onChange={this.handleChange}
                    value="deleteEnvironment"
                    color="primary"
                    inputProps={{
                      name: "deleteEnvironment",
                      id: "deleteEnvironment"
                    }}
                  />
                }
                label="Delete environment post execution"
                className={classes.field}
              />
            </Grid>
            <Grid item xs={12} className={classes.buttonContainer}>
              <Button
                className={classes.button}
                onClick={this.handleCreate}
                color="primary"
                variant="contained"
                disabled={this.shouldDisableButton("createButton")}
              >
                START EXECUTION
              </Button>
            </Grid>
          </Grid>
        </div>
        <div className={classes.progress}>
          {this.state.createProgress && <LinearProgress />}
        </div>
      </Paper>
    );
  }
}

const mapStateToProps = state => {
  return {
    configurations: _.values(state.configurations),
    environments: _.values(state.environments),
    cloudjob: state.cloudjob
  };
};

export default connect(mapStateToProps, {
  getConfigurations,
  getEnvironments,
  clearConfigurations,
  clearEnvironments,
  getExecutions,
  createExecution,
  startCloudJob,
  clearCloudJob,
  initializeCloudJob
})(withStyles(styles)(Executions));
