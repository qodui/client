import React from "react";
import { withStyles, Grid, TextField, Box } from "@material-ui/core";
import { connect } from "react-redux";
import _ from "lodash";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import { TextareaAutosize } from "@material-ui/core";
import python_backend from "api/python_backend";
import axios from "axios";
import SelectInput from "@material-ui/core/Select/SelectInput";
import store from "store";
import { DJANGO_URL } from "constants.js";
import { updateCloudJob, clearCloudJob } from "actions";
import { LazyLog } from "react-lazylog";

const styles = theme => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  heading: {
    paddingTop: "10px",
    color: "#3f51b5",
    fontVariant: "small-caps"
  },
  field: {
    width: "99%",
    margin: theme.spacing(1)
  },
  button: {
    margin: theme.spacing(1),
    width: "100%"
  },
  root: {
    width: "85%",
    margin: theme.spacing(1),
    alignItems: "center",
    justifyContent: "center"
  },
  progress: {
    width: "100%"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    flex: 1,
    overflowWrap: "break-word",
    width: "95%"
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3)
  },
  monitorContent: {
    alignItems: "center",
    justifyContent: "center"
  },
  logArea: {
    resize: "None",
    margin: "10px",
    width: "85%",
    maxHeight: "500px",
    minHeight: "100px",
    padding: "10px"
  },
  monitorLink: {
    paddingTop: "10px",
    marginBottom: "50px"
  },
  grid_root: {
    flexGrow: 1
  },
  logDiv: {
    padding: "0.5rem",
    resize: "None",
    margin: "0.5rem",
    height: "300px",
    minWidth: "500px"
  }
});
const INITIAL_STATE = {
  logData1: "",
  status: "",
  log: null
};

class TabPanel extends React.Component {
  state = INITIAL_STATE;
  linecount = 0;

  updateLog = async () => {
    let { logData1, status } = this.state;
    let { executionObj } = this.props;
    if (status !== "successful" && status !== "failed") {
      try {
        let jobId = executionObj.executionName;
        //console.log(`jobid: ${jobId}`);
        if (jobId) {
          let url = `${DJANGO_URL}/logs/id/?id=` + jobId;
          //console.log(`log url: ${url}`);
          await axios({
            method: "get",
            url: url,
            //data: postData,
            headers: {
              "Content-Type": "application/json",
              authId: store.get("user").authId
            }
          })
            .then(response => {
              //console.log(`log response: ${JSON.stringify(response, null, 2)}`);
              let data = response.data.logs;
              try {
                this.setState({
                  logData1: data
                });
                //this.scrollToBottom();
              } catch (e) {
                console.log(`error while updating logdata state: ${e}`);
              }
            })
            .catch(e => {
              console.log(`error in fetching log: ${e}`);
            });
        }
      } catch (e) {
        console.log(e);
      }
    }
  };

  updateStatus = async () => {
    let { executionObj, updateCloudJob, cloudjob, clearCloudJob } = this.props;
    let { status } = this.state;
    if (true || status !== "successful" && status !== "failed") {
      try {
        let jobId = executionObj.jobId;
        console.log(`jobid: ${jobId}`);
        if (jobId) {
          let url = `${DJANGO_URL}/status/id/?id=` + jobId;
          //console.log(`status url: ${url}`);
          await axios({
            method: "get",
            url: url,
            headers: {
              "Content-Type": "application/json",
              authId: store.get("user").authId
            }
          })
            .then(response => {
              // console.log(
              //   `status response: ${JSON.stringify(response.data, null, 2)}`
              // );
              try {
                let data = response.data;
                if (
                  cloudjob.cloudjob &&
                  executionObj.executionName === cloudjob.cloudjob.name
                ) {
                  // console.log(
                  //   `cloud job found in tabpanel: ${JSON.stringify(
                  //     cloudjob.cloudjob,
                  //     null,
                  //     2
                  //   )}`
                  // );
                  if (data.status === "successful") {
                    updateCloudJob({
                      success: true,
                      executionName: executionObj.executionName
                    });
                  } else if (data.status === "failed") {
                    updateCloudJob({
                      success: false,
                      executionName: executionObj.executionName
                    });
                  }
                } else if (
                  cloudjob.cloudjob &&
                  executionObj.executionName ===
                    cloudjob.cloudjob.name.split("-")[1]
                ) {
                  clearCloudJob();
                }
                this.setState(
                  {
                    status: data.status
                  },
                  () => {
                    // console.log(
                    //   `status set to the state: ${this.state.status}`
                    // );
                  }
                );

                this.scrollToBottom();
              } catch (e) {
                console.log(`error while updating status state: ${e}`);
              }
            })
            .catch(e => {
              console.log(`error in updating status: ${e}`);
            });
        } else {
          console.log(`job id not found in status updater`);
        }
      } catch (e) {
        console.log(e);
      }
    } else {
      clearInterval(this.logInterval);
      clearInterval(this.statusInterval);
    }
  };

  componentDidMount() {
    //console.log(`tabpanel rendered`);
    this.updateLog();
    this.updateStatus();
    this.updateLog();
    this.logInterval1 = setInterval(this.updateLog, 2000);
    this.statusInterval = setInterval(this.updateStatus, 7000);
    this.setState(
      {
        status: this.props.executionObj.status
      },
      () => {//value={executionObj.status}
        //console.log(`log data set in state`);
      }
    );
    this.logInterval2 = setInterval(this.updateLogs, 500);
  }
  updateLogs = () => {
    const { logData1 } = this.state;
    // console.log(
    //   `linecount: ${this.linecount} --------- logcount: ${logData1.length}`
    // );
    const { index, value } = this.props;

    if (
      value !== null &&
      value === index &&
      logData1.length > 0 &&
      this.linecount <= logData1.length
    ) {
      try {
        this.sleep(0).then(() => {
          this.addLineInLog(logData1[this.linecount]);
          this.sleep(400).then(() => {
            this.addLineInLog(logData1[this.linecount + 1]);
            this.sleep(400).then(() => {
              this.addLineInLog(logData1[this.linecount + 2]);
              this.linecount += 3;
            });
          });
        });
      } catch (e) {
        console.log(`exception occured: ${e}`);
      }
    }
  };

  sleep = ms => {
    return new Promise(resolve => setTimeout(resolve, ms));
  };

  addLineInLog = line => {
    if (line !== undefined) {
      this.setState(
        {
          log: line
        },
        () => {
          //console.log(`log in state: ${this.state.log}`);
        }
      );
    }
  };

  componentWillUnmount() {
    const { executionObj } = this.props;
    if (this.state.executionObj === undefined) {
      this.setState({
        executionObj: executionObj
      });
    }
    // Clear the interval right before component unmount
    clearInterval(this.logInterval1);
    clearInterval(this.logInterval2);
    clearInterval(this.statusInterval);
  }

  async componentDidUpdate() {
    //setTimeout(() => {console.log("updating the log in 5 seconds")}, 50000);
    //this.updateLog()
  }
  scrollToBottom = () => {
    if (this.textLog) {
      this.textLog.scrollTop = this.textLog.scrollHeight;
    }
  };
  render() {
    const { index, classes, value, executionObj } = this.props;
    //console.log(`executionObj: ${JSON.stringify(executionObj, null, 2)}`);

    let { status } = this.state;

    if (value === undefined || value === null || value !== index) {
      // console.log(
      //   `issue with value or index. value: ${value}, index: ${index}`
      // );
      return null;
    }

    return (
      <div className={classes.grid_root}>
        <Grid
          container
          aria-labelledby={`vertical-tab-${index}`}
          id={`vertical-tabpanel-${index}`}
          role="tabpanel"
          component="div"
          spacing={2}
          direction="row"
          justify="center"
        >
          <Grid item xs={12} sm={4}>
            <TextField
              id="outlined-read-only-input"
              label="Execution Name"
              value={executionObj.executionName}
              className={classes.textField}
              margin="normal"
              InputProps={{
                readOnly: true
              }}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            {executionObj.selectConfiguration ? (
              <TextField
                id="outlined-read-only-input"
                label="Configuration"
                value={executionObj.selectConfiguration.name}
                className={classes.textField}
                margin="normal"
                InputProps={{
                  readOnly: true
                }}
                variant="outlined"
              />
            ) : null}
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField
              id="outlined-read-only-input"
              label="Environment"
              value={executionObj.selectEnvironment.name}
              className={classes.textField}
              margin="normal"
              InputProps={{
                readOnly: true
              }}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField
              id="status"
              label="Status"
              value={status}
              className={classes.textField}
              margin="normal"
              InputProps={{
                readOnly: true
              }}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField
              id="outlined-read-only-input"
              label="Owner"
              value={executionObj.owner}
              className={classes.textField}
              margin="normal"
              InputProps={{
                readOnly: true
              }}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField
              id="outlined-read-only-input"
              label="Date"
              value={executionObj.created}
              className={classes.textField}
              margin="normal"
              InputProps={{
                readOnly: true
              }}
              variant="outlined"
            />
          </Grid>
        </Grid>

        <div className={classes.monitorContent}>
          <Typography className={classes.monitorLink}>
            <Link
              onClick={() => {
                window.open(executionObj.monitorLink);
              }}
              variant="h6"
            >
              Click here to monitor this execution
            </Link>
          </Typography>
          <Box fontWeight="fontWeightBold">
            <Typography>execution logs:</Typography>
          </Box>
          {/* <TextareaAutosize
            className={classes.logArea}
            rowsMax={5}
            aria-label="maximum height"
            placeholder="Maximum 4 rows"
            value={logData1}
            ref={textLog => (this.textLog = textLog)}
          /> */}
          <div className={classes.logDiv}>
            {this.state.log === null ? "Please wait while fetching logs..." : (
              <LazyLog
                extraLines={2}
                enableSearch
                text={this.state.log}
                caseInsensitive
                rowHeight={16}
                follow
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    configurations: _.values(state.configurations),
    environments: _.values(state.environments),
    cloudjob: state.cloudjob
  };
};

export default connect(mapStateToProps, { updateCloudJob })(
  withStyles(styles)(TabPanel)
);
