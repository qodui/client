import React from "react";
import { Grid } from "@material-ui/core";
import CreateExecution from "./CreateExecution";
import MonitorExecutions from "./MonitorExecutions";

class Executions extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  

  render() {
    return (
      <Grid container spacing={2} direction="column">
        <Grid item xs={12}>
          <CreateExecution />
        </Grid>
        <Grid item xs={12} >
          <MonitorExecutions />
        </Grid>
      </Grid>
    );
  }
}

export default Executions;
