import React from "react";
import { withStyles } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import { green } from "@material-ui/core/colors";
import Button from "@material-ui/core/Button";
import Fab from "@material-ui/core/Fab";
import CloudDoneIcon from "@material-ui/icons/CloudDone";
import CloudIcon from "@material-ui/icons/Cloud";
import { connect } from "react-redux";

const styles = theme => ({
  root: {
    display: "flex",
    alignItems: "center"
  },
  wrapper: {
    margin: theme.spacing(1),
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
    position: "relative"
  },
  buttonSuccess: {
    backgroundColor: green[500],
    "&:hover": {
      backgroundColor: green[700]
    }
  },
  fabProgress: {
    color: green[500],
    position: "absolute",
    top: -4,
    left: -4,
    zIndex: 1
  },
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12
  }
});

class InteractiveButton extends React.Component {
  createCloud = e => {
    this.props.createCloudEnv();
  };

  render() {
    const {
      classes,
      loading,
      success,
      preButtonText,
      postButtonText
    } = this.props;

    return (
      <div className={classes.root}>
        <div className={classes.wrapper}>
          <Fab
            size="small"
            aria-label="save"
            color="primary"
            className={success ? classes.buttonSuccess : null}
            onClick={this.createCloud}
          >
            {success ? <CloudDoneIcon /> : <CloudIcon />}
          </Fab>
          {loading && (
            <CircularProgress size={48} className={classes.fabProgress} />
          )}
        </div>
        <div className={classes.wrapper}>
          <Button
            variant="contained"
            color="primary"
            className={success ? classes.buttonSuccess : null}
            disabled={loading}
            onClick={this.createCloud}
          >
            {success ? postButtonText : preButtonText}
          </Button>
          {loading && (
            <CircularProgress size={24} className={classes.buttonProgress} />
          )}
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {};
};

export default connect(
  mapStateToProps,
  {}
)(withStyles(styles)(InteractiveButton));
