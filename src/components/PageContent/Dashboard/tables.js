import React from "react";
import { connect } from "react-redux";
import {
  withStyles,
  Typography,
  Box,
  Grid,
  LinearProgress,
} from "@material-ui/core";
import Table from "mui-datatables";
import _ from "lodash";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import table from "content/dashboard/table";
import { getPast5Executions } from "actions";


const styles = theme => ({
  div_root: {
    maxHeight: "350px"
  },
  heading: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    paddingTop: "10px",
    color: "#3f51b5",
    fontVariant: "small-caps",
    fontSize: "16px"
  },
  tableHeading: {
    paddingTop: "5px",
    color: "#3f51b5",
    fontVariant: "Capilatize",
    align: "center"
  },
  wrapper: {
    maxWidth: "100%",
    marginLeft: "0px",
    maxHeight: "350px"
  }
});

const INITIAL_STATE = {
  loading: false,
  open: false,
  previous_executions: {
    successJobs:[],
    failedjobs:[]
  }
};

class DashboardTables extends React.Component {
  state = INITIAL_STATE;
  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MUIDataTableBodyCell: {
          root: {
            fontSize: "0.85rem"
          }
        },
        MUIDataTableHeadCell: {
          root: {
            fontSize: "1.0rem",
            fontWeight: "bold",
            fontVariant: "small-caps",
            color: "#3f51b5"
          }
        },
        MuiTableCell: {
          root: {
            overflow: "scroll"
          }
        }
      }
    });

  componentDidMount() {
    this.getData();
  }

  componentWillUnmount() {}
  getData = () => {
    this.setState({ loading: true }, async ()=> {
      const data = await getPast5Executions();
      this.setState({ previous_executions: data, loading: false });
    });
  };

  render() {
    const { classes } = this.props;
    const { loading, previous_executions } = this.state;
    return (
      <div className={classes.div_root}>
        <Grid container spacing={4} className={classes.div_root}>
          <Grid item xs={6}>
            <div className={classes.wrapper}>
              {loading && <LinearProgress />}
              <MuiThemeProvider theme={this.getMuiTheme()}>
                <Table
                  title={
                    <Typography
                      variant="h6"
                      gutterBottom
                      className={classes.tableHeading}
                    >
                      <Box fontWeight="fontWeightMedium" textAlign="center">
                        Last 5 successful executions
                      </Box>
                    </Typography>
                  }
                  columns={table.columns}
                  data={previous_executions.successJobs}
                  options={table.options}
                />
              </MuiThemeProvider>
            </div>
          </Grid>
          <Grid item xs={6}>
            <div className={classes.wrapper}>
              {loading && <LinearProgress />}
              <MuiThemeProvider theme={this.getMuiTheme()}>
                <Table
                  title={
                    <Typography
                      variant="h6"
                      gutterBottom
                      className={classes.tableHeading}
                    >
                      <Box fontWeight="fontWeightMedium" textAlign="center">
                        Last 5 failed executions
                      </Box>
                    </Typography>
                  }
                  columns={table.columns}
                  data={previous_executions.failedJobs}
                  options={table.options}
                />
              </MuiThemeProvider>
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => {};

export default connect(mapStateToProps, {})(
  withStyles(styles)(DashboardTables)
);
