import React from "react";
import { connect } from "react-redux";
import { withStyles, Typography, Grid, CircularProgress } from "@material-ui/core";
import _ from "lodash";
import {
  PieChart,
  Pie,
  Cell,
  ResponsiveContainer,
  Tooltip,
  Legend
} from "recharts";

const styles = theme => ({
  itemHeading: {
    background: "LightGray",
    margin: theme.spacing(1)
  },
  graphHeading: {
    color: "#3f51b5",
    textTransform: "capitalize"
  },
  gridItem: {
    borderRadius: 10,
    borderWidth: 2,
    //borderColor: '#d6d7da',
    borderColor: "black",
  }
});

class PiechartComponent extends React.Component {
  componentDidMount = () => {};

  render() {
    const { classes, pichartClicked, data, colors, id, loading } = this.props;
    //console.log(`data in ${id} pichart component: ${JSON.stringify(data)}`);

    return (
      <Grid
        item
        xs={3}
        id={id}
        onClick={pichartClicked}
        className={classes.gridItem}
      >
        <Typography
          variant="body2"
          className={classes.graphHeading}
          align="center"
        >
          {id}
        </Typography>
        <ResponsiveContainer width={"100%"} height={200}> 
          {!loading?<PieChart>
            <Pie
              data={data}
              outerRadius={"70%"}
              innerRadius={"30%"}
              fill="#8884d8"
              variant="overline"
              dataKey="value"
            >
              {data.map((key, index) => (
                  <Cell fill={colors[index % colors.length]} key={key} /> 
                ))}
            </Pie>
            <Legend iconType="circle" iconSize={10} />
            <Tooltip />
          </PieChart>:<CircularProgress/>}
        </ResponsiveContainer>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

export default connect(
  mapStateToProps,
  {}
)(withStyles(styles)(PiechartComponent));
