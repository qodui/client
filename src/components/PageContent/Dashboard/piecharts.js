import React from "react";
import { connect } from "react-redux";
import {
  withStyles,
  Typography,
  Grid,
  LinearProgress,
  Paper,
  Box,
  InputLabel,
  MenuItem,
  Select,
  FormControl,
  CircularProgress
} from "@material-ui/core";
import _ from "lodash";
import {
  PieChart,
  Pie,
  Sector,
  Cell,
  ResponsiveContainer,
  Tooltip,
  Legend
} from "recharts";
import PiechartComponent from "./piechartComponent";
import { getPichartData } from "actions";
import axios from "axios";
import store from "store";
import { DJANGO_URL } from "constants.js";

const styles = theme => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    minHeight: "375px",
    padding: theme.spacing(1),
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  graphHeading: {
    color: "#3f51b5",
    textTransform: "capitalize"
  },
  gridContainer: {},
  field: {
    width: "20%",
    margin: theme.spacing(1.5)
  },
  itemHeading: {
    background: "LightGray",
    margin: theme.spacing(1)
  },
  dropdownDiv: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "right"
  },
  field: {
    width: "10%",
    margin: theme.spacing(0.5)
  }
});
const INITIAL_STATE = {
  activeIndex1: 0,
  activeIndex2: 0,
  activeIndex3: 0,
  selectPeriod: "1 day",
  selectType: "cloud"
};
const data = {
  performance: [
    { name: "Passed", value: 400 },
    { name: "failed", value: 50 },
    { name: "Not started", value: 0 }
  ],
  "fault tolerance": [
    { name: "Passed", value: 2000 },
    { name: "failed", value: 68 },
    { name: "Not started", value: 0 }
  ],
  scalability: [
    { name: "Passed", value: 1656 },
    { name: "failed", value: 21 },
    { name: "Not started", value: 0 }
  ]
};

const COLORS = ["#00C49F", "Tomato", "#FFBB28"];

class Piecharts extends React.Component {
  state = INITIAL_STATE;

  pichartClicked = e => {
    this.setState(
      {
        clickedPichart: e.currentTarget.id
      },
      () => {
        this.props.setClickedPichart(this.state.clickedPichart);
      }
    );
  };
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value }, () => {
      this.getAndSetGraphData(this.props.clickedCard, this.state.selectPeriod);
    });
  };

  renderPichart = (pichartClicked, data, colors, id) => {
    //console.log(`${pichartClicked} : ${JSON.stringify(data)} : ${JSON.stringify(colors)} :${JSON.stringify(id)}`);
    return (
      <PiechartComponent
        pichartClicked={pichartClicked}
        data={data}
        colors={colors}
        id={id}
      />
    );
  };

  getAndSetGraphData = async (testarea, period) => {
    let response = null;
    let url = `${DJANGO_URL}/dashboard/pichartdata/`;
    //console.log(`carddata url: ${url}`);
    this.setState({ loading: true }, async () => {
      response = await axios({
        method: "post",
        url: url,
        data: {
          testArea: testarea,
          period: period
        },
        headers: {
          "Content-Type": "application/json",
          authId: store.get("user").authId
        }
      })
        .then(response => {
          let pichartData = response.data;
          this.setState({ loading: false }, () => {
            //console.log(`pichartData: ${JSON.stringify(pichartData)}`);
            let graphData = {};
            Object.keys(pichartData).forEach(key => {
              graphData[key] = [
                { name: "Total", value: pichartData[key].total_count },
                { name: "Failed", value: pichartData[key].errored_count }
              ];
            });
            this.setState({ graphData: graphData }, () => {
              // console.log(
              //   `pichart data set: ${JSON.stringify(this.state.graphData)}`
              // );
            });
          });
        })
        .catch(e => {
          console.log(`error in fetching carddata for area ${testarea}: ${e}`);
        });
    });
  };

  componentDidMount = async () => {
    const { clickedCard } = this.props;
    const { selectPeriod } = this.state;
    //console.log(`Clicked card : ${clickedCard}`);
    await this.getAndSetGraphData(clickedCard, selectPeriod);
  };

  render() {
    const { classes, clickedCard, pichartData } = this.props;
    const { graphData, selectPeriod, loading } = this.state;
    const periodItems = [
      { key: "day", value: "1 day" },
      { key: "week", value: "1 week" },
      { key: "month", value: "1 month" },
      { key: "3 months", value: "3 months" },
      { key: "6 months", value: "6 months" },
      { key: "1 year", value: "1 year" }
    ];
    const typeItems = [
      { key: "cloud", value: "cloud" },
      { key: "on-premise", value: "on-premise" }
    ];

    return (
      <Paper elevation={3} className={classes.paper}>
        <Grid container spacing={4} className={classes.gridContainer}>
          <Grid item xs={12} className={classes.itemHeading}>
            <Typography
              variant="body1"
              className={classes.graphHeading}
              align="left"
            >
              {`Metrics for ${clickedCard}`}
            </Typography>
          </Grid>
          <Grid item xs={12} className={classes.dropdownDiv}>
            <FormControl
              className={classes.field}
              variant="outlined"
              size="small"
            >
              <InputLabel htmlFor="selectPeriod">Period</InputLabel>
              <Select
                onChange={this.handleChange}
                value={selectPeriod}
                inputProps={{
                  name: "selectPeriod",
                  id: "selectPeriod"
                }}
              >
                <MenuItem value="" disabled>
                  Period
                </MenuItem>
                {periodItems.map(item => {
                  return (
                    <MenuItem value={item.value} key={item.key}>
                      {item.value}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          </Grid>
          {graphData ? (
            Object.keys(graphData).map((key, index) =>
              !loading ? (
                <PiechartComponent
                  pichartClicked={this.pichartClicked}
                  data={graphData[key]}
                  colors={COLORS}
                  id={key}
                  key={index}
                  loading={false}
                />
              ) : (
                <PiechartComponent
                  pichartClicked={this.pichartClicked}
                  data={graphData[key]}
                  colors={COLORS}
                  id={key}
                  key={index}
                  loading={true}
                />
              )
              //this.renderPichart(this.pichartClicked, data[key], COLORS, key);
            )
          ) : (
            <CircularProgress />
          )}
        </Grid>
      </Paper>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

export default connect(mapStateToProps, { getPichartData })(
  withStyles(styles)(Piecharts)
);
