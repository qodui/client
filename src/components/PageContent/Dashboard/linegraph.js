import React from "react";
import { connect } from "react-redux";
import {
  withStyles,
  Typography,
  Grid,
  LinearProgress,
  Paper,
  Box,
  InputLabel,
  MenuItem,
  Select,
  FormControl,
  CircularProgress
} from "@material-ui/core";
import _ from "lodash";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  Label,
  Brush
} from "recharts";
import axios from "axios";
import store from "store";
import { DJANGO_URL } from "constants.js";

const styles = theme => ({
  graphdiv: {},
  paper: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    minHeight: "375px",
    padding: theme.spacing(1),
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  graphHeading: {
    color: "#3f51b5",
    textTransform: "capitalize"
  },
  gridContainer: {},
  field: {
    width: "10%",
    margin: theme.spacing(0.5)
  },
  itemHeading: {
    background: "LightGray",
    margin: theme.spacing(1)
  },
  customTooltip: {
    background: "white",
    borderWidth: 2,
    borderColor: "black"
  },
  graphdiv: {
    justifyContent: "center",
    display: "flex",
    flexDirection: "row"
  },
  dropdownDiv: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "right"
  }
});
const INITIAL_STATE = {
  loading: false,
  graphData: null,
  selectPeriod: "1 day",
  selectConfiguration: "",
  configurations: false,
  startIndex: 0,
  endIndex: 10,
  interval: 0
};
class LineGraph extends React.Component {
  state = INITIAL_STATE;
  getAndSetGraphData = async (testarea, period, testType) => {
    // console.log(
    //   `getAndSetGraphData for area: ${testarea}, period: ${period} and type: ${testType}`
    // );
    let response = null;
    let url = `${DJANGO_URL}/dashboard/linechartdata/`;
    let executions = [];
    //console.log(`carddata url: ${url}`);
    response = await axios({
      method: "post",
      url: url,
      data: {
        testArea: testarea,
        period: period,
        testType: testType
      },
      headers: {
        "Content-Type": "application/json",
        authId: store.get("user").authId
      }
    })
      .then(response => {
        let data = response.data;
        let configurations = Object.keys(data);
        console.log(`graphData: ${JSON.stringify(data, null, 2)}`);
        this.setState(
          {
            configurations: configurations,
            selectConfiguration: configurations[0],
            data: data,
            graphData: data[configurations[0]]
          },
          () => {
            console.log(`state set: ${JSON.stringify(this.state)}`);
          }
        );
      })
      .catch(e => {
        console.log(
          `error in fetching linechartdata for area ${testarea}: ${e}`
        );
        this.setState(
          {
            configurations: [],
            selectConfiguration: "",
            graphData: null
          },
          () => {
            //console.log(`state set: ${JSON.stringify(this.state)}`);
          }
        );
      });
  };
  componentDidMount = async () => {
    const { clickedCard, clickedPichart } = this.props;
    const { selectPeriod } = this.state;
    //console.log(`Clicked card : ${clickedCard}`);
    await this.getAndSetGraphData(clickedCard, selectPeriod, clickedPichart);
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value }, () => {
      if (e.target.name === "selectPeriod") {
        this.getAndSetGraphData(
          this.props.clickedCard,
          this.state.selectPeriod,
          this.props.clickedPichart
        );
      } else if (e.target.name === "selectConfiguration") {
        this.setState({
          graphData: this.state.data[e.target.value]
        });
      }
    });
  };

  renderGraph = () => {
    const graphData = this.state.graphData;
    const classes = this.props.classes;
    //console.log(`rendergraph called`);
    return (
      <ResponsiveContainer width="90%" minHeight={300}>
        <LineChart
          data={graphData}
          margin={{
            top: 5,
            right: 35,
            left: 20,
            bottom: 25
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" interval={this.state.interval}>
            <Label value="Execution" offset={-25} position="insideBottom" />
          </XAxis>
          <YAxis>
            <Label
              value={"Minutes"}
              offset={-20}
              position="insideLeft"
              angle={-90}
            />
          </YAxis>
          <Tooltip
            content={data => {
              if (data.active && data.payload) {
                const payload = data.payload[0].payload;
                return (
                  <div className={classes.customTooltip}>
                    <p>{`${payload.execution_name} : ${payload.duration} mins`}</p>
                  </div>
                );
              }
            }}
          />
          <Legend
            verticalAlign="bottom"
            align="left"
            //layout="vertical"
            iconSize={20}
            //wrapperStyle={{ bottom: -10, left: 0 }}
          />
          <Line
            type="monotone"
            dataKey="duration"
            //stroke="#8884d8"
            stroke="Orange"
            strokeWidth={2}
            activeDot={{ r: 8 }}
            label={data => {
              const { x, y, value } = data;
              return (
                <text x={x} y={y} dy={-15} fontSize={10} textAnchor="middle">
                  {value}
                </text>
              );
            }}
          />
          <Brush dataKey="name">
            <LineChart>
              <Line
                type="monotone"
                dataKey="duration"
                //stroke="#8884d8"
                stroke="Orange"
                strokeWidth={2}
                activeDot={{ r: 8 }}
              ></Line>
            </LineChart>
          </Brush>
        </LineChart>
      </ResponsiveContainer>
    );
  };

  render() {
    const { classes, clickedCard, clickedPichart } = this.props;
    const {
      graphData,
      selectPeriod,
      selectConfiguration,
      configurations
    } = this.state;
    const periodItems = [
      { key: "day", value: "1 day" },
      { key: "week", value: "1 week" },
      { key: "month", value: "1 month" },
      { key: "3 months", value: "3 months" },
      { key: "6 months", value: "6 months" },
      { key: "1 year", value: "1 year" }
    ];

    return (
      <Paper elevation={3} className={classes.paper}>
        <Grid container spacing={4} className={classes.gridContainer}>
          <Grid item xs={12} className={classes.itemHeading}>
            <Typography
              variant="body1"
              className={classes.graphHeading}
              align="left"
            >
              {`Duration of ${clickedPichart} executions for ${clickedCard}`}
            </Typography>
          </Grid>
          <Grid item xs={12} className={classes.dropdownDiv}>
            <FormControl
              className={classes.field}
              variant="outlined"
              size="small"
            >
              <InputLabel htmlFor="selectPeriod">Period</InputLabel>
              <Select
                onChange={this.handleChange}
                value={selectPeriod}
                inputProps={{
                  name: "selectPeriod",
                  id: "selectPeriod"
                }}
              >
                <MenuItem value="" disabled>
                  Period
                </MenuItem>
                {periodItems.map(item => {
                  return (
                    <MenuItem value={item.value} key={item.key}>
                      {item.value}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
            <FormControl
              className={classes.field}
              variant="outlined"
              size="small"
            >
              <InputLabel htmlFor="selectConfiguration">
                Configuration
              </InputLabel>
              <Select
                onChange={this.handleChange}
                value={selectConfiguration}
                inputProps={{
                  name: "selectConfiguration",
                  id: "selectConfiguration"
                }}
              >
                <MenuItem value="" disabled>
                  Configuration
                </MenuItem>
                {configurations
                  ? configurations.map(item => {
                      return (
                        <MenuItem value={item} key={item}>
                          {item}
                        </MenuItem>
                      );
                    })
                  : null}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} className={classes.graphdiv}>
            {//graphData !== null ? this.renderGraph() : <CircularProgress />}
            this.renderGraph()}
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

export default connect(mapStateToProps, {})(withStyles(styles)(LineGraph));
