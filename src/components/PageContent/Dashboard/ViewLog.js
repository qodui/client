import React from "react";
import PropTypes from "prop-types";
import { Button, withStyles, Typography } from "@material-ui/core";
import { connect } from "react-redux";
import { getLogs, clearLogs } from "actions";
import _ from "lodash";
import { CircularProgress } from '@material-ui/core';

import MyModal from "layout/MyModal";

const styles = theme => ({
	content: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		padding: theme.spacing(2),
		whiteSpace: "pre-line",
		fontFamily: "Monospace",
		fontSize: "1.5rem"
	}
});

class ViewLog extends React.Component {
	state = { open: false };


	componentDidMount(){
	};

	componentWillUnmount(){
		
	};

	openModal = async () => {
		this.props.getLogs(this.props.job_id);
		this.setState({
			open: true
		});
	};

	getContent() {
		const { classes, logs } = this.props;
		return (_.isEmpty(logs)? <CircularProgress/> :
			<Typography
				variant="body1"
				display="block"
				gutterBottom
				className={classes.content}
			>
				{logs}
			</Typography>
			
		);
	}

	render() {
		return (
			<>
				<Button
					variant="outlined"
					color="secondary"
					size="small"
					onClick={this.openModal}
				>
					View Logs
				</Button>
				<MyModal
					open={this.state.open}
					onClose={() => this.setState({ open: false })}
					title={this.props.title}
					content={this.getContent()}
					actionButtons={
						<Button
							onClick={() => {
								this.props.clearLogs();
								this.setState({ open: false })
							}}
							color="primary"
							size="large"
							variant="outlined"
						>
							CLOSE
						</Button>
					}
				/>
			</>
		);
	}
}

ViewLog.propTypes = {
	classes: PropTypes.object.isRequired,
	title: PropTypes.string.isRequired,
	job_id: PropTypes.string.isRequired
};

const mapStateToProps = state => {
	return {
		logs: _.values(state.logs)
	};
  };

export default connect(mapStateToProps,{
	getLogs,
	clearLogs
})(withStyles(styles)(ViewLog));
