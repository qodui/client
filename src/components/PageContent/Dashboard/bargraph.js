import React from "react";
import { connect } from "react-redux";
import {
  withStyles,
  Typography,
  Grid,
  LinearProgress,
  Paper,
  Box,
  InputLabel,
  MenuItem,
  Select,
  FormControl,
  CircularProgress
} from "@material-ui/core";
import _ from "lodash";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  Label,
  Text,
  Brush
} from "recharts";
import axios from "axios";
import store from "store";
import { DJANGO_URL } from "constants.js";

const styles = theme => ({
  graphdiv: {
    justifyContent: "center",
    display: "flex",
    flexDirection: "row"
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    minHeight: "425px",
    padding: theme.spacing(1),
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    alignContent: "center"
  },
  graphHeading: {
    color: "#3f51b5",
    textTransform: "capitalize"
  },
  gridContainer: {},
  itemHeading: {
    background: "LightGray",
    margin: theme.spacing(1)
  },
  dropdownDiv: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "right"
  },
  field: {
    width: "10%",
    margin: theme.spacing(0.5)
  }
});
const INITIAL_STATE = {
  loading: false,
  graphData: null,
  selectPeriod: "1 day",
  selectConfiguration: "",
  configurations: false
};
class Bargraph extends React.Component {
  state = INITIAL_STATE;

  getAndSetGraphData = async (testarea, period, testType) => {
    // console.log(
    //   `getAndSetGraphData for area: ${testarea}, period: ${period} and type: ${testType}`
    // );
    let response = null;
    let url = `${DJANGO_URL}/dashboard/barchartdata/`;
    let executions = [];
    //console.log(`carddata url: ${url}`);
    response = await axios({
      method: "post",
      url: url,
      data: {
        testArea: testarea,
        period: period,
        testType: testType
      },
      headers: {
        "Content-Type": "application/json",
        authId: store.get("user").authId
      }
    })
      .then(response => {
        let data = {};
        let bargraphData = response.data;
        let configurations = Object.keys(bargraphData);
        configurations.forEach(configuration => {
          console.log(`configuration: ${configuration}`);
          executions = [];
          bargraphData[configuration].forEach(execution => {
            //console.log(`execution: ${JSON.stringify(execution, null, 2)}`);
            execution.appassureReport
              ? executions.push(execution.appassureReport)
              : executions.push(execution.sysassureReport);
          });
          data[configuration] = executions;
        });
        //console.log(`graphData: ${JSON.stringify(data, null, 2)}`);
        this.setState(
          {
            configurations: configurations,
            selectConfiguration: configurations[0],
            data: data,
            graphData: data[configurations[0]]
          },
          () => {
            //console.log(`state set: ${JSON.stringify(this.state)}`);
          }
        );
      })
      .catch(e => {
        console.log(
          `error in fetching barchartdata for area ${testarea}: ${e}`
        );
        this.setState(
          {
            configurations: [],
            selectConfiguration: "",
            graphData: null
          },
          () => {
            //console.log(`state set: ${JSON.stringify(this.state)}`);
          }
        );
      });
  };
  componentDidMount = async () => {
    const { clickedCard, clickedPichart } = this.props;
    const { selectPeriod } = this.state;
    //console.log(`Clicked card : ${clickedCard}`);
    await this.getAndSetGraphData(clickedCard, selectPeriod, clickedPichart);
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value }, () => {
      if (e.target.name === "selectPeriod") {
        this.getAndSetGraphData(
          this.props.clickedCard,
          this.state.selectPeriod,
          this.props.clickedPichart
        );
      } else if (e.target.name === "selectConfiguration") {
        this.setState({
          graphData: this.state.data[e.target.value]
        });
      }
    });
  };

  render() {
    const { classes, clickedCard, clickedPichart } = this.props;
    const {
      graphData,
      selectPeriod,
      selectConfiguration,
      configurations
    } = this.state;
    const periodItems = [
      { key: "day", value: "1 day" },
      { key: "week", value: "1 week" },
      { key: "month", value: "1 month" },
      { key: "3 months", value: "3 months" },
      { key: "6 months", value: "6 months" },
      { key: "1 year", value: "1 year" }
    ];

    return (
      <Paper elevation={1} className={classes.paper}>
        <Grid container spacing={4} className={classes.gridContainer}>
          <Grid item xs={12} className={classes.itemHeading}>
            <Typography
              variant="body1"
              className={classes.graphHeading}
              align="left"
            >
              {`${clickedPichart} executions for ${clickedCard}`}
            </Typography>
          </Grid>
          <Grid item xs={12} className={classes.dropdownDiv}>
            <FormControl
              className={classes.field}
              variant="outlined"
              size="small"
            >
              <InputLabel htmlFor="selectPeriod">Period</InputLabel>
              <Select
                onChange={this.handleChange}
                value={selectPeriod}
                inputProps={{
                  name: "selectPeriod",
                  id: "selectPeriod"
                }}
              >
                <MenuItem value="" disabled>
                  Period
                </MenuItem>
                {periodItems.map(item => {
                  return (
                    <MenuItem value={item.value} key={item.key}>
                      {item.value}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
            <FormControl
              className={classes.field}
              variant="outlined"
              size="small"
            >
              <InputLabel htmlFor="selectConfiguration">
                Configuration
              </InputLabel>
              <Select
                onChange={this.handleChange}
                value={selectConfiguration}
                inputProps={{
                  name: "selectConfiguration",
                  id: "selectConfiguration"
                }}
              >
                <MenuItem value="" disabled>
                  Configuration
                </MenuItem>
                {configurations
                  ? configurations.map(configuration => (
                      <MenuItem value={configuration} key={configuration}>
                        {configuration}
                      </MenuItem>
                    ))
                  : null}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} className={classes.graphdiv}>
            <ResponsiveContainer width="95%" height={300}>
              {
                <BarChart
                  //width={"70%"}
                  //height={"80%"}
                  data={graphData}
                  margin={{ top: 15, right: 40, left: 40, bottom: 15 }}
                  id="barchart"
                >
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis dataKey="name">
                    <Label
                      value="Execution"
                      offset={-15}
                      position="insideBottom"
                    />
                  </XAxis>
                  <YAxis yAxisId="left" orientation="left" stroke="#8884d8">
                    <Label
                      value={"requests"}
                      offset={-30}
                      position="insideLeft"
                      angle={90}
                    />
                  </YAxis>
                  <YAxis yAxisId="right" orientation="right" stroke="#6ab3cd">
                    <Label
                      value={"Throughput"}
                      offset={-30}
                      position="insideRight"
                      angle={90}
                    />
                  </YAxis>
                  <Tooltip />
                  <Legend iconType="circle" iconSize={10} align="left" />
                  <Bar
                    barSize={15}
                    yAxisId="left"
                    dataKey="passed"
                    fill="#00C49F"
                    stackId="a"
                  />
                  <Bar
                    barSize={15}
                    yAxisId="left"
                    dataKey="failed"
                    fill="Tomato"
                    stackId="a"
                  />
                  <Bar
                    barSize={15}
                    yAxisId="right"
                    dataKey="throughput"
                    fill="#6ab3cd"
                  />
                  <Brush dataKey="name">
                    <BarChart>
                      <Bar
                        barSize={15}
                        yAxisId="left"
                        dataKey="passed"
                        fill="#00C49F"
                        stackId="a"
                      />
                      <Bar
                        barSize={15}
                        yAxisId="left"
                        dataKey="failed"
                        fill="Tomato"
                        stackId="a"
                      />
                      <Bar
                        barSize={15}
                        yAxisId="right"
                        dataKey="throughput"
                        fill="#6ab3cd"
                      />
                    </BarChart>
                  </Brush>
                </BarChart>
              }
            </ResponsiveContainer>
          </Grid>
        </Grid>
      </Paper>
    );
  }

  // render(){
  //   return this.state.selectPeriod && this.state.selectConfiguration && this.state.graphData ? this.renderContent() : <CircularProgress/>
  // }
}

const mapStateToProps = state => {
  return {};
};

export default connect(mapStateToProps, {})(withStyles(styles)(Bargraph));
