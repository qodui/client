import React from "react";
import { connect } from "react-redux";
import {
  withStyles,
  Typography,
  //  Box,
  Grid
  //  LinearProgress
} from "@material-ui/core";
//import AppBar from "@material-ui/core/AppBar";
//import Table from "mui-datatables";
//import Tabs from "@material-ui/core/Tabs";
//import Tab from "@material-ui/core/Tab";
import _ from "lodash";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import LinearProgress from "@material-ui/core/LinearProgress";

import { toast } from "react-toastify";
import axios from "axios";
import store from "store";
import { DJANGO_URL } from "constants.js";
//import table from "content/dashboard/table";
import Badges from "./badges";
//import DashboardTables from "./tables";
import LineGraph from "./linegraph";
import Piecharts from "./piecharts";
import Bargraph from "./bargraph";
//import TabPanel from "./tabpanel";
//import RouterRoundedIcon from "@material-ui/icons/RouterRounded";
//import CloudRoundedIcon from "@material-ui/icons/CloudRounded";
//import PhoneIphoneRoundedIcon from "@material-ui/icons/PhoneIphoneRounded";
//import DevicesOtherRoundedIcon from "@material-ui/icons/DevicesOtherRounded";
import { setClickedCard } from "actions";
import python_backend from "api/python_backend";

const styles = theme => ({
  card: {
    width: "100%",
    borderRadius: 12
  },
  griditem: {
    display: "flex"
  },
  div_root: {},
  heading: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    paddingTop: "10px",
    color: "#3f51b5",
    fontVariant: "small-caps",
    fontSize: "16px"
  },
  tableHeading: {
    paddingTop: "5px",
    color: "#3f51b5",
    fontVariant: "Capilatize",
    align: "center"
  },
  wrapper: {
    maxWidth: "100%",
    margin: "10px"
  }
});

const INITIAL_STATE = {
  value: "",
  index: "",
  clickedCard: "",
  clickedPichart: "",
  cardData: null
};

class Dashboard extends React.Component {
  state = INITIAL_STATE;
  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MUIDataTableBodyCell: {
          root: {
            fontSize: "0.85rem"
          }
        },
        MUIDataTableHeadCell: {
          root: {
            fontSize: "1.0rem",
            fontWeight: "bold",
            fontVariant: "small-caps",
            color: "#3f51b5"
          }
        },
        MuiTableCell: {
          root: {
            overflow: "scroll"
          }
        }
      }
    });

  fetchCardData = async testarea => {
    let url = `${DJANGO_URL}/dashboard/carddata/?testarea=${testarea}`;
    //console.log(`carddata url: ${url}`);
    const response = await axios({
      method: "get",
      url: url,
      headers: {
        "Content-Type": "application/json",
        authId: store.get("user").authId
      }
    })
      .then(response => {
        //console.log(`carddata response: ${response.data}`);
        return response.data;
      })
      .catch(e => {
        //console.log(`error in fetching carddata for area ${testarea}: ${e}`);
      });
    // return {
    //   Integration: 1,
    //   Sanity: 0,
    //   Acceptance: 0,
    //   Regression: 0,
    //   Faulttolerance: 0,
    //   Scalability: 6,
    //   Performance: 5
    // };
  };

  setCardData = async () => {
    //console.log(`setCardData() called`);
    let response,
      sum,
      selectedType,
      clickedCard = null;
    let cardData = {};
    let data = null;
    let url = null;
    let testarea = null;
    for (testarea of ["edge", "devices", "cloud", "mobile"]) {
      url = `${DJANGO_URL}/dashboard/carddata/?testarea=${testarea}`;
      //console.log(`carddata url: ${url}`);
      response = await axios({
        method: "get",
        url: url,
        headers: {
          "Content-Type": "application/json",
          authId: store.get("user").authId
        }
      })
        .then(response => {
          sum = 0;
          data = response.data;
          let emptyFlag = false;
          //console.log(`data: ${JSON.stringify(Object.keys(data))}`);
          for (const key of Object.keys(data)) {
            if (selectedType == null && data[key] > 0) {
              if (clickedCard == null) {
                this.setClickedCard(testarea);
                emptyFlag = false;
              }
              selectedType = key;
            }else if(data[key] > 0){
              emptyFlag = true;
            }
            sum = sum + data[key];
            //console.log(`carddata for area ${testarea}: \n${JSON.stringify(data, null, 2)}`);
          } 
          
        })
        .catch(e => {
          console.log(
            `error in fetching carddata for area ${testarea}: ${e}`
          );
        });
        if(sum===0){
          //console.log("empty flag!!");
          this.setClickedCard("edge");
          selectedType = "Scalability";
        }
      cardData[testarea] = sum;

      //console.log(`cardData:${JSON.stringify(cardData)}`);
    }
    this.setState(
      {
        cardData: cardData,
        clickedPichart: selectedType
      },
      () => {
        //console.log(`card data set in state: ${JSON.stringify(this.state.cardData)}`);
      }
    );
  };

  componentDidMount = async () => {
    await this.setCardData();
  };

  componentWillUnmount() {}

  setClickedPichart = pichart => {
    this.setState(
      {
        clickedPichart: pichart
      },
      () => {
        //console.log(`clicked pichart in index: ${this.state.clickedPichart}`);
      }
    );
  };

  setClickedCard = card => {
    this.setState(
      {
        clickedCard: card
      },
      () => {
        //console.log(`clicked card in index: ${this.state.clickedCard}`);
      }
    );
  };

  renderContent = () => {
    const { classes } = this.props;
    const { cardData } = this.state;
    return (
      <div className={classes.div_root}>
        <Grid container spacing={5}>
          <Grid item xs={12}>
            <Badges
              clickedCard={this.state.clickedCard}
              setClickedCard={this.setClickedCard}
              cardData={cardData}
            />
          </Grid>
          <Grid item xs={12} className={classes.wrapper}>
            <Piecharts
              clickedCard={this.state.clickedCard}
              setClickedPichart={this.setClickedPichart}
            />
          </Grid>
          <Grid item xs={12} className={classes.wrapper}>
            <Bargraph
              clickedCard={this.state.clickedCard}
              clickedPichart={this.state.clickedPichart}
            />
          </Grid>
          <Grid item xs={12} className={classes.wrapper}>
            <LineGraph
              clickedCard={this.state.clickedCard}
              clickedPichart={this.state.clickedPichart}
            />
          </Grid>
        </Grid>
      </div>
    );
  };

  render() {
    return this.state.clickedCard &&
      this.state.clickedPichart &&
      this.state.cardData ? (
      this.renderContent()
    ) : (
      <LinearProgress color="secondary" />
    );
  }
}

const mapStateToProps = state => {
  return {
    clickedCard: state.dashboard.clickedCard
  };
};

export default connect(mapStateToProps, { setClickedCard })(
  withStyles(styles)(Dashboard)
);
