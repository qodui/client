import React from "react";
import { connect } from "react-redux";
import {
  withStyles,
  Typography,
  Grid,
  CircularProgress
} from "@material-ui/core";
import PropTypes from 'prop-types';
import Table from "mui-datatables";
import _ from "lodash";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import store from "store";
import { toast } from "react-toastify";
import table from "content/dashboard/table";
import { getCardData } from "actions";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActionArea from "@material-ui/core/CardActionArea";
import RouterRoundedIcon from "@material-ui/icons/RouterRounded";
import CloudRoundedIcon from "@material-ui/icons/CloudRounded";
import PhoneIphoneRoundedIcon from "@material-ui/icons/PhoneIphoneRounded";
import DevicesOtherRoundedIcon from "@material-ui/icons/DevicesOtherRounded";

const styles = theme => ({
  card: {
    width: "100%",
    borderRadius: 100,
    borderWidth: 2,
    borderColor: '#d6d7da',
  },
  griditem: {
    display: "flex"
  },
  cardcontent:{
    height: "100%"
  },
  gridContainer: {
  }
});

const INITIAL_STATE = {
  cardData: undefined,
  loading: true
};
class Badges extends React.Component {
  state = INITIAL_STATE;

  cardClicked = (e) => {
    this.setState({
      clickedCard : e.currentTarget.id
    }, () => {
      this.props.setClickedCard(this.state.clickedCard);
    });
  };

  getCardVariant = (card) =>{
    if(card === this.state.clickedCard){
      return "elevation";
    }else return "outlined";
  };

  componentDidMount() {
    //console.log(`card data: ${JSON.stringify(this.props.cardData, null, 2)}`);
    this.setState({
      clickedCard: this.props.clickedCard
    }, () => {
    })
  };
  render() {
    const { classes, cardData } = this.props;
    const { loading } = this.state;
    return (
      <Grid container spacing={2} className={classes.gridContainer}>
        <Grid item xs={3} className={classes.griditem}>
          <Card variant={this.getCardVariant("edge")} className={classes.card} raised={true}>
            <CardActionArea id={"edge"} onClick={(this.cardClicked)} className={classes.cardcontent}>
              <CardContent className={classes.cardcontent}>
                <Typography
                  gutterBottom
                  variant="h5"
                  component="h2"
                  align="center"
                  color="primary"
                >
                  <RouterRoundedIcon fontSize="large"/><br/>
                  Edge
                </Typography>
                <Typography
                  variant="h3"
                  component="h2"
                  align="center"
                  color="secondary"
                >
                  {!cardData ? <CircularProgress /> : cardData.edge}
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
        <Grid item xs={3} className={classes.griditem}>
          <Card variant={this.getCardVariant("devices")} className={classes.card} raised={true}>
          <CardActionArea id={"devices"} onClick={(this.cardClicked)} className={classes.cardcontent}>
            <CardContent className={classes.cardcontent}>
              <Typography
                gutterBottom
                variant="h5"
                component="h2"
                align="center"
                color="primary"
              >
                <DevicesOtherRoundedIcon fontSize="large"/><br/>
                Devices
              </Typography>
              <Typography
                variant="h3"
                component="h2"
                align="center"
                color="secondary"
              >
                {!cardData ? <CircularProgress /> : cardData.devices}
              </Typography>
            </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
        <Grid item xs={3} className={classes.griditem}>
          <Card variant={this.getCardVariant("cloud")} className={classes.card} raised={true}>
          <CardActionArea id={"cloud"} onClick={(this.cardClicked)} className={classes.cardcontent}>
            <CardContent className={classes.cardcontent}>
              <Typography
                gutterBottom
                variant="h5"
                component="h2"
                align="center"
                color="primary"
              >
                <CloudRoundedIcon fontSize="large"/><br/>
                Cloud
              </Typography>
              <Typography
                variant="h3"
                component="h2"
                align="center"
                color="secondary"
              >
                {!cardData ? <CircularProgress /> : cardData.cloud}
              </Typography>
            </CardContent >
            </CardActionArea>
          </Card>
        </Grid>
        <Grid item xs={3} className={classes.griditem}>
          <Card variant={this.getCardVariant("mobile")} className={classes.card} raised={true}>
          <CardActionArea id={"mobile"} onClick={(this.cardClicked)} className={classes.cardcontent}>
            <CardContent className={classes.cardcontent}>
              <Typography
                gutterBottom
                variant="h5"
                component="h2"
                align="center"
                color="primary"
              >
                <PhoneIphoneRoundedIcon fontSize="large"/><br/>
                Mobile
              </Typography>
              <Typography
                variant="h3"
                component="h2"
                align="center"
                color="secondary"
              >
                {!cardData ? <CircularProgress /> : cardData.mobile}
              </Typography>
            </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      </Grid>
    );
  }
}

Badges.propTypes = {
};

const mapStateToProps = state => {
  return {};
};

export default connect(mapStateToProps, {})(withStyles(styles)(Badges));
