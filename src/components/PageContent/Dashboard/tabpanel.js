import React from "react";
import { withStyles, Grid, TextField, Box } from "@material-ui/core";
import { connect } from "react-redux";
import _ from "lodash";

const styles = theme => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  heading: {
    paddingTop: "10px",
    color: "#3f51b5",
    fontVariant: "small-caps"
  },
  field: {
    width: "99%",
    margin: theme.spacing(1)
  },
  button: {
    margin: theme.spacing(1),
    width: "100%"
  },
  root: {
    width: "85%",
    margin: theme.spacing(1),
    alignItems: "center",
    justifyContent: "center"
  },
  progress: {
    width: "100%"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    flex: 1,
    overflowWrap: "break-word",
    width: "95%"
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3)
  },
  monitorContent: {
    alignItems: "center",
    justifyContent: "center"
  },
  logArea: {
    resize: "None",
    margin: "10px",
    width: "85%",
    maxHeight: "200px",
    minHeight: "100px",
    padding: "10px"
  },
  monitorLink: {
    paddingTop: "10px",
    marginBottom: "50px"
  },
  grid_root: {
    flexGrow: 1
  }
});
const INITIAL_STATE = {};

class TabPanel extends React.Component {
  state = INITIAL_STATE;

  componentDidMount = () => {
    const { index, value } = this.props;
    console.log(`value: ${value} index: ${index}`);
  };

  render() {
    const { index, classes, value } = this.props;
    let {} = this.state;

    if (value === undefined || value === null || value !== index) {
      return null;
    }
    return (
      <div
        className={classes.grid_root}
        aria-labelledby={`vertical-tab-${index}`}
        id={`vertical-tabpanel-${index}`}
        role="tabpanel"
        component="div"
        justify="center"
      >
        {value}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    configurations: _.values(state.configurations),
    environments: _.values(state.environments)
  };
};

export default connect(mapStateToProps, null)(withStyles(styles)(TabPanel));
