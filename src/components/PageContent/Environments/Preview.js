import React from "react";
import { withStyles, Paper, Typography } from "@material-ui/core";
import PropTypes from "prop-types";

const styles = theme => ({
	content: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		padding: theme.spacing(2),
		whiteSpace: "pre-line"
	},
	previewPaper: {
		maxHeight: "375px",
		minHeight: "375px",
		marginTop: "25px",
		overflow: "auto"
	},
	subheading: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		color: "#b71c1c",
		padding: theme.spacing(1)
	},
	heading: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		paddingTop: theme.spacing(1.5),
		color: "#3f51b5",
		fontVariant: "small-caps"
	}
});

class Preview extends React.Component {
	render() {
		const { classes, fileContent } = this.props;
		return (
			<Paper elevation={1} className={classes.previewPaper}>
				<Typography
					variant="h6"
					className={classes.heading}
					gutterBottom
				>
					PREVIEW
				</Typography>
				<Typography
					variant="subtitle1"
					className={classes.subheading}
					gutterBottom
				>
					The file uploaded should follow below template. Once
					uploaded, the same file can be previewed below.
				</Typography>
				<Typography
					variant="body1"
					display="block"
					gutterBottom
					className={classes.content}
				>
					{fileContent}
				</Typography>
			</Paper>
		);
	}
}

Preview.propTypes = {
	classes: PropTypes.object.isRequired,
	fileContent: PropTypes.string.isRequired
};

export default withStyles(styles)(Preview);
