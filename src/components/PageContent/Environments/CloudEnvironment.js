import React from "react";
import {
  withStyles,
  Paper,
  Grid,
  Stepper,
  Step,
  StepLabel,
  Typography,
  LinearProgress
} from "@material-ui/core";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import stepper from "content/environments/steps";
import CloudSelection from "./CloudSelection";
import CloudProperties from "./CloudProperties";
import CloudOptionalParameters from "./CloudOptionalParameters";
import ReviewDetails from "./ReviewDetails";
import {
  getEnvironmentProperties,
  clearEnvironmentProperties,
  createEnvironment
} from "actions";

const styles = theme => ({
  button: {
    marginRight: theme.spacing(1),
    marginBottom: theme.spacing(2)
  }
});

class CloudEnvironment extends React.Component {
  state = { activeStep: 0, isLoading: false };

  componentWillUnmount() {
    this.props.clearEnvironmentProperties();
  }

  getStepContent(step) {
    switch (step) {
      case 0:
        return <CloudSelection nextClick={this.handleNext} />;
      case 1:
        return (
          <CloudProperties
            type="packer"
            nextClick={this.handleNext}
            backClick={this.handleBack}
          />
        );
      case 2:
        return (
          <CloudProperties
            type="terraform"
            nextClick={this.handleNext}
            backClick={this.handleBack}
          />
        );
      case 3:
        return (
          <CloudOptionalParameters
            nextClick={this.handleNext}
            backClick={this.handleBack}
          />
        );
      case 4:
        return (
          <ReviewDetails
            createClick={this.handleCreate}
            backClick={this.handleBack}
          />
        );
      default:
        return <div>Unknown step</div>;
    }
  }

  handleNext = () => {
    this.setState({ activeStep: this.state.activeStep + 1 });
  };

  handleBack = () => {
    this.setState({ activeStep: this.state.activeStep - 1 });
  };

  handleCreate = async (packer, terraform) => {
    const { properties } = this.props;
    let environment = {
      name: properties["envName"].value,
      platform: properties["platform"].value.toUpperCase(),
      type: "CLOUD",
      terraform,
      packer,
      status: "To Be Created"
    };
    this.setState({ isLoading: true }, async () => {
      await this.props.createEnvironment(environment);
      this.props.clearEnvironmentProperties();
      this.setState({ activeStep: 0, isLoading: false });
    });
  };

  render() {
    const { activeStep } = this.state;
    return (
      <Paper elevation={3}>
        <Grid item xs={12}>
          <Stepper activeStep={activeStep} alternativeLabel>
            {stepper.steps.map(step => {
              return (
                <Step key={step.id}>
                  <StepLabel>
                    <Typography variant="body1">{step.label}</Typography>
                  </StepLabel>
                </Step>
              );
            })}
          </Stepper>
          {this.getStepContent(activeStep)}
        </Grid>
        {this.state.isLoading && <LinearProgress />}
      </Paper>
    );
  }
}

CloudEnvironment.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    properties: state.cloud
  };
};

export default connect(
  mapStateToProps,
  {
    getEnvironmentProperties,
    clearEnvironmentProperties,
    createEnvironment
  }
)(withStyles(styles)(CloudEnvironment));
