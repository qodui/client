import React from "react";
import { withStyles, Paper, Grid, TextField, Button } from "@material-ui/core";
import PropTypes from "prop-types";

const styles = theme => ({
	newIniFields: {
		paddingLeft: theme.spacing(1),
		paddingRight: theme.spacing(1),
		paddingBottom: theme.spacing(1)
	},
	input: {
		display: "none"
	},
	button: {
		margin: theme.spacing(1)
	}
});

class ExistingFile extends React.Component {
	handleFileSelection = file => {
		this.props.onFileSelection(file);
	};

	render() {
		const { classes } = this.props;
		return (
			<Paper elevation={1}>
				<Grid
					container
					alignItems="flex-start"
					justify="flex-end"
					direction="row"
				>
					<TextField
						className={classes.newIniFields}
						id="chosenFile"
						placeholder="File"
						helperText="Choose environment file"
						fullWidth
						margin="normal"
						readOnly
						value={this.props.selectedFile}
					/>
					<input
						accept="*"
						className={classes.input}
						id="uploadButton"
						type="file"
						onChange={e =>
							this.handleFileSelection(e.target.files[0])
						}
					/>
					<label htmlFor="uploadButton">
						<Button
							color="primary"
							variant="contained"
							component="span"
							className={classes.button}
						>
							CHOOSE FILE
						</Button>
					</label>
				</Grid>
			</Paper>
		);
	}
}

ExistingFile.propTypes = {
	classes: PropTypes.object.isRequired,
	onFileSelection: PropTypes.func.isRequired
};

export default withStyles(styles)(ExistingFile);
