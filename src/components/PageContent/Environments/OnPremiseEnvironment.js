import React from "react";
import {
  Grid,
  Paper,
  withStyles,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  Radio,
  FormControlLabel,
  Button,
  LinearProgress
} from "@material-ui/core";
import { connect } from "react-redux";

import Preview from "./Preview";
import NewFile from "./NewFile";
import ExistingFile from "./ExistingFile";
import { createEnvironment } from "actions";
import example from "content/environments/example";

const styles = theme => ({
  button: {
    margin: theme.spacing(1)
  },
  item: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    paddingBottom: theme.spacing(2)
  },
  paper: {
    minHeight: "431px"
  },
  newIniFields: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  }
});
let fileReader;

class OnPremiseEnvironment extends React.Component {
  state = {
    on: "existing",
    fileContent: "",
    name: "",
    isLoading: false,
    selectedFile: ""
  };

  componentDidMount() {
    this.setState({ fileContent: example.fileContent });
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleFileRead = e => {
    const content = fileReader.result;
    this.setState({ fileContent: content });
  };

  handleFileSelection = file => {
    this.setState({ selectedFile: file.name });
    fileReader = new FileReader();
    fileReader.onload = this.handleFileRead;
    fileReader.readAsText(file);
  };

  handleAddGroup = ({ group, values }) => {
    let { fileContent } = this.state;
    fileContent = `${fileContent}\n\n[${group}]\n${values.join("\n")}`;
    this.setState({ fileContent });
  };

  handleReset = () => {
    this.setState({ fileContent: example.fileContent, selectedFile: "" });
  };

  handleSaveEnv = () => {
    let environment = {
      name: this.state.name,
      platform: "ON PREMISE",
      type: "ON PREMISE",
      file: this.state.fileContent,
      status: "Pre-Existing"
    };
    this.setState({ isLoading: true }, async () => {
      await this.props.createEnvironment(environment);
      this.setState({
        on: "existing",
        fileContent: "",
        name: "",
        isLoading: false,
        selectedFile: ""
      });
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <>
        <Paper elevation={3} className={classes.paper}>
          <Grid container>
            <Grid item xs={6} className={classes.item}>
              <Grid container>
                <Grid item xs={12} className={classes.newIniFields}>
                  <FormControl fullWidth style={{ marginTop: "20px" }}>
                    <FormLabel htmlFor="on">Create Environment Using</FormLabel>
                    <RadioGroup
                      row
                      name="on"
                      value={this.state.on}
                      onChange={this.handleChange}
                    >
                      <FormControlLabel
                        value="existing"
                        control={<Radio color="primary" />}
                        label="Existing file"
                      />
                      <FormControlLabel
                        value="new"
                        control={<Radio color="primary" />}
                        label="New file"
                        disabled
                      />
                    </RadioGroup>
                  </FormControl>
                </Grid>
                <Grid item xs={12} className={classes.newIniFields}>
                  {this.state.on === "new" ? (
                    <NewFile addGroup={this.handleAddGroup} />
                  ) : (
                    <ExistingFile
                      onFileSelection={this.handleFileSelection}
                      selectedFile={this.state.selectedFile}
                    />
                  )}
                </Grid>
                <Grid item xs={12} className={classes.newIniFields}>
                  <TextField
                    required
                    name="name"
                    label="Environment Name"
                    margin="dense"
                    id="name"
                    value={this.state.name}
                    onChange={this.handleChange}
                    fullWidth
                  />
                </Grid>
                <Grid
                  container
                  alignItems="flex-start"
                  justify="flex-end"
                  direction="row"
                  className={classes.newInitFields}
                >
                  <Button
                    variant="outlined"
                    color="secondary"
                    className={classes.button}
                    onClick={this.handleReset}
                  >
                    Reset
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    disabled={
                      this.state.name === "" || this.state.fileContent === ""
                    }
                    onClick={this.handleSaveEnv}
                  >
                    SAVE ENVIRONMENT
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={6} className={classes.item}>
              <Preview fileContent={this.state.fileContent} />
            </Grid>
          </Grid>
        </Paper>
        {this.state.isLoading && <LinearProgress />}
      </>
    );
  }
}

export default connect(
  null,
  { createEnvironment }
)(withStyles(styles)(OnPremiseEnvironment));
