import React from "react";
import { connect } from "react-redux";
import {
	withStyles,
	Typography,
	Box,
	Grid,
	LinearProgress,
	Tabs,
	Tab
} from "@material-ui/core";
import Table from "mui-datatables";
import _ from "lodash";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import store from "store";
import { toast } from "react-toastify";

import table from "content/environments/table";
import {
	getEnvironments,
	deleteEnvironment,
	createEnvironment,
	clearEnvironments
} from "actions";
import OnPremiseEnvironment from "./OnPremiseEnvironment";
import TabPanel from "layout/TabPanel";
import CloudEnvironment from "./CloudEnvironment";

const styles = theme => ({
	heading: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		paddingTop: "10px",
		color: "#3f51b5",
		fontVariant: "small-caps",
		fontSize: "16px"
	},
	tableHeading: {
		paddingTop: "10px",
		color: "#3f51b5",
		fontVariant: "small-caps"
	},
	wrapper: {
		maxWidth: "97%",
		marginLeft: "20px"
	}
});

const INITIAL_STATE = {
	loading: false,
	tab: 0
};

class Environments extends React.Component {
	state = INITIAL_STATE;

	getMuiTheme = () =>
		createMuiTheme({
			overrides: {
				MUIDataTableBodyCell: {
					root: {
						fontSize: "1.2rem"
					}
				},
				MUIDataTableHeadCell: {
					root: {
						fontSize: "1.2rem",
						fontWeight: "bold",
						fontVariant: "small-caps",
						color: "#3f51b5"
					}
				}
			}
		});

	componentDidMount() {
		window.scrollTo(0, 0);
		this.setState({ loading: true }, async () => {
			await this.props.getEnvironments();
			this.setState({ loading: false });
		});
	}

	componentWillUnmount() {
		this.props.clearEnvironments();
	}

	handleDelete = rowsDeleted => {
		this.setState({ loading: true }, async () => {
			const envsTobeDeleted = rowsDeleted.data.map(
				d => this.props.environments[d.dataIndex].name
			);
			const idsTobeDeleted = rowsDeleted.data.map(
				d => this.props.environments[d.dataIndex]._id
			);
			await this.props.deleteEnvironment(envsTobeDeleted, idsTobeDeleted);
			this.setState({ loading: false });
		});
	};

	handleTableChange = (action, tableState) => {
		if (action === "rowDelete") {
			tableState.page = 0;
		}
	};

	handleTabChange = (event, newValue) => {
		this.setState({ tab: newValue });
	};

	render() {
		const { classes } = this.props;
		const { loading, tab } = this.state;
		const { capabilities } = store.get("user");
		let create =
			_.findIndex(capabilities, cap => {
				return cap.name === "Create_Environment";
			}) !== -1;
		let del =
			_.findIndex(capabilities, cap => {
				return cap.name === "Delete_Environment";
			}) !== -1;
		return (
			<Grid container spacing={2}>
				{create && (
					<Grid item xs={12}>
						<Tabs
							value={tab}
							onChange={this.handleTabChange}
							indicatorColor="secondary"
							centered
						>
							<Tab
								label={
									<Typography
										variant="subtitle1"
										gutterBottom
										className={classes.heading}
									>
										Existing Environment
									</Typography>
								}
							/>
							<Tab
								label={
									<Typography
										variant="subtitle1"
										gutterBottom
										className={classes.heading}
									>
										New Cloud Environment
									</Typography>
								}
							/>
						</Tabs>
						<TabPanel value={tab} index={0}>
							<OnPremiseEnvironment />
						</TabPanel>
						<TabPanel value={tab} index={1}>
							<CloudEnvironment />
						</TabPanel>
					</Grid>
				)}
				<Grid item xs={12}>
					<div className={classes.wrapper}>
						{loading && <LinearProgress />}
						<MuiThemeProvider theme={this.getMuiTheme()}>
							<Table
								title={
									<Typography
										variant="h4"
										gutterBottom
										className={classes.tableHeading}
									>
										<Box fontWeight="fontWeightBold">
											ENVIRONMENTS
										</Box>
									</Typography>
								}
								columns={table.columns}
								data={this.props.environments}
								options={{
									...table.options,
									onRowsDelete: rowsDeleted => {
										if (del) this.handleDelete(rowsDeleted);
										else {
											toast.error(
												"Unauthorized to perform this operation"
											);
											return false;
										}
									},
									onTableChange: (action, tableState) => {
										this.handleTableChange(
											action,
											tableState
										);
									}
								}}
							/>
						</MuiThemeProvider>
					</div>
				</Grid>
			</Grid>
		);
	}
}

const mapStateToProps = state => {
	return {
		environments: _.values(state.environments)
	};
};

export default connect(
	mapStateToProps,
	{
		getEnvironments,
		deleteEnvironment,
		createEnvironment,
		clearEnvironments
	}
)(withStyles(styles)(Environments));
