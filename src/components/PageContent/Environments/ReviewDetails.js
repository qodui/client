import React from "react";
import { Paper, withStyles, Typography, Grid, Button } from "@material-ui/core";
import { connect } from "react-redux";

import { getEnvironmentProperties } from "actions";
import params from "content/environments/parameters";

const styles = theme => ({
  content: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    padding: theme.spacing(2),
    whiteSpace: "pre-line",
    fontFamily: "Monospace",
    fontSize: "1.5rem"
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  previewPaper: {
    overflow: "auto",
    maxHeight: "45vh",
    marginBottom: theme.spacing(2),
    marginLeft: theme.spacing(1)
  },
  heading: {
    color: "#3f51b5",
    fontVariant: "small-caps"
  },
  button: {
    marginRight: theme.spacing(1),
    marginBottom: theme.spacing(2)
  },
  subheading: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    color: "#b71c1c",
    padding: theme.spacing(1),
    fontVariant: "small-caps",
    fontSize: "1.2rem"
  }
});

let packer = {};
let tf = "";

class ReviewDetails extends React.Component {
  async componentDidMount() {
    await this.props.getEnvironmentProperties();
  }

  componentWillUnmount() {
    packer = {};
    tf = "";
  }

  getTerraformFile() {
    tf = ""
    const { properties, classes } = this.props;
    let keys = Object.keys(properties);
    let platform = properties["platform"] ? properties["platform"].value : "";
    keys.forEach(key => {
      let property = properties[key];
      if (Array.isArray(property.type) && property.type.includes("terraform")) {
        if (platform === "gcp") {
          tf += `variable "${key}" {\n\tdescription = "${params.gcp.terraform[property.name].helper}"\ndefault = "${property.value}"\n}\n\n`;
        } else {
          tf += `variable "${key}" {\n\tdefault = "${property.value}"\n}\n\n`;
        }
      }
    });
    return (
      <Typography
        variant="body1"
        display="block"
        gutterBottom
        className={classes.content}
      >
        {tf}
      </Typography>
    );
  }

  getPackerJson() {
    const { properties, classes } = this.props;
    let keys = Object.keys(properties);
    keys.forEach(key => {
      let property = properties[key];
      if (Array.isArray(property.type) && property.type.includes("packer")) {
        packer[key] = property.value;
      }
    });
    return (
      <Typography
        variant="body1"
        display="block"
        gutterBottom
        className={classes.content}
      >
        {JSON.stringify(packer, null, 2)}
      </Typography>
    );
  }

  render() {
    const { classes } = this.props;
    return (
      <Paper elevation={0} className={classes.paper}>
        <Typography variant="h6" className={classes.heading} gutterBottom>
          PREVIEW
        </Typography>
        <Grid container spacing={1}>
          <Grid item xs={6}>
            <Typography
              variant="body1"
              gutterBottom
              className={classes.subheading}
            >
              Packer .json file
            </Typography>
            <Paper elevation={2} className={classes.previewPaper}>
              {this.getPackerJson()}
            </Paper>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="body1"
              gutterBottom
              className={classes.subheading}
            >
              Terraform .tf file
            </Typography>
            <Paper elevation={2} className={classes.previewPaper}>
              {this.getTerraformFile()}
            </Paper>
          </Grid>
        </Grid>
        <Grid
          container
          alignItems="flex-start"
          justify="flex-end"
          direction="row"
        >
          <Button
            onClick={() => this.props.backClick()}
            className={classes.button}
            variant="outlined"
          >
            Back
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={() => this.props.createClick(packer, tf)}
            className={classes.button}
          >
            Create
          </Button>
        </Grid>
      </Paper>
    );
  }
}

const mapStateToProps = state => {
  return {
    properties: state.cloud
  };
};

export default connect(
  mapStateToProps,
  {
    getEnvironmentProperties
  }
)(withStyles(styles)(ReviewDetails));
