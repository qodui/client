import React from "react";
import {
	withStyles,
	Paper,
	Grid,
	TextField,
	List,
	ListItem,
	ListItemText,
	ListSubheader,
	ListItemSecondaryAction,
	IconButton,
	Button
} from "@material-ui/core";
import PropTypes from "prop-types";
import DeleteIcon from "@material-ui/icons/Delete";

const styles = theme => ({
	newIniFields: {
		paddingLeft: theme.spacing(1),
		paddingRight: theme.spacing(1),
		paddingBottom: theme.spacing(1)
	},
	list: {
		overflow: "auto",
		display: "flex",
		flexDirection: "row",
		padding: 0
	},
	button: {
		margin: theme.spacing(1)
	}
});

class NewFile extends React.Component {
	state = { group: "", gValue: "", values: [] };

	handleChange = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	handleValueAdd = e => {
		let { values, gValue } = this.state;
		if (e.key === "Enter" && gValue !== "") {
			values.push(gValue);
			this.setState({ values, gValue: "" });
		}
	};

	handleDeleteValue = index => {
		let { values } = this.state;
		values.splice(index, 1);
		this.setState({ values });
	};

	handleAddGroup = () => {
		let { group, values } = this.state;
		this.setState({ group: "", gValue: "", values: [] });
		this.props.addGroup({ group, values });
	};

	render() {
		const { classes } = this.props;
		return (
			<Paper elevation={1}>
				<Grid container>
					<Grid xs={6} className={classes.newIniFields}>
						<TextField
							required
							value={this.state.group}
							onChange={this.handleChange}
							name="group"
							label="Group Name"
							margin="dense"
							id="group"
							fullWidth
						/>
					</Grid>
					<Grid xs={6} className={classes.newIniFields}>
						<TextField
							value={this.state.gValue}
							onChange={this.handleChange}
							onKeyDown={this.handleValueAdd}
							name="gValue"
							label="Group Value (hit enter to add to list)"
							margin="dense"
							id="gValue"
							fullWidth
						/>
					</Grid>
					<Grid xs={12} className={classes.newIniFields}>
						<List
							className={classes.list}
							subheader={<ListSubheader>Values *</ListSubheader>}
						>
							{this.state.values.map((value, index) => {
								return (
									<ListItem key={index}>
										<ListItemText primary={value} />
										<ListItemSecondaryAction>
											<IconButton
												edge="end"
												aria-label="delete"
												onClick={() =>
													this.handleDeleteValue(
														index
													)
												}
											>
												<DeleteIcon />
											</IconButton>
										</ListItemSecondaryAction>
									</ListItem>
								);
							})}
						</List>
					</Grid>
					<Grid
						container
						alignItems="flex-start"
						justify="flex-end"
						direction="row"
						xs={12}
						className={classes.newInitFields}
					>
						<Button
							variant="contained"
							color="primary"
							className={classes.button}
							onClick={this.handleAddGroup}
							disabled={
								this.state.group === "" ||
								this.state.values.length === 0
							}
						>
							Add Group
						</Button>
					</Grid>
				</Grid>
			</Paper>
		);
	}
}

NewFile.propTypes = {
	classes: PropTypes.object.isRequired,
	addGroup: PropTypes.func.isRequired
};

export default withStyles(styles)(NewFile);
