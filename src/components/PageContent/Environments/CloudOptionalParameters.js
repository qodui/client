import React from "react";
import {
	withStyles,
	Paper,
	FormControlLabel,
	Grid,
	Button,
	Checkbox,
	FormControl,
	FormLabel,
	FormGroup,
	FormHelperText,
	TextField,
	Chip
} from "@material-ui/core";
import _ from "lodash";
import { connect } from "react-redux";

import {
	getEnvironmentProperties,
	setEnvironmentProperty,
	deleteEnvironmentProperty
} from "actions";

const styles = theme => ({
	paper: {
		padding: theme.spacing(2),
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center"
	},
	chipArray: {
		display: "flex",
		justifyContent: "center",
		flexWrap: "wrap",
		margin: theme.spacing(2)
	},
	button: {
		marginRight: theme.spacing(1),
		marginBottom: theme.spacing(2)
	},
	formControl: {
		marginLeft: theme.spacing(3),
		marginRight: theme.spacing(3)
	},
	chip: {
		margin: theme.spacing(0.5)
	}
});

class CloudOptionalParameters extends React.Component {
	state = {
		packer: false,
		terraform: false,
		propertyName: "",
		propertyValue: "",
		stateProperties: []
	};

	async componentDidMount() {
		await this.props.getEnvironmentProperties();
		let { properties } = this.props;
		let keys = Object.keys(properties).filter(
			p => properties[p].additional
		);
		let stateProperties = [];
		keys.forEach(p => {
			stateProperties.push(properties[p]);
		});
		this.setState({ stateProperties });
	}

	componentWillUnmount() {
		this.state.stateProperties.forEach(p => {
			this.props.setEnvironmentProperty(p);
		});
	}

	handleCheckChange = value => {
		this.setState({ [value]: !this.state[value] });
	};

	handleChange = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	handleAdd = () => {
		let newProp = { name: "", value: "", type: [], additional: true };
		let {
			packer,
			terraform,
			propertyName,
			propertyValue,
			stateProperties
		} = this.state;
		if (packer) newProp.type.push("packer");
		if (terraform) newProp.type.push("terraform");
		newProp.name = propertyName;
		newProp.value = propertyValue;
		if (!stateProperties.includes(newProp)) {
			stateProperties.push(newProp);
			this.setState({ stateProperties });
		}
		this.setState({
			packer: false,
			terraform: false,
			propertyName: "",
			propertyValue: ""
		});
	};

	handleDelete = property => {
		let { stateProperties } = this.state;
		stateProperties.splice(
			_.findIndex(stateProperties, p => p === property),
			1
		);
		this.props.deleteEnvironmentProperty(property);
		this.setState({ stateProperties });
	};

	render() {
		const { classes } = this.props;
		const {
			packer,
			terraform,
			propertyName,
			propertyValue,
			stateProperties
		} = this.state;
		return (
			<Paper elevation={0} className={classes.paper}>
				<Grid container>
					<Grid item xs={3}>
						<FormControl
							component="fieldset"
							className={classes.formControl}
						>
							<FormLabel component="legend">
								Property Type
							</FormLabel>
							<FormHelperText>
								Please select one or both, if you want to add
								additional properties. Note, this step is
								optional
							</FormHelperText>
							<FormGroup>
								<FormControlLabel
									control={
										<Checkbox
											checked={packer}
											onChange={() =>
												this.handleCheckChange("packer")
											}
											value="packer"
										/>
									}
									label="Packer Property"
								/>
								<FormControlLabel
									control={
										<Checkbox
											checked={terraform}
											onChange={() =>
												this.handleCheckChange(
													"terraform"
												)
											}
											value="terraform"
										/>
									}
									label="Terraform Property"
								/>
							</FormGroup>
						</FormControl>
					</Grid>
					<Grid item xs={3}>
						<Grid
							container
							alignItems="flex-start"
							justify="flex-end"
							direction="row"
						>
							<Grid item xs={12}>
								<TextField
									name="propertyName"
									label="Property Name"
									margin="dense"
									value={propertyName}
									className={classes.field}
									onChange={this.handleChange}
									fullWidth
								/>
							</Grid>
							<Grid item xs={12}>
								<TextField
									name="propertyValue"
									label="Property Value"
									margin="dense"
									value={propertyValue}
									className={classes.field}
									onChange={this.handleChange}
									fullWidth
								/>
							</Grid>
							<Button
								variant="contained"
								color="secondary"
								onClick={this.handleAdd}
								disabled={
									propertyName === "" ||
									propertyValue === "" ||
									!(packer || terraform)
								}
							>
								ADD
							</Button>
						</Grid>
					</Grid>
					<Grid item xs={6}>
						<Paper className={classes.chipArray}>
							{stateProperties &&
								stateProperties.map((p, index) => {
									return (
										<Chip
											key={index}
											label={p.name}
											onDelete={() =>
												this.handleDelete(p)
											}
											className={classes.chip}
										/>
									);
								})}
						</Paper>
					</Grid>
				</Grid>
				<Grid
					container
					alignItems="flex-start"
					justify="flex-end"
					direction="row"
				>
					<Button
						onClick={() => this.props.backClick()}
						className={classes.button}
						variant="outlined"
					>
						Back
					</Button>
					<Button
						variant="contained"
						color="primary"
						onClick={() => this.props.nextClick()}
						className={classes.button}
					>
						Next
					</Button>
				</Grid>
			</Paper>
		);
	}
}

const mapStateToProps = state => {
	return {
		properties: state.cloud
	};
};

export default connect(
	mapStateToProps,
	{
		getEnvironmentProperties,
		setEnvironmentProperty,
		deleteEnvironmentProperty
	}
)(withStyles(styles)(CloudOptionalParameters));
