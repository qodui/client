import React from "react";
import PropTypes from "prop-types";
import { Button, withStyles, Typography } from "@material-ui/core";

import MyModal from "layout/MyModal";

const styles = theme => ({
	content: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		padding: theme.spacing(2),
		whiteSpace: "pre-line",
		fontFamily: "Monospace",
		fontSize: "1.5rem"
	}
});

class ViewFile extends React.Component {
	state = { open: false };

	openModal = () => {
		console.log("Coming here???");
		this.setState({ open: true });
	};

	getContent() {
		const { classes } = this.props;
		return (
			<Typography
				variant="body1"
				display="block"
				gutterBottom
				className={classes.content}
			>
				{this.props.content}
			</Typography>
		);
	}

	render() {
		return (
			<>
				<Button
					variant="outlined"
					color="secondary"
					size="small"
					onClick={this.openModal}
				>
					View File
				</Button>
				<MyModal
					open={this.state.open}
					onClose={() => this.setState({ open: false })}
					title={this.props.title}
					content={this.getContent()}
					actionButtons={
						<Button
							onClick={() => this.setState({ open: false })}
							color="primary"
							size="large"
							variant="outlined"
						>
							CLOSE
						</Button>
					}
				/>
			</>
		);
	}
}

ViewFile.propTypes = {
	classes: PropTypes.object.isRequired,
	title: PropTypes.string.isRequired,
	content: PropTypes.string.isRequired
};

export default withStyles(styles)(ViewFile);
