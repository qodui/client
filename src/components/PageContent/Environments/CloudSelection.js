import React from "react";
import {
  Paper,
  withStyles,
  FormControlLabel,
  RadioGroup,
  Radio,
  TextField,
  Grid,
  Button,
  Box
} from "@material-ui/core";
import { connect } from "react-redux";
import _ from "lodash";

import {
  getEnvironmentProperties,
  setEnvironmentProperty,
  clearEnvironmentProperties
} from "actions";

const styles = theme => ({
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  field: {
    width: "50%"
  },
  button: {
    marginRight: theme.spacing(1),
    marginBottom: theme.spacing(2)
  },
  radio: {
    padding: theme.spacing(1),
    fontSize: theme.typography.pxToRem(15)
  }
});

const platforms = [
  { name: "Amazon Web Services", key: "aws", disabled: false },
  { name: "Google Cloud Platform", key: "gcp", disabled: false },
  { name: "Azure", key: "azure", disabled: true },
  { name: "Rack Space", key: "rack", disabled: true },
  { name: "Digital Ocean", key: "do", disabled: true }
];

class CloudSelection extends React.Component {
  state = { properties: [] };

  async componentDidMount() {
    await this.props.getEnvironmentProperties();
    let properties = [];
    let storeProps = _.pick(this.props.properties, "platform", "envName");
    let storeProperties = Object.keys(storeProps);
    storeProperties.forEach((prop, index) => {
      properties.push({
        name: prop,
        value: storeProps[prop].value,
        type: "user-defined"
      });
      if (index === storeProperties.length - 1) {
        this.setState({ properties });
      }
    });
  }

  componentWillUnmount() {
    this.state.properties.forEach(p => {
      this.props.setEnvironmentProperty(p);
    });
  }

  handleChange = e => {
    const { properties } = this.state;
    const index = properties.findIndex(p => p.name === e.target.name);
    if (e.target.name === "platform" && index !== -1) {
      this.props.clearEnvironmentProperties();
      this.setState({ properties: [] });
    }
    if (e.target.value.trim() === "" && index === -1) {
    } else {
      if (index === -1) {
        properties.push({
          name: e.target.name,
          value: e.target.value.trim(),
          type: "user-defined"
        });
      } else {
        properties.splice(index, 1, {
          name: e.target.name,
          value: e.target.value.trim(),
          type: "user-defined"
        });
      }
    }
    this.setState({ properties });
  };

  render() {
    const { classes } = this.props;
    let disabled;
    if (this.state.properties.length === 2) {
      disabled = this.state.properties
        .map(p => {
          return p.value === "";
        })
        .reduce((disable, pv) => {
          return disable || pv;
        });
    } else {
      disabled = true;
    }
    return (
      <Paper elevation={0} className={classes.paper}>
        <RadioGroup
          row
          aria-label="platform"
          name="platform"
          defaultValue={
            this.props.properties.platform
              ? this.props.properties.platform.value
              : ""
          }
          onChange={this.handleChange}
        >
          {platforms.map(platform => {
            return (
              <FormControlLabel
                key={platform.key}
                value={platform.key}
                control={<Radio color="secondary" />}
                label={
                  <Box fontSize={16} m={1}>
                    {platform.name}
                  </Box>
                }
                className={classes.radio}
                disabled={platform.disabled}
              />
            );
          })}
        </RadioGroup>
        <TextField
          required
          name="envName"
          label="Environment Name"
          margin="dense"
          id="envName"
          defaultValue={
            this.props.properties.envName
              ? this.props.properties.envName.value
              : ""
          }
          onChange={this.handleChange}
          className={classes.field}
          fullWidth
        />

        <Grid
          container
          alignItems="flex-start"
          justify="flex-end"
          direction="row"
          className={classes.newInitFields}
        >
          <Button
            variant="contained"
            color="primary"
            onClick={() => this.props.nextClick()}
            className={classes.button}
            disabled={disabled}
          >
            Next
          </Button>
        </Grid>
      </Paper>
    );
  }
}

const mapStateToProps = state => {
  return {
    properties: state.cloud
  };
};

export default connect(
  mapStateToProps,
  {
    getEnvironmentProperties,
    setEnvironmentProperty,
    clearEnvironmentProperties
  }
)(withStyles(styles)(CloudSelection));
