import React from "react";
import {
  Paper,
  withStyles,
  Grid,
  MenuItem,
  TextField,
  Select,
  FormControl,
  InputLabel,
  Button,
  FormHelperText
} from "@material-ui/core";
import { connect } from "react-redux";
import _ from "lodash";
import PropTypes from "prop-types";

import params from "content/environments/parameters";
import {
  getEnvironmentProperties,
  setEnvironmentProperty
} from "actions";

const styles = theme => ({
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  field: {
    margin: theme.spacing(2),
    width: "90%"
  },
  button: {
    marginRight: theme.spacing(1),
    marginBottom: theme.spacing(2)
  }
});

class CloudProperties extends React.Component {
  state = { platformParams: {}, stateProperties: [] };

  async initialize() {
    await this.props.getEnvironmentProperties();
    let { type, properties } = this.props;
    this.setState(
      {
        platformParams: params[properties.platform.value][type]
      },
      () => {
        let storeProps = _.omit(properties, "platform", "envName");
        let stateProperties = [];
        let keys = Object.keys(this.state.platformParams);
        keys.forEach((key, index) => {
          if (_.has(storeProps, key)) {
            stateProperties.push({
              name: key,
              value: storeProps[key].value,
              type: [...storeProps[key].type, type]
            });
          } else {
            stateProperties.push({
              name: key,
              value: this.state.platformParams[key].defaultValue,
              type: [type]
            });
          }
          if (index === keys.length - 1) {
            this.setState({ stateProperties });
          }
        });
      }
    );
  }

  async componentDidMount() {
    await this.initialize();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.type !== this.props.type) {
      this.state.stateProperties.forEach(p => {
        this.props.setEnvironmentProperty(p);
      });
      this.setState({ stateProperties: [] }, async () => {
        await this.initialize();
      });
    }
  }

  componentWillUnmount() {
    this.state.stateProperties.forEach(p => {
      this.props.setEnvironmentProperty(p);
    });
  }

  handleChange = e => {
    const { stateProperties } = this.state;
    const index = stateProperties.findIndex(p => p.name === e.target.name);
    let value = isNaN(e.target.value) ? e.target.value.trim() : e.target.value;
    if (value !== "" && index === -1) {
    } else {
      if (index === -1) {
        stateProperties.push({
          name: e.target.name,
          value,
          type: [this.props.type]
        });
      } else {
        stateProperties.splice(index, 1, {
          name: e.target.name,
          value,
          type: !stateProperties[index].type.includes(this.props.type)
            ? [...stateProperties[index].type, this.props.type]
            : stateProperties[index].type
        });
      }
    }
    this.setState({ stateProperties });
  };

  renderSelect(param) {
    const { classes } = this.props;
    const { platformParams, stateProperties } = this.state;
    const parameter = platformParams[param];
    const index = stateProperties.findIndex(p => p.name === param);
    return (
      <FormControl className={classes.field} fullWidth>
        <InputLabel htmlFor={`${param}-helper`} required={parameter.required}>
          {parameter.label}
        </InputLabel>
        <Select
          onChange={this.handleChange}
          value={
            index === -1 ? parameter.defaultValue : stateProperties[index].value
          }
          inputProps={{
            name: param,
            id: `${param}-helper`
          }}
        >
          {parameter.options.map((option, index) => {
            return (
              <MenuItem value={option} key={index}>
                {isNaN(option) ? option.toUpperCase() : option}
              </MenuItem>
            );
          })}
        </Select>
        <FormHelperText>{parameter.helper}</FormHelperText>
      </FormControl>
    );
  }

  renderTextField(param) {
    const { classes } = this.props;
    const { platformParams, stateProperties } = this.state;
    const parameter = platformParams[param];
    const index = stateProperties.findIndex(p => p.name === param);
    return (
      <TextField
        type={parameter.type}
        error={parameter.required && stateProperties[index].value === ""}
        required={parameter.required}
        InputProps={{
          readOnly: parameter.readOnly
        }}
        name={param}
        label={parameter.label}
        margin="dense"
        id={param}
        defaultValue={
          index === -1 ? parameter.defaultValue : stateProperties[index].value
        }
        className={classes.field}
        onChange={this.handleChange}
        fullWidth
        helperText={parameter.helper}
      />
    );
  }

  renderGrid() {
    const { platformParams } = this.state;
    return Object.keys(platformParams).map((param, index) => {
      return (
        <Grid item xs={4} key={index}>
          {platformParams[param].type === "select"
            ? this.renderSelect(param)
            : this.renderTextField(param)}
        </Grid>
      );
    });
  }

  render() {
    const { classes } = this.props;
    const { stateProperties, platformParams } = this.state;
    let disabled;
    if (
      stateProperties.length > 0 &&
      stateProperties.length === Object.keys(platformParams).length
    ) {
      disabled = stateProperties
        .map(p => {
          switch (p.name) {
            case "profile":
              if (p.value === "") {
                return (
                  _.find(stateProperties, pV => pV.name === "aws_access_key")
                    .value === "" &&
                  _.find(stateProperties, pV => pV.name === "aws_secret_key")
                    .value === ""
                );
              } else {
                return false;
              }
            case "aws_access_key":
            case "aws_secret_key":
              if (p.value === "") {
                return (
                  _.find(stateProperties, pV => pV.name === "profile").value ===
                  ""
                );
              } else {
                return false;
              }
            default:
              return platformParams[p.name].required ? p.value === "" : false;
          }
        })
        .reduce((disable = true, pv) => {
          return disable || pv;
        });
    } else {
      disabled = true;
    }
    return (
      <Paper elevation={0} className={classes.paper}>
        {this.state.stateProperties.length > 0 && (
          <Grid container>{this.renderGrid()}</Grid>
        )}
        <Grid
          container
          alignItems="flex-start"
          justify="flex-end"
          direction="row"
        >
          <Button
            onClick={() => this.props.backClick()}
            className={classes.button}
            variant="outlined"
          >
            Back
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={() => this.props.nextClick()}
            className={classes.button}
            disabled={disabled}
          >
            Next
          </Button>
        </Grid>
      </Paper>
    );
  }
}

const mapStateToProps = state => {
  return {
    properties: state.cloud
  };
};

CloudProperties.propTypes = {
  type: PropTypes.string.isRequired,
  nextClick: PropTypes.func.isRequired,
  backClick: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  { getEnvironmentProperties, setEnvironmentProperty }
)(withStyles(styles)(CloudProperties));
