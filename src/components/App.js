import React from "react";
import { Switch, Route, Router } from "react-router-dom";
import store from "store";
import { connect } from "react-redux";

import history from "../history";
import Login from "./Login";
import Landing from "./Landing";
import { logout } from "actions";

class App extends React.Component {
	componentDidMount() {
		this.renderRedirect();
		setInterval(() => {
			if (
				!store.get("isLoggedIn") &&
				window.location.pathname !== "/login"
			) {
				this.props.logout();
			}
		}, 3600000);
	}

	renderRedirect() {
		if (!store.get("isLoggedIn")) {
			history.replace("/login");
		}
	}

	render() {
		return (
			<Router history={history}>
				<Switch>
					<Route path="/login" exact component={Login} />
					<Route path="/" exact component={Landing} />
				</Switch>
			</Router>
		);
	}
}

export default connect(
	null,
	{ logout }
)(App);
