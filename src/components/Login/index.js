import React from "react";
import {
	Button,
	CssBaseline,
	TextField,
	Paper,
	Grid,
	Box,
	Typography,
	LinearProgress
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import _ from "lodash";
import { toast } from "react-toastify";

import { login } from "actions";
import logo from "resources/images/wipro-logo.png";
import Info from "./Info";
import background from "resources/images/background.jpg";

const styles = theme => ({
	root: {
		height: "100vh"
	},
	image: {
		backgroundImage: `url(${background})`,
		backgroundSize: "cover",
		backgroundPosition: "center"
	},
	paper: {
		margin: theme.spacing(8, 4),
		display: "flex",
		flexDirection: "column",
		alignItems: "center"
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main
	},
	form: {
		width: "100%", // Fix IE 11 issue.
		marginTop: theme.spacing(1)
	},
	submit: {
		margin: theme.spacing(3, 0, 2)
	}
});

const INITIAL_STATE = { userId: "", password: "", loading: false };

class Login extends React.Component {
	state = INITIAL_STATE;

	handleChange = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	handleSubmit = async e => {
		e.preventDefault();
		if (this.state.userId === "" || this.state.password === "") {
			toast.error("User ID and/or Password missing.");
		} else {
			this.setState({ loading: true }, async () => {
				try {
					await this.props.login(_.omit(this.state, "clicked"));
				} catch {
					this.setState(INITIAL_STATE);
				}
			});
		}
	};

	render() {
		const { classes } = this.props;
		return (
			<Grid container component="main" className={classes.root}>
				<CssBaseline />
				<Grid
					item
					xs={false}
					sm={6}
					md={9}
					component={Paper}
					square
					className={classes.image}
				>
					<Info />
				</Grid>
				<Grid
					item
					xs={12}
					sm={6}
					md={3}
					component={Paper}
					elevation={2}
					square
				>
					<div className={classes.paper}>
						<img src={logo} alt="fireSpot" />
						<form className={classes.form} autoComplete="off">
							<TextField
								autoComplete="off"
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="userId"
								label="User ID"
								name="userId"
								autoFocus
								value={this.state.userId}
								onChange={this.handleChange}
							/>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								name="password"
								label="Password"
								type="password"
								id="password"
								value={this.state.password}
								onChange={this.handleChange}
							/>
							<Button
								size="large"
								type="submit"
								fullWidth
								variant="contained"
								color="primary"
								className={classes.submit}
								onClick={this.handleSubmit}
							>
								Login
							</Button>
							{this.state.loading && (
								<LinearProgress
									color="primary"
									variant="query"
								/>
							)}
							<Box mt={5}>
								<Typography
									variant="body2"
									color="textSecondary"
									align="center"
								>
									{"Copyright © "}
									WIPRO {` ${new Date().getFullYear()}`}
									{"."}
								</Typography>
							</Box>
						</form>
					</div>
				</Grid>
			</Grid>
		);
	}
}

Login.propTypes = {
	classes: PropTypes.object.isRequired
};

export default connect(
	null,
	{ login }
)(withStyles(styles)(Login));
