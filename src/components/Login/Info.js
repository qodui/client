import React from "react";
import { Typography, Tab, Tabs, Button } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import VisibilityTwoToneIcon from "@material-ui/icons/VisibilityTwoTone";
import VpnKeyTwoToneIcon from "@material-ui/icons/VpnKeyTwoTone";
import DeveloperBoardTwoToneIcon from "@material-ui/icons/DeveloperBoardTwoTone";
import BuildTwoToneIcon from "@material-ui/icons/BuildTwoTone";
import MuiExpansionPanel from "@material-ui/core/ExpansionPanel";
import MuiExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import MuiExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";

import TabPanel from "layout/TabPanel";
import panels from "content/info/pov";
import tools from "content/info/fat";
import kvp from "content/info/kvp";
import dom from "content/info/dom";
import MyModal from "layout/MyModal";
import framework from "resources/images/FrameWork.png";

const ExpansionPanel = withStyles({
	root: {
		opacity: 0.9,
		backgroundColor: "black"
	},
	expanded: {}
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
	root: {
		"&$expanded": {
			minHeight: 56
		},
		backgroundColor: "black"
	},
	content: {
		"&$expanded": {
			margin: "12px 0"
		}
	},
	expanded: {}
})(MuiExpansionPanelSummary);

const ExpansionPanelDetails = withStyles(theme => ({
	root: {
		padding: theme.spacing(2)
	}
}))(MuiExpansionPanelDetails);

const styles = theme => ({
	root: {
		display: "flex",
		flexDirection: "column"
	},
	heading: {
		fontSize: theme.typography.pxToRem(22),
		flexBasis: "33.33%",
		flexShrink: 0
	},
	secondaryHeading: {
		fontSize: theme.typography.pxToRem(15),
		color: theme.palette.text.secondary
	},
	details: {
		fontSize: theme.typography.pxToRem(18)
	},
	paper: {
		margin: "25px",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		overflow: "auto",
		maxHeight: "100vh"
	},
	card: {
		minWidth: 275
	},
	button: {
		margin: theme.spacing(1)
	}
});

const INITIAL_STATE = {
	expanded: "",
	value: 0,
	toolsExpanded: "",
	kvpExpanded: "",
	domExpanded: "",
	showFrameWork: false
};

class Info extends React.Component {
	state = INITIAL_STATE;

	handleChange = panel => (event, isExpanded) => {
		this.setState({ expanded: isExpanded ? panel : false });
	};

	handleKvpChange = k => (event, isExpanded) => {
		this.setState({ kvpExpanded: isExpanded ? k : false });
	};

	handleDomChange = d => (event, isExpanded) => {
		this.setState({ domExpanded: isExpanded ? d : false });
	};

	handleToolChange = tool => (event, isExpanded) => {
		this.setState({ toolsExpanded: isExpanded ? tool : false });
	};

	handleTabChange = (event, newValue) => {
		this.setState({ ...INITIAL_STATE, value: newValue });
	};

	renderPanels() {
		const { classes } = this.props;
		let tabContent = [];
		let panelName = null;
		let changeFunction = null;
		switch (this.state.value) {
			case 0:
				tabContent = panels;
				panelName = this.state.expanded;
				changeFunction = name => this.handleChange(name);
				break;
			case 1:
				tabContent = tools;
				panelName = this.state.toolsExpanded;
				changeFunction = name => this.handleToolChange(name);
				break;
			case 2:
				tabContent = kvp;
				panelName = this.state.kvpExpanded;
				changeFunction = name => this.handleKvpChange(name);
				break;
			case 3:
				tabContent = dom;
				panelName = this.state.domExpanded;
				changeFunction = name => this.handleDomChange(name);
				break;
			default:
				break;
		}

		return tabContent.map(tc => {
			return (
				<ExpansionPanel
					expanded={panelName === tc.name}
					onChange={changeFunction(tc.name)}
					key={tc.name}
				>
					<ExpansionPanelSummary
						expandIcon={<ExpandMoreIcon />}
						aria-controls={`${tc.name}bh-content`}
						id={`${tc.name}bh-heade`}
					>
						<Typography
							className={classes.heading}
							style={{
								color: tc.backgroundColor
							}}
						>
							{tc.heading}
						</Typography>
						<Typography className={classes.secondaryHeading}>
							{tc.subheading}
						</Typography>
					</ExpansionPanelSummary>
					<ExpansionPanelDetails
						style={{
							backgroundColor: tc.backgroundColor,
							color: tc.color
						}}
					>
						{tc.details}
					</ExpansionPanelDetails>
				</ExpansionPanel>
			);
		});
	}

	renderModal() {
		return (
			<MyModal
				open={this.state.showFrameWork}
				onClose={() => this.setState({ showFrameWork: false })}
				content={<img src={framework} alt="dummy" />}
				title="QoD FRAMEWORK"
				actionButtons={
					<Button
						onClick={() => this.setState({ showFrameWork: false })}
						color="primary"
						size="large"
						variant="outlined"
					>
						CLOSE
					</Button>
				}
			/>
		);
	}

	render() {
		const { classes } = this.props;
		return (
			<div className={classes.paper}>
				<Typography
					variant="h2"
					component="h1"
					gutterBottom
					align="center"
					style={{ color: "white" }}
				>
					QUALITY OF DEVICES
				</Typography>
				<Typography
					variant="h6"
					gutterBottom
					style={{ fontWeight: "bold", color: "white" }}
					align="center"
					paragraph
				>
					Quality of Devices encompasses the overall quality of an IOT
					system - covering components from devices to the cloud, and
					processes such as onboarding to business usage.
				</Typography>
				<Tabs
					value={this.state.value}
					onChange={this.handleTabChange}
					indicatorColor="secondary"
					style={{ color: "white" }}
					centered
				>
					<Tab
						label="OUR POINT OF VIEW"
						icon={<VisibilityTwoToneIcon />}
					/>
					<Tab
						label="FRAMEWORK & TOOLS OFFERED"
						icon={<BuildTwoToneIcon />}
					/>
					<Tab
						label="KEY VALUE PROPOSITION"
						icon={<VpnKeyTwoToneIcon />}
					/>
					<Tab
						label="DELIVERY/OPERATING MODEL"
						icon={<DeveloperBoardTwoToneIcon />}
					/>
				</Tabs>
				<TabPanel value={this.state.value} index={0}>
					<div className={classes.root}>{this.renderPanels()}</div>
				</TabPanel>
				<TabPanel value={this.state.value} index={1}>
					<div className={classes.root}>
						{this.renderPanels()}
						<Button
							size="large"
							variant="contained"
							color="primary"
							className={classes.button}
							onClick={() =>
								this.setState({ showFrameWork: true })
							}
						>
							VIEW FRAMEWORK
						</Button>
						{this.renderModal()}
					</div>
				</TabPanel>
				<TabPanel value={this.state.value} index={2}>
					<div className={classes.root}>{this.renderPanels()}</div>
				</TabPanel>
				<TabPanel value={this.state.value} index={3}>
					<div className={classes.root}>{this.renderPanels()}</div>
				</TabPanel>
			</div>
		);
	}
}

Info.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Info);
