import _ from "lodash";

import { GET_USERS, ADD_USER, EDIT_USER, DELETE_USER, CLEAR_USERS } from "actions/types";

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case ADD_USER:
		case EDIT_USER:
			return { ...state, [action.payload._id]: action.payload };
		case GET_USERS:
			return { ...state, ..._.mapKeys(action.payload, "_id") };
		case DELETE_USER:
			return _.omit(state, action.payload);
		case CLEAR_USERS:
			return INITIAL_STATE;
		default:
			return state;
	}
};
