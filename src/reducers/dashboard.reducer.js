import _ from "lodash";
import {
	SET_CLICKED_CARD,
	GET_LINEGRAPH_DATA,
	GET_CARD_DATA,
	GET_BARGRAPH_DATA,
	GET_PICHART_DATA,
	CLEAR_GRAPH_DATA
} from "actions/types";

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case SET_CLICKED_CARD:
			return {...state, "clickedCard": action.payload };
		case GET_LINEGRAPH_DATA:
			return {...state, "linegraphData": action.payload};
		case GET_CARD_DATA:
			return {...state, "cardData": action.payload};
		case GET_BARGRAPH_DATA:
			return {...state, "bargraphData": action.payload};
		case GET_PICHART_DATA:
			return {...state, "pichartData" : action.payload};
		case CLEAR_GRAPH_DATA:
			return INITIAL_STATE;
		default:
			return state;
	}
};
