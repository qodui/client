import _ from "lodash";
import {
    CREATE_EXEC,
	GET_EXECS,
	CLEAR_EXEC
} from "actions/types";

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case CREATE_EXEC:
			return { ...state, [action.payload._id]: action.payload };
		case GET_EXECS:
			return { ...state, ..._.mapKeys(action.payload, "_id") };
		case CLEAR_EXEC:
			return INITIAL_STATE;
		default:
			return state;
	}
};
