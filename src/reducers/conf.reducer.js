import _ from "lodash";
import {
	CREATE_CONF,
	DELETE_CONF,
	GET_CONFS,
	CLEAR_CONF
} from "actions/types";

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case CREATE_CONF:
			return {[action.payload._id]: action.payload, ...state };
		case GET_CONFS:
			return { ...state, ..._.mapKeys(action.payload, "_id") };
		case DELETE_CONF:
			return _.omit(state, action.payload);
		case CLEAR_CONF:
			return INITIAL_STATE;
		default:
			return state;
	}
};
