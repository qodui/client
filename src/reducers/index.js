import { combineReducers } from "redux";
import users from "./user.mgmt.reducer";
import capabilities from "./cap.mgmt.reducer";
import roles from "./role.mgmt.reducer";
import environments from "./env.reducer";
import cloud from "./env.properties.reducer";
import configurations from "./conf.reducer";
import executions from "./exec.reducer";
import logs from "./log.reducer";
import dashboard from "./dashboard.reducer";
import cloudjob from "./cloudjob.reducer";
import { LOGOUT, LOGIN } from "actions/types";

const appReducers = combineReducers({
	users,
	capabilities,
	roles,
	environments,
	configurations,
	cloud,
	executions,
	logs,
	dashboard,
	cloudjob
});

const rootReducer = (state, action) => {
	if (action.type === LOGOUT) {
		state = undefined;
	} else if (action.type === LOGIN) {
		//DO Nothing to state.
	}
	return appReducers(state, action);
};

export default rootReducer;
