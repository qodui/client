import _ from "lodash";

import {
	GET_CAPABILITIES,
	ADD_CAPABILITY,
	EDIT_CAPABILITY,
	DELETE_CAPABILITY,
	CLEAR_CAPABILITIES
} from "actions/types";

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case ADD_CAPABILITY:
		case EDIT_CAPABILITY:
			return { ...state, [action.payload._id]: action.payload };
		case GET_CAPABILITIES:
			return { ...state, ..._.mapKeys(action.payload, "_id") };
		case DELETE_CAPABILITY:
			return _.omit(state, action.payload);
		case CLEAR_CAPABILITIES:
			return INITIAL_STATE;
		default:
			return state;
	}
};
