import _ from "lodash";
import { CREATE_ENV, DELETE_ENV, GET_ENVS, CLEAR_ENV } from "actions/types";

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case CREATE_ENV:
			return {[action.payload._id]: action.payload, ...state };
		case GET_ENVS:
			return { ...state, ..._.mapKeys(action.payload, "_id") };
		case DELETE_ENV:
			return _.omit(state, action.payload);
		case CLEAR_ENV:
			return INITIAL_STATE;
		default:
			return state;
	}
};
