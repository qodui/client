import _ from "lodash";
import {
    INITIALIZE_CLOUD_JOB,
    START_CLOUD_JOB,
    CLEAR_CLOUD_JOB,
    UPDATE_CLOUD_JOB
} from "actions/types";

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case INITIALIZE_CLOUD_JOB:
            return { ...state, "cloudjob":{
                success : false,
                loading: true
            } };
		case START_CLOUD_JOB:
			return { ...state, "cloudjob":{
                ...state.cloudjob,
                name: action.payload.executionName,
                selectEnvironment: action.payload.selectEnvironment,

            } };
        case UPDATE_CLOUD_JOB:
            return { ...state, "cloudjob":{
                loading: false,
                name: action.payload.executionName,
                success: action.payload.success
            } };
		case CLEAR_CLOUD_JOB:
			return INITIAL_STATE;
		default:
			return state;
	}
};
