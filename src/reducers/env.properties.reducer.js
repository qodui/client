import _ from "lodash";

import {
	SET_ENV_PROP,
	GET_ENV_PROP,
	CLEAR_ENV_PROP,
	DELETE_ENV_PROP
} from "actions/types";

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case SET_ENV_PROP:
			return { ...state, [action.payload.name]: action.payload };
		case DELETE_ENV_PROP:
			return _.omit(state, action.payload.name);
		case CLEAR_ENV_PROP:
			return INITIAL_STATE;
		case GET_ENV_PROP:
		default:
			return state;
	}
};
