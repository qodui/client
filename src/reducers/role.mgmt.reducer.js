import _ from "lodash";

import {
	GET_ROLES,
	ADD_ROLE,
	EDIT_ROLE,
	DELETE_ROLE,
	CLEAR_ROLES
} from "actions/types";

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case ADD_ROLE:
		case EDIT_ROLE:
			return { ...state, [action.payload._id]: action.payload };
		case GET_ROLES:
			return { ...state, ..._.mapKeys(action.payload, "_id") };
		case DELETE_ROLE:
			return _.omit(state, action.payload);
		case CLEAR_ROLES:
			return INITIAL_STATE;
		default:
			return state;
	}
};
