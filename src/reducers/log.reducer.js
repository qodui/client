import _ from "lodash";
import {
    GET_LOGS,
    CLEAR_LOGS
} from "actions/types";

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case GET_LOGS:
            return { ...state, logviewer_content: action.payload?action.payload:[] };
        case CLEAR_LOGS:
            return INITIAL_STATE;
		default:
			return state;
	}
};