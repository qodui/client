import React from "react";
import {
	Dialog,
	DialogContent,
	DialogTitle,
	Slide,
	DialogActions
} from "@material-ui/core";

const Transition = React.forwardRef(function Transition(props, ref) {
	return <Slide direction="up" ref={ref} {...props} />;
});
class MyModal extends React.Component {
	render() {
		return (
			<Dialog
				aria-labelledby="customized-dialog-title"
				open={this.props.open}
				onClose={this.props.onClose}
				maxWidth="lg"
				TransitionComponent={Transition}
			>
				<DialogTitle id="customized-dialog-title" color="#0d47a1">
					{this.props.title}
				</DialogTitle>
				<DialogContent>{this.props.content}</DialogContent>
				<DialogActions>{this.props.actionButtons}</DialogActions>
			</Dialog>
		);
	}
}

export default MyModal;
