import React from "react";
import { withStyles } from "@material-ui/core/styles";
import {
	ListItemIcon,
	Divider,
	Toolbar,
	Typography,
	IconButton,
	MenuItem,
	Tooltip,
	Zoom,
	Menu
} from "@material-ui/core";
import MuiAppBar from "@material-ui/core/AppBar";
import { MoreVert, AccountCircle } from "@material-ui/icons";

const styles = theme => ({
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
		background: "linear-gradient(#5c6bc0, #303f9f, #283593, #1a237e)"
	},
	drawer: {
		width: 240,
		flexShrink: 0
	},
	drawerPaper: {
		width: 240
	},
	content: {
		flexGrow: 1,
		padding: theme.spacing(3)
	},
	toolbar: theme.mixins.toolbar,
	nested: {
		paddingLeft: theme.spacing(4)
	}
});

class AppBar extends React.Component {
	state = { anchorEl: null };

	handleMenuClose = () => {
		this.setState({ anchorEl: null });
	};

	handleMenuOpen = event => {
		this.setState({ anchorEl: event.currentTarget });
	};

	handleItemClick = item => {
		this.props.handleItemClick(item);
		this.setState({ anchorEl: null });
	};

	renderMenu() {
		return (
			<Menu
				anchorEl={this.state.anchorEl}
				anchorOrigin={{ vertical: "top", horizontal: "right" }}
				id="primary-account-menu"
				keepMounted
				transformOrigin={{ vertical: "top", horizontal: "right" }}
				open={Boolean(this.state.anchorEl)}
				onClose={this.handleMenuClose}
			>
				<MenuItem onClick={() => this.handleItemClick("account")}>
					<ListItemIcon>
						<AccountCircle />
					</ListItemIcon>
					<Typography variant="h6">Account</Typography>
				</MenuItem>
				<MenuItem onClick={this.props.handleLogout}>
					<ListItemIcon>
						<i className="material-icons" fontSize="large">
							power_settings_new
						</i>
					</ListItemIcon>
					<Typography variant="h6">Logout</Typography>
				</MenuItem>
			</Menu>
		);
	}

	render() {
		const { classes } = this.props;
		return (
			<>
				<MuiAppBar position="fixed" className={classes.appBar}>
					<Toolbar>
						<Typography variant="h5" noWrap>
							QUALITY OF DEVICES
						</Typography>
						<Divider orientation="vertical" variant="middle" />
						<Typography variant="subtitle1">
							&copy; Wipro Technologies, 2019
						</Typography>
						<Tooltip
							title="More Options"
							enterDelay={500}
							leaveDelay={200}
							TransitionComponent={Zoom}
						>
							<IconButton
								edge="start"
								aria-label="account of current user"
								aria-controls="primary-account-menu"
								aria-haspopup="true"
								onClick={this.handleMenuOpen}
								style={{ marginLeft: "auto" }}
								color="inherit"
							>
								<MoreVert />{" "}
							</IconButton>
						</Tooltip>
					</Toolbar>
				</MuiAppBar>
				{this.renderMenu()}
			</>
		);
	}
}

export default withStyles(styles)(AppBar);
