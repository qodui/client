import React from "react";
import {
	withStyles,
	Drawer,
	MenuItem,
	ListItemIcon,
	ListItemText,
	Collapse,
	List,
	Badge
} from "@material-ui/core";
import ExpandMore from "@material-ui/icons/ExpandMore";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import HourglassEmptyIcon from "@material-ui/icons/HourglassEmpty";
import PropTypes from "prop-types";
import store from "store";

import drawerContent from "content/drawer/content";

const styles = theme => ({
	drawer: {
		width: 300,
		flexShrink: 0
	},
	drawerPaper: {
		width: 300
	},
	toolbar: theme.mixins.toolbar,
	nested: {
		paddingLeft: theme.spacing(4),
		paddingRight: theme.spacing(2)
	}
});

class Sidebar extends React.Component {
	handleItemClick = item => {
		this.props.handleItemClick(item);
	};

	handleExpand = menu => {
		this.props.handleExpand(menu);
	};

	renderChildren(subItems) {
		const { classes } = this.props;
		if (this.props.childrenLoading) {
			return (
				<MenuItem className={classes.nested}>
					<ListItemIcon>
						<HourglassEmptyIcon style={{ color: "white" }} />
					</ListItemIcon>
					<ListItemText primary="LOADING..." />
				</MenuItem>
			);
		} else {
			return subItems.map(subItem => {
				const show = this.props.currentUserCapabilities.includes(
					subItem.id
				);
				return (
					show && (
						<MenuItem
							key={subItem.id}
							button
							onClick={() => this.handleItemClick(subItem.id)}
							selected={subItem.id === this.props.selected}
							className={classes.nested}
						>
							<ListItemIcon>
								<Badge
									overlap="circle"
									color="secondary"
									variant="dot"
									invisible={
										subItem.id !== this.props.selected
									}
								>
									{subItem.icon}
								</Badge>
							</ListItemIcon>
							<ListItemText primary={subItem.title} />
						</MenuItem>
					)
				);
			});
		}
	}

	renderDrawerList() {
		let isAdmin = store.get("user") && store.get("user").role === "Admin";
		return drawerContent.map(item => {
			return item.admin ? (
				isAdmin && (
					<React.Fragment key={item.id}>
						<MenuItem
							button
							onClick={() =>
								!item.nested
									? this.handleItemClick(item.id)
									: this.handleExpand(item.id)
							}
							selected={item.id === this.props.selected}
						>
							<ListItemIcon>
								<Badge
									overlap="circle"
									color="secondary"
									variant="dot"
									invisible={item.id !== this.props.selected}
								>
									{item.icon}
								</Badge>
							</ListItemIcon>
							<ListItemText primary={item.title} />
							{item.nested === true &&
							this.props.expanded &&
							this.props.menu === item.id ? (
								<ExpandMore />
							) : (
								<ChevronRightIcon />
							)}
						</MenuItem>
						{item.nested && (
							<Collapse
								in={
									this.props.expanded &&
									this.props.menu === item.id
								}
								timeout="auto"
								unmountOnExit
							>
								<List component="div" disablePadding>
									{this.renderChildren(item.subItems)}
								</List>
							</Collapse>
						)}
					</React.Fragment>
				)
			) : (
				<React.Fragment key={item.id}>
					<MenuItem
						button
						onClick={() =>
							!item.nested
								? this.handleItemClick(item.id)
								: this.handleExpand(item.id)
						}
						selected={item.id === this.props.selected}
					>
						<ListItemIcon>
							<Badge
								overlap="circle"
								color="secondary"
								variant="dot"
								invisible={item.id !== this.props.selected}
							>
								{item.icon}
							</Badge>
						</ListItemIcon>
						<ListItemText primary={item.title} />
						{item.nested === true &&
							(this.props.expanded &&
							this.props.menu === item.id ? (
								<ExpandMore />
							) : (
								<ChevronRightIcon />
							))}
					</MenuItem>
					{item.nested && (
						<Collapse
							in={
								this.props.expanded &&
								this.props.menu === item.id
							}
							timeout="auto"
							unmountOnExit
						>
							<List component="div" disablePadding>
								{this.renderChildren(item.subItems)}
							</List>
						</Collapse>
					)}
				</React.Fragment>
			);
		});
	}

	render() {
		const { classes } = this.props;
		return (
			<Drawer
				variant="permanent"
				className={classes.drawer}
				classes={{
					paper: classes.drawerPaper
				}}
				PaperProps={{
					style: { backgroundColor: "#212121", color: "white" }
				}}
			>
				<div className={classes.toolbar} />
				{this.renderDrawerList()}
			</Drawer>
		);
	}
}

Sidebar.propTypes = {
	classes: PropTypes.object.isRequired,
	menu: PropTypes.string.isRequired,
	currentUserCapabilities: PropTypes.array.isRequired,
	selected: PropTypes.string.isRequired,
	expanded: PropTypes.bool.isRequired,
	childrenLoading: PropTypes.bool.isRequired
};

export default withStyles(styles)(Sidebar);
