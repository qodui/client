import React from "react";
import { ListItemText, Typography, ListItem } from "@material-ui/core";

class ItemText extends React.Component {
	render() {
		return (
			<ListItem>
				<ListItemText
					disableTypography
					primary={
						<Typography type="body2" style={{ fontSize: "14pt" }}>
							{this.props.text}
						</Typography>
					}
				/>
			</ListItem>
		);
	}
}

export default ItemText;
