/*Authentication*/
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";

/*User Management*/
export const GET_USERS = "GET_USERS";
export const ADD_USER = "ADD_USER";
export const EDIT_USER = "EDIT_USER";
export const DELETE_USER = "DELETE_USER";
export const CLEAR_USERS = "CLEAR_USERS";

/*Capability Management*/
export const GET_CAPABILITIES = "GET_CAPABILITIES";
export const ADD_CAPABILITY = "ADD_CAPABILITY";
export const EDIT_CAPABILITY = "EDIT_CAPABILITY";
export const DELETE_CAPABILITY = "DELETE_CAPABILITY";
export const CLEAR_CAPABILITIES = "CLEAR_CAPABILITIES";

/*Role Management*/
export const GET_ROLES = "GET_ROLES";
export const ADD_ROLE = "ADD_ROLE";
export const EDIT_ROLE = "EDIT_ROLE";
export const DELETE_ROLE = "DELETE_ROLE";
export const CLEAR_ROLES = "CLEAR_ROLES";

/*Environments */
export const GET_ENVS = "GET_ENVS";
export const CREATE_ENV = "CREATE_ENV";
export const DELETE_ENV = "DELETE_ENV";
export const CLEAR_ENV = "CLEAR_ENV";

/*Environment Properties */
export const SET_ENV_PROP = "SET_ENV_PROP";
export const CLEAR_ENV_PROP = "CLEAR_ENV_PROP";
export const GET_ENV_PROP = "GET_ENV_PROP";
export const DELETE_ENV_PROP = "DELETE_ENV_PROP";

/*Configurations */
export const GET_CONFS = "GET_CONFS";
export const CREATE_CONF = "CREATE_CONF";
export const DELETE_CONF = "DELETE_CONF";
export const CLEAR_CONF = "CLEAR_CONF";

/*Executions */
export const GET_EXECS = "GET_EXECS";
export const CREATE_EXEC = "CREATE_EXEC";
export const CLEAR_EXEC = "CLEAR_EXEC";
export const GET_PAST5_EXECS = "GET_PAST5_EXECS";

/*Logs*/
export const GET_LOGS = "GET_LOGS";
export const CLEAR_LOGS = "CLEAR_LOGS";

/*Dashboard*/
export const GET_LINEGRAPH_DATA = "GET_LINEGRAPH_DATA";
export const GET_CARD_DATA = "GET_CARD_DATA";
export const SET_CLICKED_CARD = "SET_CLICKED_CARD";
export const GET_BARGRAPH_DATA = "GET_BARGRAPH_DATA";
export const GET_PICHART_DATA = "GET_PICHART_DATA";
export const CLEAR_GRAPH_DATA = "CLEAR_GRAPH_DATA";

/*cloud job */
export const INITIALIZE_CLOUD_JOB = "INITIALIZE_CLOUD_JOB";
export const START_CLOUD_JOB = "START_CLOUD_JOB";
export const CLEAR_CLOUD_JOB = "CLEAR_CLOUD_JOB";
export const UPDATE_CLOUD_JOB = "UPDATE_CLOUD_JOB";

