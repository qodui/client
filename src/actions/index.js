import store from "store";
import { toast } from "react-toastify";
import _ from "lodash";

import bb from "api/bb";
import python_backend from "api/python_backend";
import history from "../history";
import {
  LOGIN,
  LOGOUT,
  GET_USERS,
  GET_CAPABILITIES,
  ADD_CAPABILITY,
  EDIT_CAPABILITY,
  DELETE_CAPABILITY,
  DELETE_USER,
  EDIT_USER,
  ADD_USER,
  GET_ROLES,
  ADD_ROLE,
  EDIT_ROLE,
  DELETE_ROLE,
  GET_ENVS,
  CREATE_ENV,
  DELETE_ENV,
  CLEAR_ENV,
  SET_ENV_PROP,
  GET_ENV_PROP,
  CLEAR_ENV_PROP,
  GET_CONFS,
  CREATE_CONF,
  DELETE_CONF,
  CLEAR_CONF,
  CLEAR_USERS,
  CLEAR_ROLES,
  CLEAR_CAPABILITIES,
  DELETE_ENV_PROP,
  CREATE_EXEC,
  GET_EXECS,
  GET_PAST5_EXECS,
  CLEAR_EXEC,
  GET_LOGS,
  CLEAR_LOGS,
  GET_LINEGRAPH_DATA,
  GET_CARD_DATA,
  SET_CLICKED_CARD,
  GET_BARGRAPH_DATA,
  GET_PICHART_DATA,
  CLEAR_GRAPH_DATA,
  INITIALIZE_CLOUD_JOB,
  START_CLOUD_JOB,
  CLEAR_CLOUD_JOB,
  UPDATE_CLOUD_JOB
} from "actions/types";
import { ContactsOutlined } from "@material-ui/icons";

const expirePlugin = require("store/plugins/expire");
store.addPlugin(expirePlugin);

export const login = formValues => async dispatch => {
  return new Promise((resolve, reject) => {
    bb.post("/sm/login", formValues)
      .then(response => {
        dispatch({ type: LOGIN });
        store.set(
          "user",
          response.data,
          new Date().getTime() + 1000 * 60 * 60 * 8
        );
        store.set(
          "isLoggedIn",
          true,
          new Date().getTime() + 1000 * 60 * 60 * 8
        );
        history.replace("/");
        resolve();
      })
      .catch(error => {
        dispatch({ type: LOGIN });
        if (!_.isEmpty(error)) toast.error(error.response.data.message);
        reject();
      });
  });
};

export const logout = () => async dispatch => {
  try {
    let { userId, authId } = store.get("user");
    await bb.post("/sm/logout", { userId, authId });
    dispatch({ type: LOGOUT });
    store.remove("user");
    store.remove("isLoggedIn");
  } catch (error) {
    dispatch({ type: LOGOUT });
    if (_.isEmpty(error)) {
      toast.info("Session expired. Please login again.", {
        autoClose: false
      });
    } else {
      toast.error(error.response.data.message);
    }
  } finally {
    history.replace("/login");
  }
};

export const getUsers = () => async dispatch => {
  try {
    const response = await bb.get("/userMgmt/users", {
      headers: { authid: store.get("user").authId }
    });

    dispatch({ type: GET_USERS, payload: response.data });
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const clearUsers = () => dispatch => {
  dispatch({ type: CLEAR_USERS });
};

export const addUser = newValues => async dispatch => {
  try {
    const response = await bb.post("/userMgmt/addUser", newValues, {
      headers: { authid: store.get("user").authId }
    });
    dispatch({ type: ADD_USER, payload: response.data });
    toast.success("User added successfully.");
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const editUser = (id, newValues) => async dispatch => {
  try {
    const response = await bb.put(`/userMgmt/editUser/${id}`, newValues, {
      headers: { authid: store.get("user").authId }
    });
    dispatch({ type: EDIT_USER, payload: response.data });
    let user = store.get("user");
    if (id === user._id) {
      user.name = newValues.name;
      store.set("user", user);
    }
    toast.success("User updated successfully.");
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const deleteUser = id => async dispatch => {
  try {
    await bb.delete(`/userMgmt/deleteUser/${id}`, {
      headers: { authid: store.get("user").authId }
    });
    dispatch({ type: DELETE_USER, payload: id });
    toast.success("User deleted successfully.");
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const getRoles = () => async dispatch => {
  try {
    const response = await bb.get("/roleMgmt/roles", {
      headers: { authid: store.get("user").authId }
    });

    dispatch({ type: GET_ROLES, payload: response.data });
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error);
  }
};

export const clearRoles = () => dispatch => {
  dispatch({ type: CLEAR_ROLES });
};

export const addRole = newValues => async dispatch => {
  try {
    const response = await bb.post("/roleMgmt/createRole", newValues, {
      headers: { authid: store.get("user").authId }
    });
    dispatch({ type: ADD_ROLE, payload: response.data });
    toast.success("Role Added successfully.");
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const editRole = (id, newValues) => async dispatch => {
  try {
    const response = await bb.put(`/roleMgmt/editRole/${id}`, newValues, {
      headers: { authid: store.get("user").authId }
    });
    dispatch({ type: EDIT_ROLE, payload: response.data });
    toast.success("Role Updated successfully.");
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const deleteRole = id => async dispatch => {
  try {
    await bb.delete(`/roleMgmt/deleteRole/${id}`, {
      headers: { authid: store.get("user").authId }
    });
    dispatch({ type: DELETE_ROLE, payload: id });
    toast.success("Role deleted successfully.");
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const getCapabilities = () => async dispatch => {
  try {
    const response = await bb.get("/capMgmt/capabilities", {
      headers: { authid: store.get("user").authId }
    });

    dispatch({ type: GET_CAPABILITIES, payload: response.data });
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error);
  }
};

export const clearCapabilities = () => dispatch => {
  dispatch({ type: CLEAR_CAPABILITIES });
};

export const addCapability = newValues => async dispatch => {
  try {
    const response = await bb.post("/capMgmt/createCapability", newValues, {
      headers: { authid: store.get("user").authId }
    });
    dispatch({ type: ADD_CAPABILITY, payload: response.data });
    toast.success("Capability Added successfully.");
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error);
  }
};

export const editCapability = (id, newValues) => async dispatch => {
  try {
    const response = await bb.put(`/capMgmt/editCapability/${id}`, newValues, {
      headers: { authid: store.get("user").authId }
    });
    dispatch({ type: EDIT_CAPABILITY, payload: response.data });
    toast.success("Capability Updated successfully.");
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error);
  }
};

export const deleteCapability = id => async dispatch => {
  try {
    await bb.delete(`/capMgmt/deleteCapability/${id}`, {
      headers: { authid: store.get("user").authId }
    });
    dispatch({ type: DELETE_CAPABILITY, payload: id });
    toast.success("Capability deleted successfully.");
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const getEnvironments = (status = "") => async dispatch => {
  let url =
    status === "free" ? "/environments/all/?status=free" : "/environments/all";
  try {
    const response = await bb.get(url, {
      headers: { authid: store.get("user").authId }
    });
    dispatch({ type: GET_ENVS, payload: response.data });
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const createEnvironment = newEnv => async dispatch => {
  try {
    let created = new Date();
    newEnv.created = `${created.getFullYear()}-${created.getMonth() +
      1}-${created.getDate()} ${created.getHours()}:${created.getMinutes()}:${created.getSeconds()}`;
    const response = await bb.post("/environments/create", newEnv, {
      headers: { authid: store.get("user").authId }
    });
    dispatch({ type: CREATE_ENV, payload: response.data });
    toast.success("Environment Created successfully.");
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const deleteEnvironment = (envs, ids) => async dispatch => {
  try {
    const response = await bb.delete("/environments/delete", {
      headers: { authid: store.get("user").authId },
      data: envs
    });
    dispatch({ type: DELETE_ENV, payload: ids });
    toast.success(response.data.message);
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const clearEnvironments = () => dispatch => {
  dispatch({ type: CLEAR_ENV });
};

export const setEnvironmentProperty = property => dispatch => {
  dispatch({ type: SET_ENV_PROP, payload: property });
};

export const deleteEnvironmentProperty = property => dispatch => {
  dispatch({ type: DELETE_ENV_PROP, payload: property });
};

export const getEnvironmentProperties = () => async dispatch => {
  dispatch({ type: GET_ENV_PROP });
};

export const clearEnvironmentProperties = () => dispatch => {
  dispatch({ type: CLEAR_ENV_PROP });
};

export const getConfigurations = () => async dispatch => {
  try {
    const response = await bb.get("/configurations/all", {
      headers: { authid: store.get("user").authId }
    });
    dispatch({ type: GET_CONFS, payload: response.data });
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const createConfiguration = newConf => async dispatch => {
  try {
    let created = new Date();
    newConf.created = `${created.getFullYear()}-${created.getMonth() +
      1}-${created.getDate()} ${created.getHours()}:${created.getMinutes()}:${created.getSeconds()}`;
    const response = await bb.post("/configurations/create", newConf, {
      headers: { authid: store.get("user").authId }
    });
    dispatch({ type: CREATE_CONF, payload: response.data });
    toast.success("Configuration Created successfully.");
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const deleteConfiguration = (confs, ids) => async dispatch => {
  try {
    const response = await bb.delete("/configurations/delete", {
      headers: { authid: store.get("user").authId },
      data: confs
    });
    dispatch({ type: DELETE_CONF, payload: ids });
    toast.success(response.data.message);
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const clearConfigurations = () => dispatch => {
  dispatch({ type: CLEAR_CONF });
};

export const getExecutions = (id = 0, purpose = "") => async dispatch => {
  try {
    const userObj = store.get("user");
    let response = null;
    if (purpose !== "reports") {
      let x = id === 0 ? "all/?purpose=executions" : `id/${id}`;
      let url = `/executions/${x}`;
      response = await bb.get(url, {
        headers: {
          authid: userObj.authId
        }
      });
    } else if (purpose === "reports") {
      response = await bb.get(`/executions/all/?purpose=reports`, {
        headers: {
          authid: userObj.authId
        }
      });
    }
    dispatch({ type: GET_EXECS, payload: response.data });
  } catch (error) {
    console.log(`error: ${error}`);
    if (!_.isEmpty(error)) toast.error(error);
  }
};

export const getPast5Executions = async () => {
  try {
    const response = await python_backend.get(`/dashboard/tabledata/`, {
      headers: {
        "Content-Type": "application/json",
        authId: store.get("user").authId
      }
    });
    return response.data;
  } catch (e) {
    console.log(e);
    return null;
  }
};

export const createExecution = newExec => async dispatch => {
  try {
    const userObj = store.get("user");
    const response = await bb.post("/executions/create", newExec, {
      headers: {
        authid: userObj.authId,
        userId: userObj.userId
      }
    });
    dispatch({ type: CREATE_EXEC, payload: response.data });
    toast.success("Execution Created successfully.");
  } catch (error) {
    if (!_.isEmpty(error)) {
      console.log(JSON.stringify(error, null, 2));
      toast.error(error.response.data.message);
    }
  }
};
export const clearExecutions = () => dispatch => {
  dispatch({ type: CLEAR_EXEC });
};

export const getLogs = jobId => async dispatch => {
  try {
    const response = await python_backend.get(`/logs/id/?id=${jobId}`, {
      headers: {
        "Content-Type": "application/json",
        authId: store.get("user").authId
      }
    });
    dispatch({ type: GET_LOGS, payload: response.data.logs });
  } catch (e) {
    console.log(e);
    return null;
  }
};
export const clearLogs = () => dispatch => {
  dispatch({ type: CLEAR_LOGS });
};

export const getLineGraphData = period => async dispatch => {
  console.log(`get line graph data called`);

  dispatch({
    type: GET_LINEGRAPH_DATA,
    payload: [
      {
        name: "Ex24",
        duration: 324
      },
      {
        name: "Ex29",
        duration: 102
      },
      {
        name: "Ex33",
        duration: 432
      },
      {
        name: "Ex36",
        duration: 286
      },
      {
        name: "Ex41",
        duration: 127
      },
      {
        name: "Ex47",
        duration: 521
      },
      {
        name: "EX51",
        duration: 226
      },
      {
        name: "EX58",
        duration: 428
      },
      {
        name: "EX59",
        duration: 576
      },
      {
        name: "EX67",
        duration: 658
      }
    ]
  });
};

export const getCardData = testarea => async dispatch => {
  console.log(`getCardData called for area: ${testarea}`);
  try {
    /*const response = await python_backend.get(`/dashboard/cardsdata/`, {
      headers: {
        "Content-Type": "application/json",
        authId: store.get("user").authId
      }
    });
    return response.data;*/
    dispatch({
      type: GET_CARD_DATA,
      payload: {
        Integration: 1,
        Sanity: 0,
        Acceptance: 0,
        Regression: 0,
        Faulttolerance: 0,
        Scalability: 6,
        Performance: 5
      }
    });
  } catch (e) {
    console.log(e);
    return null;
  }
};

export const getPichartData = testarea => async dispatch => {
  //console.log(`getPichartData called for area: ${testarea}`);
  try {
    /*const response = await python_backend.get(`/dashboard/cardsdata/`, {
      headers: {
        "Content-Type": "application/json",
        authId: store.get("user").authId
      }
    });
    return response.data;*/
    dispatch({
      type: GET_PICHART_DATA,
      payload: {
        Integration: {
          total_count: 5628,
          error_count: 1213
        },
        Sanity: {
          total_count: 4312,
          error_count: 1234
        },
        Acceptance: {
          total_count: 5743,
          error_count: 343
        },
        Regression: {
          total_count: 4534,
          error_count: 1223
        },
        Faulttolerance: {
          total_count: 7867,
          error_count: 2311
        },
        Scalability: {
          total_count: 9876,
          error_count: 1231
        },
        Performance: {
          total_count: 8798,
          error_count: 2342
        }
      }
    });
  } catch (e) {
    console.log(e);
    return null;
  }
};

export const getBargraphData = testarea => async dispatch => {
  //console.log(`getBargraphData called for area: ${testarea}`);
  try {
    /*const response = await python_backend.get(`/dashboard/cardsdata/`, {
      headers: {
        "Content-Type": "application/json",
        authId: store.get("user").authId
      }
    });
    return response.data;*/
    dispatch({
      type: GET_BARGRAPH_DATA,
      payload: {
        config1: [
          {
            _id: "ex1",
            configName: "config1",
            sysassureReport: {
              total_count: 8979,
              error_count: 1231
            }
          },
          {
            _id: "ex3",
            configName: "config1",
            sysassureReport: {
              total_count: 7463,
              error_count: 2312
            }
          }
        ],
        config2: [
          {
            _id: "ex4",
            configName: "config2",
            sysassureReport: {
              total_count: 9807,
              error_count: 2334
            }
          },
          {
            _id: "ex8",
            configName: "config2",
            sysassureReport: {
              total_count: 6756,
              error_count: 123
            }
          }
        ]
      }
    });
  } catch (e) {
    console.log(e);
    return null;
  }
};

export const getLinegraphData = testarea => async dispatch => {
  //console.log(`getLinegraphData called for area: ${testarea}`);
  try {
    /*const response = await python_backend.get(`/dashboard/cardsdata/`, {
      headers: {
        "Content-Type": "application/json",
        authId: store.get("user").authId
      }
    });
    return response.data;*/
    dispatch({
      type: GET_LINEGRAPH_DATA,
      payload: {
        config1: [
          {
            _id: "ex1",
            configName: "config1",
            duration: 43
          },
          {
            _id: "ex3",
            configName: "config1",
            duration: 59
          }
        ],
        config2: [
          {
            _id: "ex4",
            configName: "config2",
            duration: 32
          },
          {
            _id: "ex8",
            configName: "config2",
            duration: 40
          }
        ]
      }
    });
  } catch (e) {
    console.log(e);
    return null;
  }
};

export const setClickedCard = card => dispatch => {
  dispatch({ type: SET_CLICKED_CARD, payload: card });
};

export const clearGraphData = dispatch => {
  dispatch({
    type: CLEAR_GRAPH_DATA
  });
};
let timer = null;
export const initializeCloudJob = payload => async dispatch => {
  try {
    await dispatch({ type: INITIALIZE_CLOUD_JOB, payload: payload });
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const startCloudJob = payload => async dispatch => {
  try {
    const userObj = store.get("user");
    console.log(`sending request to django to create cloud`);
    const response = await python_backend.post("/cloud/create/", payload, {
      headers: {
        authid: userObj.authId,
        userId: userObj.userId,
        "Content-Type": "application/json",
      }
    });
    console.log(`response from django for cloud creation: ${JSON.stringify(response.data)}`);
    dispatch({ type: START_CLOUD_JOB, payload: response.data });
    dispatch({ type: CREATE_EXEC, payload: response.data });
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};

export const updateCloudJob = payload => async dispatch => {
  try {
    await dispatch({ type: UPDATE_CLOUD_JOB, payload: payload });
  } catch (error) {
    if (!_.isEmpty(error)) {
      console.log(JSON.stringify(error, null, 2));
      toast.error("could not create cloud env");
    }
  }
};

export const clearCloudJob = () => async dispatch => {
  try {
    clearTimeout(timer);
    dispatch({ type: CLEAR_CLOUD_JOB });
  } catch (error) {
    if (!_.isEmpty(error)) toast.error(error.response.data.message);
  }
};
