import React from "react";
import TuneTwoToneIcon from "@material-ui/icons/TuneTwoTone";
import DirectionsRunTwoToneIcon from "@material-ui/icons/DirectionsRunTwoTone";
import DescriptionOutlinedIcon from "@material-ui/icons/DescriptionOutlined";
import SupervisorAccountTwoToneIcon from "@material-ui/icons/SupervisorAccountTwoTone";
import VerifiedUserOutlinedIcon from "@material-ui/icons/VerifiedUserOutlined";
import ToggleOnOutlinedIcon from "@material-ui/icons/ToggleOnOutlined";
import PublicOutlinedIcon from "@material-ui/icons/PublicOutlined";
import DashboardOutlinedIcon from "@material-ui/icons/DashboardOutlined";
import PersonAddOutlinedIcon from '@material-ui/icons/PersonAddOutlined';


export default [
	{
		id: "dashboard",
		nested: false,
		icon: <DashboardOutlinedIcon style={{ color: "white" }} />,
		title: "DASHBOARD",
		admin: false
	},
	{
		id: "environment",
		nested: false,
		admin: false,
		icon: <PublicOutlinedIcon style={{ color: "white" }} />,
		title: "ENVIRONMENTS"
	},
	{
		id: "configurations",
		nested: false,
		icon: <TuneTwoToneIcon style={{ color: "white" }} />,
		title: "CONFIGURATIONS",
		admin: false
	},
	{
		id: "executions",
		nested: false,
		icon: <DirectionsRunTwoToneIcon style={{ color: "white" }} />,
		title: "EXECUTIONS",
		admin: false
	},
	{
		id: "reports",
		nested: false,
		icon: <DescriptionOutlinedIcon style={{ color: "white" }} />,
		title: "REPORTS",
		admin: false
	},
	{
		id: "admin",
		nested: true,
		admin: true,
		title: "ADMIN",
		icon: <SupervisorAccountTwoToneIcon style={{ color: "white" }} />,
		subItems: [
			{
				id: "users",
				title: "USER MANAGEMENT",
				icon: <PersonAddOutlinedIcon style={{ color: "white" }} />
			},
			{
				id: "roles",
				title: "ROLE MANAGEMENT",
				icon: <VerifiedUserOutlinedIcon style={{ color: "white" }} />
			},
			{
				id: "capabilities",
				title: "CAPABILITY MANAGEMENT",
				icon: <ToggleOnOutlinedIcon style={{ color: "white" }} />
			}
		]
	}
];
