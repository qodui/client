import edge from "resources/images/edge.png";
import device from "resources/images/device.svg";
import cloud from "resources/images/cloud.png";
import mobile from "resources/images/mobile.png";
import funct from "resources/images/function.png";
import nonfunctional from "resources/images/nonfunction.svg";
import overall from "resources/images/overall.svg";

export const edcm = [
	{
		id: "device",
		title: "DEVICE",
		subtitle: "Configurations for DEVICE",
		image: device,
		configurations: [
			{ id: 1, title: "Configuration 1", avatar: "C1", main: "DEVICE" },
			{ id: 2, title: "Configuration 2", avatar: "C2", main: "DEVICE" },
			{ id: 3, title: "Configuration 3", avatar: "C3", main: "DEVICE" }
		]
	},
	{
		id: "edge",
		title: "EDGE",
		subtitle: "Configurations for EDGE",
		image: edge,
		configurations: [
			{ id: 1, title: "Configuration 1", avatar: "C1", main: "EDGE" },
			{ id: 2, title: "Configuration 2", avatar: "C2", main: "EDGE" },
			{ id: 3, title: "Configuration 3", avatar: "C3", main: "EDGE" }
		]
	},
	{
		id: "cloud",
		title: "CLOUD",
		subtitle: "Configurations for CLOUD",
		image: cloud,
		configurations: [
			{ id: 1, title: "Configuration 1", avatar: "C1", main: "CLOUD" },
			{ id: 2, title: "Configuration 2", avatar: "C2", main: "CLOUD" },
			{ id: 3, title: "Configuration 3", avatar: "C3", main: "CLOUD" }
		]
	},
	{
		id: "mobile",
		title: "MOBILE",
		subtitle: "Configurations for MOBILE",
		image: mobile,
		configurations: [
			{ id: 1, title: "Configuration 1", avatar: "C1", main: "MOBILE" },
			{ id: 2, title: "Configuration 2", avatar: "C2", main: "MOBILE" },
			{ id: 3, title: "Configuration 3", avatar: "C3", main: "MOBILE" }
		]
	}
];

export const fno = [
	{
		id: 1,
		title: "FUNCTIONAL",
		image: funct,
		options: [
			{ id: 1, title: "Integration Testing" },
			{ id: 2, title: "API Testing" },
			{ id: 3, title: "AppAssure" }
		]
	},
	{
		id: 2,
		title: "NON FUNCTIONAL",
		image: nonfunctional,
		options: [
			{ id: 1, title: "Performance" },
			{ id: 2, title: "Scalability" },
			{ id: 3, title: "SysAssure" }
		]
	},
	{
		id: 3,
		title: "OVERALL",
		image: overall,
		options: [
			{ id: 1, title: "Status" },
			{ id: 2, title: "Reports" },
			{ id: 3, title: "Monitoring" }
		]
	}
];
