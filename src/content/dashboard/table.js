import React from "react";
import OpenInNewIcon from "@material-ui/icons/OpenInNew";
import { Link, Button } from "@material-ui/core";
import ViewLog from "components/PageContent/Dashboard/ViewLog";

export default {
  columns: [
    {
      name: "executionName",
      label: "Name",
      options: { filter: false, sort: true, viewColumns: false ,searchable: true, setCellProps: (value) => ({})},

    },
    {
      name: "envType",
      label: "Env Type",
      options: {
        filter: true,
        sort: true,
        searchable: true,
        customBodyRender: (value, { rowData }) => {
          return value ? value : "-----";
        }
      }
    },
    {
      name: "testType",
      label: "Test Type",
      options: {
        filter: true,
        sort: true,
        searchable: true,
        customBodyRender: (value, { rowData }) => {
          return value ? value : "-----";
        }
      }
    },
    /*{
      name: "created",
      label: "Date",
      options: { filter: true, sort: true }
    },*/
    /*{
      name: "selectEnvironment",
      label: "Environment",
      options: {
        filter: true,
        sort: true,
        searchable: true,
        customBodyRender: (value, { rowData }) => {
          return value ? value["name"] : "-----";
        }
      }
    },*/
    {
      name: "testArea",
      label: "Test Area",
      options: {
        filter: true,
        sort: true,
        searchable: true,
        customBodyRender: (value, { rowData }) => {
          return value ? value : "-----";
        }
      }
    },
    /*{
      name: "selectConfiguration",
      label: "Configuration",
      options: {
        filter: true,
        sort: true,
        searchable: true,
        customBodyRender: (value, { rowData }) => {
          return value ? value["name"] : "-----";
        }
      }
    },*/

    
    {
      name: "jobId",
      label: "Log",
      options: {
        filter: false,
        sort: false,
        searchable: false,
        customBodyRender: (value, { rowData }) => {
          return value ? (
            <ViewLog
              title={`Log viewer`}
              job_id={value}
            />
          ) : (
            "-----"
          );
        }
      }
    },
    {
      name: "monitorLink",
      label: "INSIGHTS",
      options: {
        filter: false,
        sort: false,
        searchable: false,
        customBodyRender: (value, { rowData }) => {
          return value ? (
            <Link
              onClick={() => {
                window.open(value);
              }}
            >
              <OpenInNewIcon> </OpenInNewIcon>
            </Link>
          ) : (
            "-----"
          );
        }
      }
    }
  ],
  options: {
    filter: false,
    print: false,
    download: false,
    responsive: "scrollMaxHeight",
    rowsPerPage: 5,
    rowsPerPageOptions: [5],
    searchPlaceholder:
      "Search through Name, environment, configuration and Status",
    selectTableRowsOnClick: false,
    selectableRows: "none",
    search: false,
    sort: false,
    viewColumns: false,
    pagination: false
  }
};
