import _ from "lodash";

export default {
  aws: {
    packer: {
      profile: {
        label: "Profile",
        helper: "Required if Access Key and Secret Key cannot be provided",
        type: "text",
        defaultValue: "default",
        options: "default",
        required: true,
        readOnly: false
      },
      aws_access_key: {
        label: "Access Key",
        helper: "Required if Profile is not provided",
        type: "text",
        defaultValue: "",
        required: false,
        readOnly: false
      },
      aws_secret_key: {
        label: "Secret Key",
        helper: "Required if Profile is not provided",
        type: "text",
        defaultValue: "",
        required: false,
        readOnly: false
      },
      ssh_username: {
        label: "SSH User",
        helper: "",
        type: "text",
        defaultValue: "ubuntu",
        required: true,
        readOnly: false
      },
      instance_type: {
        label: "Instance Size",
        helper: "",
        type: "select",
        defaultValue: "t2.micro",
        options: ["t2.micro", "t2.medium", "t2.large", "m4.large", "m5.large"],
        required: true,
        readOnly: false
      },
      aws_region: {
        label: "Region",
        helper: "",
        type: "select",
        defaultValue: "ap-south-1",
        options: ["ap-south-1", "us-east-1", "us-west-2"],
        required: true,
        readOnly: false
      },
      vpc_id: {
        label: "VPC ID",
        helper: "Optional",
        type: "text",
        defaultValue: "",
        required: false,
        readOnly: false
      }
    },
    terraform: {
      profile: {
        label: "Profile",
        helper: "Required if Access Key and Secret Key cannot be provided",
        type: "text",
        defaultValue: "default",
        options: "default",
        required: true,
        readOnly: false
      },
      aws_access_key: {
        label: "Access Key",
        helper: "Required if Profile is not provided",
        type: "text",
        defaultValue: "",
        required: false,
        readOnly: false
      },
      aws_secret_key: {
        label: "Secret Key",
        helper: "Required if Profile is not provided",
        type: "text",
        defaultValue: "",
        required: false,
        readOnly: false
      },

      ssh_user: {
        label: "SSH User",
        helper: "",
        type: "text",
        defaultValue: "ubuntu",
        required: true,
        readOnly: false
      },
      aws_instance_size: {
        label: "Instance Size",
        helper: "",
        type: "select",
        defaultValue: "t2.micro",
        options: ["t2.micro", "t2.medium", "t2.large", "m4.large", "m5.large"],
        required: true,
        readOnly: false
      },
      aws_region: {
        label: "Region",
        helper: "",
        type: "select",
        defaultValue: "ap-south-1",
        options: ["ap-south-1", "us-east-1", "us-west-2"],
        required: true,
        readOnly: false
      },
      public_key_path: {
        label: "Public Key",
        type: "text",
        helper: "",
        defaultValue: "/home/ubuntu/.ssh/id_rsa.pub",
        required: false,
        readOnly: true
      },
      private_key_path: {
        label: "Private Key",
        type: "text",
        helper: "",
        defaultValue: "/home/ubuntu/.ssh/id_rsa",
        required: false,
        readOnly: true
      },
      aws_worker_count: {
        label: "Worker Count",
        type: "select",
        defaultValue: 2,
        options: [..._.range(1, 101)],
        required: true,
        readOnly: false
      },
      aws_master_members_count: {
        label: "Master Member Count",
        type: "select",
        helper: "Additional Masters",
        defaultValue: 0,
        options: [..._.range(8)],
        required: true,
        readOnly: false
      },
      aws_key_name: {
        label: "Key Name",
        helper: "",
        type: "text",
        defaultValue: "terrakey",
        required: true,
        readOnly: false
      },
      ansible_path: {
        label: "Ansible Path",
        type: "text",
        helper: "",
        defaultValue: "/home/ubuntu/sysAssure/Sysassure/Infra-aws/terraform",
        required: false,
        readOnly: false
      }
    }
  },
  gcp: {
    packer: {
      account_file: {
        label: "Credentials",
        helper: "",
        type: "text",
        defaultValue: "../account.json",
        required: true,
        readOnly: false
      },
      project_id: {
        label: "Project",
        helper: "",
        type: "text",
        defaultValue: "sysassure2",
        required: true,
        readOnly: false
      },
      ssh_username: {
        label: "SSH User",
        helper: "",
        type: "text",
        defaultValue: "ubuntu",
        required: false,
        readOnly: true
      },
      source_image: {
        label: "Source Image",
        helper: "",
        type: "text",
        defaultValue: "ubuntu-1804-bionic-v20190722a",
        required: false,
        readOnly: true
      },
      zone: {
        label: "Zone",
        type: "select",
        defaultValue: "asia-south1-a",
        options: ["asia-south1-a", "us-central1-a", "us-east1-a"],
        required: true,
        readOnly: false
      },
      image_name: {
        label: "Image Name",
        helper: "",
        type: "text",
        defaultValue: "packer-image",
        required: true,
        readOnly: false
      }
    },
    terraform: {
      credentials: {
        label: "Credentials",
        helper:
          "File that contains your service account private key in JSON format",
        type: "text",
        defaultValue: "../account.json",
        required: true,
        readOnly: false
      },
      project_id: {
        label: "Project",
        helper: "GCP project where resources will be created",
        type: "text",
        defaultValue: "sysassure2",
        required: true,
        readOnly: false
      },
      ssh_user: {
        label: "SSH User",
        helper: "GCE SSH username",
        type: "text",
        defaultValue: "ubuntu",
        required: false,
        readOnly: true
      },
      ssh_pub_key_file: {
        label: "SSH Public Key File",
        helper: "SSH Public key path",
        type: "text",
        defaultValue: "/home/ubuntu/.ssh/id_rsa.pub",
        required: false,
        readOnly: true
      },
      region: {
        label: "Region",
        helper: "Location for your resources to be created in",
        type: "select",
        defaultValue: "asia-south1",
        options: ["asia-south1", "us-central1", "us-east1"],
        required: true,
        readOnly: false
      },
      zone: {
        label: "Zone",
        helper: "Availability zone",
        type: "select",
        defaultValue: "asia-south1-a",
        options: ["asia-south1-a", "us-central1-a", "us-east1-a"],
        required: true,
        readOnly: false
      },
      image_name: {
        label: "Image Name",
        helper: "Image to be used",
        type: "text",
        defaultValue: "ubuntu-packer-image",
        required: true,
        readOnly: false
      },
      swarm_managers_instance_type: {
        label: "Swarm Manager Instance Type",
        helper: "Machine type",
        type: "select",
        defaultValue: "g1-small",
        options: [
          "g1-small",
          "n1-standard-1",
          "n1-standard-2",
          "n1-standard-4"
        ],
        required: true,
        readOnly: false
      },
      swarm_workers: {
        label: "Swarm Workers Count",
        helper: "Number of Swarm workers",
        type: "select",
        defaultValue: 1,
        options: [..._.range(1, 101)],
        required: true,
        readOnly: false
      },
      swarm_workers_instance_type: {
        label: "Swarm Worker Instance Type",
        helper: "Machine type",
        type: "select",
        defaultValue: "g1-small",
        options: ["g1-small", "n1-standard-1", "n1-standard-2"],
        required: true,
        readOnly: false
      },
      ansiblePath: {
        label: "Ansible Path",
        helper: "",
        type: "text",
        defaultValue: "/home/ubuntu/sysAssure/Sysassure/Infra-gcp/terraform",
        required: false,
        readOnly: false
      },
      swarm_managers: {
        label: "Swarm Managers",
        helper: "Number of swarm managers",
        type: "select",
        defaultValue: 1,
        options: [..._.range(1, 101)],
        required: false,
        readOnly: false
      }
    }
  },
  azure: {},
  rack: {},
  do: {}
};
