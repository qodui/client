export default {
	steps: [
		{ id: 0, label: "Select Cloud Platform" },
		{ id: 1, label: "Packer Properties" },
		{ id: 2, label: "Terraform Properties" },
		{ id: 3, label: "Additional Properties" },
		{ id: 4, label: "Preview" }
	]
};
