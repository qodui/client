import React from "react";
import ViewFile from "components/PageContent/Environments/ViewFile";
import Typography from '@material-ui/core/Typography';
import { createMuiTheme } from '@material-ui/core/styles';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import green from '@material-ui/core/colors/green';
import ReportProblemOutlinedIcon from '@material-ui/icons/ReportProblemOutlined';






const theme = createMuiTheme({
  palette: {
   primary: green,
   secondary: {
     main: '#2e7d32',
   },
  },
 });

export default {
  columns: [
    {
      name: "name",
      label: "Name",
      options: { filter: false, sort: true, viewColumns: false }
    },
    {
      name: "platform",
      label: "Platform",
      options: { filter: true, sort: true }
    },
    { name: "type", label: "Type", options: { filter: true, sort: true } },
    {
      name: "file",
      label: "Existing File",
      options: {
        filter: false,
        sort: true,
        searchable: false,
        customBodyRender: (value, { rowData }) => {
          return value ? (
            <ViewFile
              title={`${rowData[0]} Environment File`}
              content={value}
            />
          ) : (
            "-----"
          );
        }
      }
    },
    {
      name: "terraform",
      label: "TF File",
      options: {
        filter: false,
        sort: false,
        searchable: false,
        customBodyRender: (value, { rowData }) => {
          return value ? (
            <ViewFile
              title={`${rowData[0]} - Terraform File`}
              content={value}
            />
          ) : (
            "-----"
          );
        }
      }
    },
    {
      name: "packer",
      label: "Packer File",
      options: {
        filter: false,
        sort: false,
        searchable: false,
        customBodyRender: (value, { rowData }) => {
          return value ? (
            <ViewFile
              title={`${rowData[0]} - Packer JSON File`}
              content={JSON.stringify(value, null, 2)}
            />
          ) : (
            "-----"
          );
        }
      }
    },
    {
      name: "status",
      label: "Status",
      options: { 
        filter: true, 
        sort: true,
        customBodyRender: (value, { rowData }) => {
          return value ? (
            <MuiThemeProvider theme = {theme}>
            <Typography color={value==="To Be Created"?"error":"primary"}>{value==="To Be Created"?<ReportProblemOutlinedIcon fontSize="large" style={{ color: "black" }}></ReportProblemOutlinedIcon>:""}{value}</Typography>
            </MuiThemeProvider>
          ) : (
            "-----"
          );
        } }
    }
  ],
  options: {
    filterType: "multiselect",
    print: false,
    download: false,
    responsive: "scrollMaxHeight",
    rowsPerPage: 5,
    rowsPerPageOptions: [5, 10],
    searchPlaceholder: "Search through Name, Type, Platform and Status",
    selectableRowsOnClick: true
  }
};
