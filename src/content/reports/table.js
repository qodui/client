import React from "react";
import OpenInNewIcon from "@material-ui/icons/OpenInNew";
import { Link, Button } from "@material-ui/core";
import ViewLog from "components/PageContent/Reports/ViewLog";
import { createMuiTheme } from '@material-ui/core/styles';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import green from '@material-ui/core/colors/green';
import Typography from '@material-ui/core/Typography';
import ErrorIcon from '@material-ui/icons/Error';



const theme = createMuiTheme({
  palette: {
   primary: green,
   secondary: {
     main: '#2e7d32',
   },
  },
 });


export default {
  columns: [
    {
      name: "executionName",
      label: "Name",
      options: { filter: false, sort: true, viewColumns: false ,searchable: true}
    },
    {
      name: "created",
      label: "Date",
      options: { filter: true, sort: true }
    },
    {
      name: "selectEnvironment",
      label: "Environment",
      options: {
        filter: true,
        sort: true,
        searchable: true,
        customBodyRender: (value, { rowData }) => {
          return value ? value["name"] : "-----";
        }
      }
    },
    {
      name: "selectConfiguration",
      label: "Configuration",
      options: {
        filter: true,
        sort: true,
        searchable: true,
        customBodyRender: (value, { rowData }) => {
          return value ? value["name"] : "-----";
        }
      }
    },

    {
      name: "status",
      label: "Status",
      options: { filter: true, sort: true,
        customBodyRender: (value, { rowData }) => {
          return value ? (
            <MuiThemeProvider theme = {theme}>
            <Typography color={value==="successful"?"primary":"error"}>{value==="failed"?<ErrorIcon style={{ color: "red" }}></ErrorIcon>:""}{value}</Typography>
            </MuiThemeProvider>
          ) : (
            "-----"
          );
        }
       }
    },
    {
      name: "jobId",
      label: "Log",
      options: {
        filter: false,
        sort: false,
        searchable: false,
        customBodyRender: (value, { rowData }) => {
          return value ? (
            <ViewLog
              title={`${rowData[0]} Log viewer`}
              job_id={rowData[0]}
            />
          ) : (
            "-----"
          );
        }
      }
    },
    {
      name: "monitorLink",
      label: "INSIGHTS",
      options: {
        filter: false,
        sort: false,
        searchable: false,
        customBodyRender: (value, { rowData }) => {
          return value ? (
            <Link
              onClick={() => {
                window.open(value);
              }}
            >
              <OpenInNewIcon> </OpenInNewIcon>
            </Link>
          ) : (
            "-----"
          );
        }
      }
    }
  ],
  options: {
    filterType: "multiselect",
    print: false,
    download: false,
    responsive: "scrollMaxHeight",
    rowsPerPage: 10,
    rowsPerPageOptions: [5, 10, 15],
    searchPlaceholder:
      "Search through Name, environment, configuration and Status",
    selectTableRowsOnClick: false
  }
};
