import React from "react";
import { List } from "@material-ui/core";

import Item from "layout/Item";

export default [
  {
    name: "kvp1",
    heading: "Automation",
    details: (
      <List>
        <Item text="Enable E2E Test automation from Edge device to application layer." />
        <Item text="Automated Test approach covering functional, non-functional, performance and security aspects." />
        <Item text="Wipro's Assure toolset provides test automation at each layer." />
      </List>
    ),
    backgroundColor: "#d50000",
    color: "white",
    subheading: ""
  },
  {
    name: "kvp2",
    heading: "Crowd Sourcing",
    details: (
      <List>
        <Item text="Testing of Smart Connected applications in real world conditions at speed." />
        <Item text="Ability to test Geo-specific real world scenarios with multiple devices and components." />
        <Item text="Enhancement of productivity gain by quicker volume of test coverage." />
      </List>
    ),
    backgroundColor: "#ff5722",
    color: "white",
    subheading: "Using TopCoder"
  },
  {
    name: "kvp3",
    heading: "Cognitive Approach",
    details: (
      <List>
        <Item text="Providing analysis and insights on QoD testing using Wipro's AI powered Holmes platform." />
        <Item text="Automated Failure Bot for auto failure triaging of logs generated from various layers due to QoD automation framework execution." />
      </List>
    ),
    backgroundColor: "#f57f17",
    color: "white",
    subheading: ""
  },
  {
    name: "kvp4",
    heading: "Skilled Team",
    details: (
      <List>
        <Item text="Trained on advanced IoT testing capabilities along with hands on Automation tool usage." />
        <Item text="Skills on leveraging Crowd sourcing tools when necessary." />
      </List>
    ),
    backgroundColor: "#26a69a",
    color: "white",
    subheading: ""
  }
];
