import React from "react";
import { Typography } from "@material-ui/core";

export default [
  {
    name: "tPanel1",
    heading: "Application/UI/UX",
    details: (
      <Typography type="body2" style={{ fontSize: "14pt" }}>
        API Testing, WebApps Testing, Test Automation, Device Control validation
      </Typography>
    ),
    backgroundColor: "#d50000",
    color: "white",
    subheading: "App Assure, API Assure"
  },
  {
    name: "tPanel2",
    heading: "IOT Platform & Data Management",
    details: (
      <Typography type="body2" style={{ fontSize: "14pt" }}>
        API testing, Fault Tolerance, error injection, interoperability, and
        Performance testing
      </Typography>
    ),
    backgroundColor: "#ff5722",
    color: "white",
    subheading:
      "ThingsSimulator, Dev Assure, Sys Assure, Fault Tolerant Simulator"
  },
  {
    name: "tPanel3",
    heading: "Connectivity (devices)",
    details: (
      <Typography type="body2" style={{ fontSize: "14pt" }}>
        Connectivity, inter-operability and conformance availablity testing, and
        Protocol validation
      </Typography>
    ),
    backgroundColor: "#f57f17",
    color: "white",
    subheading: "Protocol Validation (Mosquitto, Iotify.io, Copper)"
  },
  {
    name: "tPanel4",
    heading: "Edge",
    details: (
      <Typography type="body2" style={{ fontSize: "14pt" }}>
        Endpoint validation, device functionality testing, Certification testing
      </Typography>
    ),
    backgroundColor: "#26a69a",
    color: "white",
    subheading:
      "ThingsSimulator, DevAssure, Cognitive Device Management(Wipro Sentriso)"
  },
  {
    name: "tPanel5",
    heading: "Security",
    details: (
      <Typography type="body2" style={{ fontSize: "14pt" }}>
        Validation for data authentication, protection/encryption, trust,
        access, device identiry, IAM
      </Typography>
    ),
    backgroundColor: "#1565c0",
    color: "white",
    subheading:
      "Cognitive Device Management(Wipro Sentriso), Thingful, Shodan, SOASTA, CloudTest"
  }
];
