import React from "react";
import { List } from "@material-ui/core";

import Item from "layout/Item";

export default [
  {
    name: "dom1",
    heading: "Tools Offering",
    details: (
      <List>
        <Item text="Quality of devices framework tools hosted on cloud/on premise." />
        <Item text="Tools offered as hosted services as integrated test solution." />
        <Item text="Individual tool with specific licensing." />
        <Item text="Customization based on customer needs." />
        <Item text="Lead time of 2-6 weeks of initial tool set-up based on tool set selected." />
      </List>
    ),
    backgroundColor: "#d50000",
    color: "white",
    subheading: ""
  },
  {
    name: "dom2",
    heading: "IP Model Deployment",
    details: (
      <List>
        <Item text="As a service model (part of managed services model)." />
        <Item text="Catalogue service model (Specific license based on selected)" />
        <Item text="Continuous Support for tools deployment/maintenance." />
      </List>
    ),
    backgroundColor: "#ff5722",
    color: "white",
    subheading: "Customization may involve extra costing"
  },
  {
    name: "dom3",
    heading: "Engagement Model",
    details: (
      <List>
        <Item text="FPP model with tools + support people." />
        <Item text="T&M model." />
        <Item text="Outcome based model." />
        <Item text="Core and Flex model" />
      </List>
    ),
    backgroundColor: "#f57f17",
    color: "white",
    subheading: "Support resource and tools charged separately."
  }
];
