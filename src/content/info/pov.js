import React from "react";

import { Typography } from "@material-ui/core";

export default [
  {
    name: "panel1",
    heading: "Interoperability/Certification/Regulatory",
    subheading: "",
    backgroundColor: "#d50000",
    color: "white",
    details: (
      <Typography type="body2" style={{ fontSize: "14pt" }}>
        Interoperability between components (h/w, s/w), Operator certifications,
        Protocol level conformance and Industry regulations.
      </Typography>
    )
  },
  {
    name: "panel4",
    heading: "Functionality and Connectivity",
    subheading: "",
    backgroundColor: "#ff5722",
    color: "white",
    details: (
      <Typography type="body2" style={{ fontSize: "14pt" }}>
        These tests verify that data is transmitted in sequence and acknowledged
        in proper fashion.{" "}
      </Typography>
    )
  },
  {
    name: "panel5",
    heading: "Upgrade and Supportability",
    subheading: "",
    backgroundColor: "#f57f17",
    color: "white",
    details: (
      <Typography type="body2" style={{ fontSize: "14pt" }}>
        All upgrade and downgrade paths should be tested for firmware releases.
      </Typography>
    )
  },
  {
    name: "panel2",
    heading: "Quality of Service",
    subheading: "",
    backgroundColor: "#26a69a",
    color: "white",
    details: (
      <Typography type="body2" style={{ fontSize: "14pt" }}>
        Test for overall QoS of the environment and the E2E ecosystem.
      </Typography>
    )
  },
  {
    name: "panel3",
    heading: "Security",
    subheading: "",
    backgroundColor: "#1565c0",
    color: "white",
    details: (
      <Typography type="body2" style={{ fontSize: "14pt" }}>
        Due to the multitude of devices of testing security is a high priority
        focus area along with Test for users' roles, access, authentication and
        encryption techniques.
      </Typography>
    )
  },
  {
    name: "panel7",
    heading: "Performance",
    subheading: "",
    backgroundColor: "#2196f3",
    color: "white",
    details: (
      <Typography type="body2" style={{ fontSize: "14pt" }}>
        Test for load, stress and over all performance of the complete ecosystem
        involving all components.
      </Typography>
    )
  },
  {
    name: "panel6",
    heading: "Reliability and Scalability",
    subheading: "",
    backgroundColor: "#9575cd",
    color: "white",
    details: (
      <Typography type="body2" style={{ fontSize: "14pt" }}>
        Test for reliability of service and scalability testing along with the
        tests to validate the data as well as ensuring that all the data types
        are saved properly and retrieved from the cloud.
      </Typography>
    )
  }
];
