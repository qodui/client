export default {
	columns: [
		{
			name: "name",
			label: "Name",
			options: { filter: false, sort: true, viewColumns: false }
		},
		{
			name: "testArea",
			label: "Test Area",
			options: { filter: true, sort: false }
		},
		{
			name: "testType",
			label: "Test Type",
			options: { filter: true, sort: false }
		},
		{
			name: "suiteType",
			label: "Test Suite Type",
			options: {
				filter: false,
				sort: false
			}
		}
	],
	options: {
		filterType: "multiselect",
		print: false,
		download: false,
		responsive: "scrollMaxHeight",
		rowsPerPage: 5,
		rowsPerPageOptions: [5, 10],
		searchPlaceholder: "Search table...",
		selectableRowsOnClick: true
	}
};
