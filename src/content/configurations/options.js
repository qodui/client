export default {
  testArea: [
    { id: 0, name: "Edge", value: "Edge" },
    { id: 1, name: "Device", value: "Device" },
    { id: 2, name: "Cloud", value: "Cloud" },
    { id: 3, name: "Mobile", value: "Mobile" }
  ],
  testType: [
    {
      id: 0,
      name: "Web UI",
      value: "Web UI"
    },
    {
      id: 1,
      name: "Mobile UI",
      value: "Mobile UI"
    },
    {
      id: 2,
      name: "API",
      value: "Api"
    },
    {
      id: 3,
      name: "Non Functional",
      value: "Non Functional"
    }
  ],
  suiteType: [
    {
      id: 0,
      name: "Integration",
      value: "Integration",
      type: "functional"
    },
    { id: 1, name: "Sanity", value: "Sanity", type: "functional" },
    { id: 2, name: "Acceptance", value: "Acceptance", type: "functional" },
    { id: 3, name: "Regression", value: "Regression", type: "both" },
    {
      id: 4,
      name: "Fault Tolerance",
      value: "Fault Tolerance",
      type: "non-functional"
    },
    {
      id: 5,
      name: "Scalability",
      value: "Scalability",
      type: "non-functional"
    },
    {
      id: 6,
      name: "Performance",
      value: "Performance",
      type: "non-functional"
    }
  ]
};
